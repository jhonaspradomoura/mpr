import json
import csv
import os
import numpy as np
import argparse


def planning(args):
    with open(f"{args.folder}/statistics.json", "r") as f:
        stats = json.load(f)
    for seen_status, results in stats.items():
        time_fieldnames = ["#", *results["timing"].keys()]
        with open(f"{args.folder}/{seen_status}_timing.csv", "w") as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=time_fieldnames)
            writer.writeheader()
            csv_dict = []
            for i in range(len(results["timing"][time_fieldnames[1]])):
                csv_dict.append({"#": i + 1})
                for model, time in results["timing"].items():
                    csv_dict[-1][model] = time[i]
            writer.writerows(csv_dict)
        with open(f"{args.folder}/{seen_status}_performance.csv", "w") as csv_file:
            results_fieldnames = ["result", *results["results"].keys()]
            writer = csv.DictWriter(csv_file, fieldnames=results_fieldnames)
            writer.writeheader()
            csv_dict = []
            for res in results["results"][results_fieldnames[-1]].keys():
                csv_dict.append({"result": res})
                for model in results["results"].keys():
                    try:
                        csv_dict[-1][model] = results["results"][model][res]
                    except KeyError:
                        csv_dict[-1][model] = 0
            writer.writerows(csv_dict)


def encoder(args, file, json_data, csv_file):
    fieldnames = ["itteration", "best loss", "best epoch"]
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()
    if not args.convert:
        with open(f"{args.folder}/new_{file}", "w") as f:
            step = np.array(json_data[file.split(".json")[0]])
            data = {"loss": [], "epochs": []}
            for n, itt in enumerate(step):
                losses = itt["loss"]
                epochs = itt["epochs"]
                best_epoch_idx = np.where(losses == np.min(losses))[0][0]
                # writer.writerow(
                #     {"itteration": n + 1, "best loss": losses[best_epoch_idx], "best epoch": epochs[best_epoch_idx]}
                # )
                data["loss"].append(losses[best_epoch_idx])
                data["epochs"].append(epochs[best_epoch_idx])
            json.dump(data, f)
    else:
        for n, (loss, epoch) in enumerate(zip(json_data["loss"], json_data["epochs"])):
            writer.writerow({"itteration": n + 1, "best loss": loss, "best epoch": epoch})


def join_data(folder, files: list, compare, step):
    if step == "encoder":
        reference = "prelu.json"
        ref_name = "prelu"
    else:
        reference = "prelu.json"
        ref_name = "prelu"
    files.remove(reference)

    fieldnames = ["model", "best loss", "best epoch"]
    with open(f"{folder}/{reference}", "r") as f:
        prelu_data = json.load(f)
    if compare == "all":
        with open(f"{folder}/csv/join_data.csv", "w") as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()
            for loss, epoch in zip(prelu_data["loss"], prelu_data["epochs"]):
                writer.writerow({"model": ref_name, "best loss": loss, "best epoch": epoch})

            for file in files:
                with open(f"{folder}/{file}", "r") as f:
                    data = json.load(f)
                for loss, epoch in zip(data["loss"], data["epochs"]):
                    writer.writerow({"model": f"{file.split('.json')[0]}", "best loss": loss, "best epoch": epoch})
    else:
        for file in files:
            with open(f"{folder}/csv/{file.split('.json')[0]}.csv", "w") as csv_file:
                with open(f"{folder}/{file}", "r") as f:
                    data = json.load(f)
                writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
                writer.writeheader()
                for p_loss, p_epochs, t_loss, t_epochs in zip(
                    prelu_data["loss"], prelu_data["epochs"], data["loss"], data["epochs"]
                ):
                    writer.writerow({"model": ref_name, "best loss": p_loss, "best epoch": p_epochs})
                    writer.writerow({"model": f"{file.split('.json')[0]}", "best loss": t_loss, "best epoch": t_epochs})


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("--folder", type=str, help="Folder to get json files from", required=True)
    parser.add_argument("--convert", const=True, nargs="?", help="No processing done, only converts to csv")
    parser.add_argument("--plan", const=True, nargs="?", help="Convert planning json to csv.")
    parser.add_argument(
        "--compare", default=None, type=str, help="Joins where other tuner data with prelu", required=False
    )
    args = parser.parse_args()

    if args.plan:
        planning(args)
    else:
        files = os.listdir(args.folder)
        files = [file for file in files if "json" in file]
        if args.compare:
            join_data(args.folder, files, args.compare, "planner" if args.folder == "planner" else "encoder")

        else:
            csv_folder = "csv" if not args.convert else "csv_indiv"

            for file in files:
                with open(f"{args.folder}/{file}", "r") as f:
                    json_data = json.load(f)
                    with open(f"{args.folder}/{csv_folder}/{file.split('.json')[0]}.csv", "w") as csv_file:
                        encoder(args, file, json_data, csv_file)
