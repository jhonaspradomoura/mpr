FROM ros:melodic-robot

RUN apt update && apt install -y ros-melodic-move-base-flex gdb

WORKDIR /usr/src/mpr
ENV ROS_WS "/usr/src/mpr"

COPY ./src/navigation /usr/src/mpr/src/navigation

RUN sed --in-place --expression '$icd $ROS_WS && catkin_make install && source "$ROS_WS/devel/setup.bash"' /ros_entrypoint.sh

CMD ["roslaunch","navigation","move_base"]
