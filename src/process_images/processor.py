from matplotlib import pyplot as plt
import plotly.graph_objects as go
import os
import numpy as np
from mpl_toolkits.axes_grid1 import ImageGrid
from plotly.subplots import make_subplots

project_path = f"{os.path.abspath(__file__).split('mpr')[0]}mpr"


def show_random_images():
    imgs = os.listdir(f"{os.path.dirname(__file__)}/../navigation/maps/cropped/")
    imgs = [img for img in imgs if img.endswith(".png")]
    
    chosen_imgs = np.random.choice(imgs, 6)
    
    imgs = []
    for img in chosen_imgs:
        loaded_img = plt.imread(f"{os.path.dirname(__file__)}/../navigation/maps/cropped/{img}", 0)
        imgs.append(cv2.resize(loaded_img, (110, 110)))
    
    f = plt.figure(figsize=(5, 5))
    
    grid = ImageGrid(f, 111, nrows_ncols=(2, 3), axes_pad=(0.01, 0.01))
    
    for axs, img in zip(grid, imgs):
        axs.imshow(img)
        axs.axis("off")
    plt.show()


def show_image(id):
    imgs = os.listdir(f"{project_path}/obs/center{id}")
    imgs = [img for img in imgs if "env" in img]
    
    chosen_imgs = np.random.choice(imgs, 6)
    
    fig = make_subplots(
            rows=2, cols=3, shared_xaxes=True, shared_yaxes=True,
            # specs=[[{"colspan": 6}, {"colspan": 6}, *[None] * 4]]
            )
    for n, img in enumerate(chosen_imgs):
        loaded_img = np.load(f"{project_path}/obs/center{id}/{img}")
        fig.add_trace(
                go.Scatter(x=loaded_img[:, 0], y=loaded_img[:, 1], mode="markers", marker=dict(
                        color="royalblue")),
                row=int(n / 3) + 1,
                col=(n % 3) + 1)
    fig.update_xaxes(showticklabels=False)
    fig.update_yaxes(showticklabels=False)
    fig.update_layout(width=1024, height=768, showlegend=False)
    
    fig.show()


if __name__ == "__main__":
    show_image(0)
