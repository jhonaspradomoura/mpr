from distutils.core import setup

from catkin_pkg.python_setup import generate_distutils_setup

# fetch values from package.xml
setup_args = generate_distutils_setup(packages=['cae', 'planner'],
                                      package_dir={'': 'src'},
                                      py_modules=[],
                                      scripts=['src/control/pioneer3at.py', 'src/utils/pioneer_teleop.py',
                                               'bin/data_gen.py'])
setup(**setup_args)
