#! /usr/bin/env python3

import rospy
from pynput import keyboard as kb
from geometry_msgs.msg import Twist

VEL_OFFSET = 0.1
VEL_MAX = 6.4

tmp_msg = Twist()
msg = Twist()


def linear_x(offset):
    tmp_msg.linear.x += offset if abs(tmp_msg.linear.x + offset) <= abs(VEL_MAX) \
        else tmp_msg.linear.x / abs(tmp_msg.linear.x) * (VEL_MAX - abs(tmp_msg.linear.x))


def angular_z(offset):
    tmp_msg.angular.z += offset if abs(tmp_msg.angular.z + offset) <= abs(VEL_MAX) \
        else offset / abs(offset) * (VEL_MAX - abs(tmp_msg.angular.z))


def reset_angular(*args):
    tmp_msg.angular.z = 0


def stop(*args):
    tmp_msg.linear.x = 0
    tmp_msg.angular.z = 0


hotkeys = {kb.Key.up   : (linear_x, 1), kb.Key.down: (linear_x, -1), kb.Key.left: (angular_z, -1),
           kb.Key.right: (angular_z, 1), kb.Key.space: (reset_angular, None), kb.KeyCode.from_char('p'): (stop, None)}

pressed_keys = set()
held_keys = {}


def key_listener(key):
    try:
        if key in hotkeys:
            func, mod = hotkeys.get(key)
            if mod and key in held_keys:
                func(mod * VEL_OFFSET * held_keys.get(key, 1))
            else:
                func()
            msg.linear.x = tmp_msg.linear.x / VEL_MAX
            msg.angular.z = tmp_msg.angular.z / VEL_MAX
            pub.publish(msg)
            pressed_keys.remove(key)
            held_keys.pop(key)
    except Exception as err:
        rospy.logerr(err)


def press_listener(key):
    pressed_keys.add(key)
    if key in pressed_keys:
        if key not in held_keys:
            held_keys[key] = 0
        if held_keys[key] < 40:
            held_keys[key] += 1


if __name__ == '__main__':
    rospy.init_node("tele_op", anonymous=True, log_level=rospy.DEBUG)
    pub = rospy.Publisher("cmd_vel", Twist, queue_size=100)
    board = kb.Controller()
    listener = kb.Listener(on_press=press_listener, on_release=key_listener)
    listener.start()
    rospy.spin()
