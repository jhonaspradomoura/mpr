from logging import NOTSET
import plotly.graph_objects as go
import plotly.express as px
import os
import argparse
import numpy as np
import tensorflow as tf
from tensorflow.python.keras.backend import maximum
from planner.data_loader import get_autoencoder
from planner.run_stats import mpnet_plan
from planner.model import PlanningModule as pnet

project_path = f"{os.path.abspath(__file__).split('mpr')[0]}mpr"


def load_enet_model(model):
    ae = tf.keras.models.load_model(f"{project_path}/models/ae/{model}", custom_objects={"contractive_loss": None})
    return ae


def load_pnet_model(name):
    model = tf.saved_model.load(f"{project_path}/models/planner/{name}")
    return model


def load_env(id):
    envs = os.listdir(f"{project_path}/obs/center{id}")
    envs = [env for env in envs if "env" in env]

    chosen_env = np.random.choice(envs, 1)[0]
    return np.load(f"{project_path}/obs/center{id}/{chosen_env}")


def load_perm(center_id, env_id):
    env_number = env_id.split("env")[-1].split(".")[0].split("_")[-1]
    return np.load(f"{project_path}/obs/center{center_id}/perm{env_number}.npy")


def load_path(center_id):
    path = []
    envs_list = os.listdir(f"{project_path}/paths/training")
    envs_list = [env for env in envs_list if "env" in env]

    env_id = np.random.choice(envs_list, 1)[0]
    env = np.load(f"{project_path}/obs/center{center_id}/env{env_id.split('_')[-1]}.npy")
    path_list = os.listdir(f"{project_path}/paths/training/{env_id}")
    paths = [path for path in path_list]

    while len(path) < 4:
        chosen_path = np.random.choice(paths, 1)[0]
        path = np.load(f"{project_path}/paths/training/{env_id}/{chosen_path}")
    return path, env, env_id


def encoded(env):
    model_names = ["prelu", "tuner_0", "tuner_1", "tuner_2", "tuner_3"]
    models = []
    for name in model_names:
        models.append(load_enet_model(name))

    max_id = get_new_id("encoding", "encoded")

    for name, model in zip(model_names, models):
        env = np.interp(env, (-20, 20), (0, 1))
        env = env.reshape((1, -1))
        if "tuner" in name:
            model(env)
            encoded = model.code.numpy()
        else:
            encoded = model.encoder(env).numpy()
        fig = px.imshow(encoded)
        # fig.show()
        print(max_id)
        max_id += 1
        fig.write_image(f"{project_path}/visualizations/encoding/encoded_{max_id}.png")


def get_new_id(folder, file_name):
    existing_files = os.listdir(f"{project_path}/visualizations/{folder}")
    existing_files = [file for file in existing_files if file_name in file]
    ids = [int(file.split("_")[-1].split(".")[0]) for file in existing_files]
    if ids:
        ids = sorted(ids)
        max_id = int(ids[-1])
    else:
        max_id = 0
    return max_id


def reconstrution(env):
    max_id = get_new_id("reconstructed", "env")
    model_names = ["prelu", "tuner_1", "tuner_2", "tuner_3", "tuner_4"]
    models = []
    for name in model_names:
        models.append(load_enet_model(name))

    env = np.interp(env, (-20, 20), (0, 1))
    nn_env = env.reshape((1, -1))
    env = env.reshape((-1, 2))

    for name, model in zip(model_names, models):
        reconstruction = model.predict(nn_env).reshape((-1, 2))

        fig = go.Figure()
        fig.add_trace(
            go.Scatter(x=env[:, 0], y=env[:, 1], mode="markers", marker=dict(size=10), name="Cenário de entrada")
        )
        fig.add_trace(
            go.Scatter(
                x=reconstruction[:, 0],
                y=reconstruction[:, 1],
                mode="markers",
                marker=dict(size=10),
                name="Reconstrução",
            )
        )
        fig.update_xaxes(range=[0, 1])
        fig.update_yaxes(range=[0, 1])
        fig.update_layout(autosize=False, width=800, height=800, showlegend=False)
        fig.show()
        max_id += 1
        fig.write_image(f"{project_path}/visualizations/reconstructed/reconstruction_{max_id}.png")


def path(env, trajectory, center_id, env_id):
    input_data = {"env": {"norm": None, "perm": None}}
    env = np.interp(env, (-20, 20), (0, 1))
    input_data["env"]["norm"] = env
    input_data["env"]["perm"] = load_perm(center_id, env_id)

    input_path = np.array([trajectory[0], trajectory[1], trajectory[-2], trajectory[-1]])
    input_path = np.interp(input_path, (-20, 20), (0, 1))
    model_names = [
        # "prelu",
        "prelu_tuner",
        # "selu_1",
        # "selu_1_tuner",
        # "selu_2",
        "selu_2_tuner",
        # "selu_3",
        # "selu_3_tuner",
        # "selu_4",
        "selu_4_tuner",
    ]
    models = []
    for name in model_names:
        models.append(
            (
                load_enet_model("tuner_1" if "tuner" in name else "prelu"),
                load_pnet_model(name),
            )
        )

    fig = go.Figure()
    env = [[], []]
    obst_size = 5 / 40
    for obst_center in input_data["env"]["perm"]:
        x, y = np.interp(obst_center, (-20, 20), (0, 1))
        env[0].extend(
            [x - obst_size / 2, x - obst_size / 2, x + obst_size / 2, x + obst_size / 2, x - obst_size / 2, None]
        )
        env[1].extend(
            [y - obst_size / 2, y + obst_size / 2, y + obst_size / 2, y - obst_size / 2, y - obst_size / 2, None]
        )
    env = np.array(env)
    x = env[0]
    y = env[1]

    fig.add_trace(go.Scatter(x=x, y=y, fill="toself", fillcolor="black", name="obstacle"))

    colors = ["green", "blue", "red", "magenta"]  # , "yellow", "olive"]
    for n, model in enumerate(models):
        plan, _ = mpnet_plan(input_data, input_path, model)

        if plan is not None:
            plan = plan.reshape((-1, 2))

            res_x = plan[:, 0]
            res_y = plan[:, 1]
            fig.add_trace(
                go.Scatter(x=res_x, y=res_y, mode="lines+markers", line=dict(color=colors[n]), name=model_names[n])
            )
    fig.show()


def main(args):
    env = load_env(args.cid)
    mapping = {"ae": {"encoded": encoded, "reconstruction": reconstrution}, "plan": {"path": path}}
    for k, val in mapping.items():
        if args.op in val:
            stage = k
            break

    trajectory, _, chosen_env = load_path(args.cid)
    args_mapping = {"encoded": (env,), "reconstruction": (env,), "path": (env, trajectory, args.cid, chosen_env)}
    mapping[stage][args.op](*args_mapping[args.op])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-op", required=True, help="Qual operação deseja visualizar")
    parser.add_argument("--cid", default=3, required=True, help="ID da pasta center em obs")
    args = parser.parse_args()

    main(args)