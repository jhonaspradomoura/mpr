import tensorflow as tf
from tensorflow.core.example.feature_pb2 import Features
from cae import data_loader
import os
import numpy as np
import argparse

project_path = f"{os.path.abspath(__file__).split('mpr')[0]}mpr"


def contractive_loss(nn, predicted, reference):

    mse_error = tf.reduce_mean(tf.square(reference - predicted), axis=1)

    weights = nn.encoder.classifier.weights[0]
    weights = tf.transpose(weights)
    h = nn.code
    dh = h * (1 - h)
    contractive = nn.lambd * tf.reduce_sum(dh ** 2 * tf.reduce_sum(weights ** 2, axis=1), axis=1)
    return mse_error + contractive


def parse_file_cae(example_proto):
    keys_to_features = {
        "input": tf.io.FixedLenFeature((2800,), tf.float32),
        "output": tf.io.FixedLenFeature((2800,), tf.float32),
    }
    parsed_features = tf.io.parse_single_example(example_proto, keys_to_features)
    return parsed_features["input"], parsed_features["output"]


def parse_file_pnet(example_proto):
    keys_to_features = {
        "input": tf.io.FixedLenFeature((32,), tf.float32),
        "reference": tf.io.FixedLenFeature((2,), tf.float32),
    }
    parsed_features = tf.io.parse_single_example(example_proto, keys_to_features)
    return parsed_features["input"], parsed_features["reference"]


# def parse_array(record):
#     example = tf.train.Example()
#     example.ParseFromString(record.numpy())
#     return example


def read_cae():
    dataset = tf.data.TFRecordDataset([f"{project_path}/tfrecords/center3.tfrecord"])
    dataset = dataset.map(parse_file_cae)
    training = dataset.skip(5500).shuffle(1000).cache().batch(100).prefetch(tf.data.experimental.AUTOTUNE)
    validation = dataset.take(5500).cache().batch(100).prefetch(tf.data.experimental.AUTOTUNE)

    return training, validation


def read_pnet(file, batch_size):
    dataset = tf.data.TFRecordDataset([f"{project_path}/tfrecords/{file}.tfrecord"])
    dataset = dataset.map(parse_file_pnet)
    if "validation" in file:
        dataset = dataset.cache().batch(batch_size).prefetch(tf.data.experimental.AUTOTUNE)
    else:
        dataset = dataset.shuffle(1000).cache().batch(batch_size).prefetch(tf.data.experimental.AUTOTUNE)
    return dataset


def write(path, filename):
    files = os.listdir(f"{project_path}/{path}")
    files = [file for file in files if "env" in file]
    writer = tf.io.TFRecordWriter(f"{project_path}/tfrecords/{filename}.tfrecord")
    for file in files:
        arr = np.load(f"{project_path}/{path}/{file}", allow_pickle=True)
        arr = arr.flatten()
        arr = np.interp(arr, (-20, 20), (0, 1))
        feature = {}
        feature["input"] = tf.train.Feature(float_list=tf.train.FloatList(value=arr))
        feature["output"] = tf.train.Feature(float_list=tf.train.FloatList(value=arr))

        example = tf.train.Example(features=tf.train.Features(feature=feature))

        serialized = example.SerializeToString()
        writer.write(serialized)
    writer.close()


def write_pnet(path, filename, ae):
    path_files = os.listdir(f"{project_path}/{path}")
    path_files = [file for file in path_files if "env" in file]
    writer = tf.io.TFRecordWriter(f"{project_path}/tfrecords/{filename}_{ae}.tfrecord")

    cae = tf.keras.models.load_model(
        f"{project_path}/models/ae/{ae}", custom_objects={"contractive_loss": contractive_loss}
    )
    for file in path_files:
        arr = np.load(f"{project_path}/{path}/{file}", allow_pickle=True)
        env = np.load(f"{project_path}/obs/center3/env{file.split('_')[-1].split('.')[0]}.npy", allow_pickle=True)
        env = env.reshape((1, -1))
        encoded_env = cae.encoder(env).numpy()
        for traj in arr:
            for idx, data in enumerate(traj[:-1]):
                feature = {}
                data = np.interp(data, (-20, 20), (0, 1))
                next_data = np.interp(traj[idx + 1], (-20, 20), (0, 1))
                goal = np.interp(traj[-1], (-20, 20), (0, 1))

                input_data = np.array([*encoded_env.flatten(), *data, *goal]).flatten()
                reference = np.array([*next_data]).flatten()
                feature["input"] = tf.train.Feature(float_list=tf.train.FloatList(value=input_data))
                feature["reference"] = tf.train.Feature(float_list=tf.train.FloatList(value=reference))

                example = tf.train.Example(features=tf.train.Features(feature=feature))

                serialized = example.SerializeToString()
                writer.write(serialized)
    writer.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("path", default="")
    parser.add_argument("filename", default="")
    parser.add_argument("-w", const=True, nargs="?", required=False)
    parser.add_argument("-r", const=True, nargs="?", required=False)
    parser.add_argument("-op", default="enet")
    parser.add_argument("-ae", default="", required=False)
    args = parser.parse_args()

    if args.op == "enet":
        if args.r:
            read_cae()
        elif args.w and args.op == "enet":
            write(args.path, args.filename)
    else:
        if args.w:
            write_pnet(args.path, args.filename, args.ae)
    # training_ds, validation_ds = data_loader.load_tf_dataset(f"{project_path}/obs/center3/env*", True)