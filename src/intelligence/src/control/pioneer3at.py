#! /usr/bin/env python3
from math import inf

import tf
from geometry_msgs.msg import Twist, Quaternion, TransformStamped, Vector3, PoseWithCovarianceStamped
from sensor_msgs.msg import Imu, NavSatFix
from webots_ros.srv import set_float, set_int
from mpnet.srv import ready
import rospy
from std_msgs.msg import Empty, String
import multiprocessing as mp
import subprocess

controller = None
gps = True
time_step_proxy: rospy.ServiceProxy

reset_ev = mp.Event()
ready_ev = mp.Event()

motor_names = {'front_right_wheel', 'front_left_wheel', 'back_right_wheel', 'back_left_wheel'}
right_motors = {'front_right_wheel', 'back_right_wheel'}
left_motors = {'front_left_wheel', 'back_left_wheel'}

imu_data = 0, 0, 0, 0
localization_data = [0, 0, 0]


def on_shutdown():
    rospy.loginfo('User stopped the pioneer3at node.')
    # time_step_proxy(0)
    rospy.signal_shutdown("Shutting ros down!")


def ready_cb(msg):
    ready_ev.wait()
    ready_ev.clear()
    return True


def init_pure(msg: str):
    global controller, time_step_proxy, gps
    # rospy.wait_for_message('/model_name', String)
    
    rospy.loginfo("init pioneer3at")
    controller = msg
    
    # # Ativando IMU
    rospy.Subscriber(f"{controller}/inertial_unit/roll_pitch_yaw", Imu, imu_callback)
    
    rospy.Subscriber(f"{controller}/gps/values", NavSatFix, gps_callback)
    
    # Requisição do próximo time step ao webots
    while not rospy.is_shutdown():
        pass


def init(msg: String):
    global controller, time_step_proxy, gps
    rospy.wait_for_message('/model_name', String)
    
    rospy.loginfo("init pioneer3at")
    gps = rospy.get_param("/pioneer3at_control/gps")
    controller = msg.data
    rospy.Subscriber('/cmd_vel', Twist, cmd_vel, queue_size=100)
    
    # Ativando motores
    setup_motors()
    
    # Ativando lidar
    lidar_proxy = rospy.ServiceProxy(f"{controller}/Sick_LMS_291/enable", set_int)
    response = lidar_proxy(32)
    rospy.loginfo(f"Lidar {'enabled' if response.success else 'not enable'}.\n")
    #
    # # Ativando IMU
    imu_proxy = rospy.ServiceProxy(f"{controller}/inertial_unit/enable", set_int)
    response = imu_proxy(32)
    rospy.Subscriber(f"{controller}/inertial_unit/roll_pitch_yaw", Imu, imu_callback)
    rospy.loginfo(f"IMU {'enabled' if response.success else 'not enable'}.\n")
    
    # Caso seja requisitado o uso de GPS, ativar seu servico.
    if gps:
        # Ativando GPS
        gps_proxy = rospy.ServiceProxy(f"{controller}/gps/enable", set_int)
        response = gps_proxy(32)
        rospy.Subscriber(f"{controller}/gps/values", NavSatFix, gps_callback)
        rospy.loginfo(f"GPS {'enabled' if response.success else 'not enable'}.\n")
    else:
        rospy.Subscriber("amcl_pose", PoseWithCovarianceStamped, amcl_callback)
    
    time_step_proxy = rospy.ServiceProxy(f"{controller}/robot/time_step", set_int)
    ready_ev.set()
    
    # Requisição do próximo time step ao webots
    while not rospy.is_shutdown():
        try:
            if reset_ev.is_set():
                break
            rospy.wait_for_service(f"{controller}/robot/time_step", rospy.Duration(nsecs=int(2e8)))
            response = time_step_proxy(32)
            if not response.success:
                raise rospy.ServiceException("Response failure!")
        except (rospy.ServiceException, rospy.ROSException) as exc:
            rospy.logerr(f"Failed to retrieve next time step:\n\t{exc}")
            break


def setup_motors():
    global controller
    try:
        for motor in motor_names:
            set_position_proxy = rospy.ServiceProxy(f"{controller}/{motor}/set_position", set_float)
            response = set_position_proxy(-inf)
            rospy.loginfo(
                    f"{'Success' if response.success else 'Failure'} in setting position of {motor} to {inf}.")
            
            set_velocity_proxy = rospy.ServiceProxy(f"{controller}/{motor}/set_velocity", set_float)
            response = set_velocity_proxy(0)
            rospy.loginfo(f"{'Success' if response.success else 'Failure'} in setting velocity of {motor} to 0.\n")
    except rospy.service.ServiceException:
        setup_motors()


def gps_callback(msg: NavSatFix):
    global localization_data
    localization_data = [msg.latitude, msg.altitude, msg.longitude]
    transform()


def amcl_callback(msg: PoseWithCovarianceStamped):
    localization_data = [msg.pose.pose.position.x, msg.pose.pose.position.y, abs(msg.pose.pose.orientation.z)]
    transform()


def imu_callback(msg: Imu):
    global imu_data
    imu_data = msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w
    transform()


def transform():
    global imu_data, localization_data, gps
    broadcaster = tf.TransformBroadcaster()
    
    transf = TransformStamped()
    transf.header.frame_id = "map"
    transf.child_frame_id = "base_link"
    transf.header.stamp = rospy.Time.now()
    if gps:
        transf.transform.translation = Vector3(-localization_data[2], localization_data[0], localization_data[1])
    else:
        transf.transform.translation = Vector3(*localization_data)
    transf.transform.rotation = Quaternion(*imu_data[:-1], -imu_data[-1])
    broadcaster.sendTransformMessage(transf)
    
    transf.header.frame_id = "base_link"
    transf.child_frame_id = f"{controller}/Sick_LMS_291"
    transf.transform.translation = Vector3(0, 0, 0)
    transf.transform.rotation = Quaternion(0, 0, 0, 1)
    broadcaster.sendTransformMessage(transf)
    
    transf.header.frame_id = "map"
    transf.child_frame_id = "odom"
    broadcaster.sendTransformMessage(transf)


def cmd_vel(msg: Twist):
    if msg.linear.x > 0:
        if msg.angular.z > 0:
            right_speed = 6.4 * (msg.linear.x - .8 * msg.angular.z)
            left_speed = msg.linear.x * 6.4
        else:
            left_speed = 6.4 * (msg.linear.x + msg.angular.z)
            right_speed = 6.4 * msg.linear.x
    elif msg.linear.x < 0:
        if msg.angular.z > 0:
            right_speed = 6.4 * (msg.linear.x + msg.angular.z)
            left_speed = msg.linear.x * 6.4
        else:
            left_speed = 6.4 * (msg.linear.x - msg.angular.z)
            right_speed = 6.4 * msg.linear.x
    else:
        if msg.angular.z > 0:
            right_speed = 6.4 * msg.angular.z
            left_speed = 0
        else:
            left_speed = 6.4 * -msg.angular.z
            right_speed = 0
    
    for motor in right_motors:
        set_velocity_proxy = rospy.ServiceProxy(f"{controller}/{motor}/set_velocity", set_float)
        if 'back' in motor:
            response = set_velocity_proxy(0.9 * right_speed)
        else:
            response = set_velocity_proxy(right_speed)
        
        rospy.logdebug(f"{'Success' if response.success else 'Failure'} in setting velocity of {motor} to "
                       f"{right_speed:.1f}.")
    
    for motor in left_motors:
        set_velocity_proxy = rospy.ServiceProxy(f"{controller}/{motor}/set_velocity", set_float)
        if 'back' in motor:
            response = set_velocity_proxy(0.9 * left_speed)
        else:
            response = set_velocity_proxy(left_speed)
        
        rospy.logdebug(f"{'Success' if response.success else 'Failure'} in setting velocity of {motor} to "
                       f"{left_speed:.1f}.")


def reset(msg):
    ready_ev.clear()
    reset_ev.set()
    rospy.loginfo("Starting webots")
    subprocess.call(["webots", "--mode=realtime", msg.data, "--stdout", "--stderr", "--batch", "--minimize",
                     "--no-sandbox"])


if __name__ == '__main__':
    rospy.init_node('pioneer3at_control', log_level=rospy.INFO)
    # rospy.Subscriber('model_name', String, init)
    init_pure('pioneer3at')
    rospy.Subscriber('pioneer3at/reset', String, reset)
    rospy.Service("/pioneer3at/ready", ready, ready_cb)
    rospy.on_shutdown(on_shutdown)
    while not rospy.is_shutdown():
        continue
