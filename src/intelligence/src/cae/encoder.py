import os

import tensorflow as tf
from tensorflow.keras import layers, Sequential
from tensorflow.keras.optimizers import Adam
import numpy as np
from cae import data_loader
import argparse
from plotly import graph_objects as go
from plotly.subplots import make_subplots
from kerastuner import HyperParameters, Hyperband

# from cae.find_best_encoder import ContractiveAutoEncoder as tuner_cae


class Encoder(layers.Layer):
    def __init__(self, activation, **kwargs):
        super().__init__(**kwargs)
        self.inp_shape = kwargs.get("input_shape", (2800,))

        self.actv = activation()
        initializer = tf.keras.initializers.LecunNormal

        activation = layers.PReLU

        self.input_layer = layers.InputLayer(input_shape=self.inp_shape)
        self.hidden_layers = [
            layers.Dense(512, activation=activation(), kernel_initializer=initializer()),
            layers.Dense(
                256, activation=activation(), bias_initializer=initializer(), kernel_initializer=initializer()
            ),
            layers.Dense(
                128, activation=activation(), bias_initializer=initializer(), kernel_initializer=initializer()
            ),
        ]
        self.classifier = layers.Dense(28, bias_initializer=initializer())
        self.code = None

    def get_config(self):
        config = super(Encoder, self).get_config().copy()
        config.update({"activation": self.actv, "input_shape": self.inp_shape})
        return config

    def call(self, inputs, **kwargs):
        x = self.input_layer(inputs)
        for hidden in self.hidden_layers:
            x = hidden(x)
        self.code = self.classifier(x)
        return self.code


class Decoder(layers.Layer):
    def __init__(self, activation, **kwargs):
        super().__init__(**kwargs)
        self.actv = activation()
        self.inp_shape = kwargs.get("input_shape", (2800,))
        initializer = tf.keras.initializers.LecunNormal

        activation = layers.PReLU

        self.first_layer = layers.Dense(
            128, activation=activation(), bias_initializer=initializer(), kernel_initializer=initializer()
        )
        self.hidden_layers = [
            layers.Dense(
                256, activation=activation(), bias_initializer=initializer(), kernel_initializer=initializer()
            ),
            layers.Dense(
                512, activation=activation(), bias_initializer=initializer(), kernel_initializer=initializer()
            ),
        ]
        self.reconstruction = layers.Dense(
            kwargs["input_shape"][0], bias_initializer=initializer(), kernel_initializer=initializer()
        )

    def get_config(self):
        config = super(Decoder, self).get_config().copy()
        config.update({"activation": self.actv, "input_shape": self.inp_shape})
        return config

    def call(self, inputs, **kwargs):
        x = self.first_layer(inputs)
        for hidden in self.hidden_layers:
            x = hidden(x)
        x = self.reconstruction(x)
        return x


class ContractiveAutoEncoder(Sequential):
    def __init__(self, input_shape, activation):
        super().__init__()
        activation = lambda: "selu" if activation == "selu" else layers.PReLU
        self.encoder = Encoder(activation, input_shape=(input_shape,))
        self.add(self.encoder)

        self.decoder = Decoder(activation, input_shape=(input_shape,))
        self.add(self.decoder)

        self.lambd = 1e-3

        self.code = None
        self.compile(Adam(learning_rate=1e-4), loss=self.contractive_loss)

    def contractive_loss(self, predicted, reference):
        mse_error = tf.reduce_mean(tf.square(reference - predicted), axis=1)

        weights = self.encoder.classifier.weights[0]
        weights = tf.transpose(weights)
        h = self.code
        dh = h * (1 - h)
        contractive = self.lambd * tf.reduce_sum(dh ** 2 * tf.reduce_sum(weights ** 2, axis=1), axis=1)
        return mse_error + contractive

    def call(self, inputs, training=None, mask=None):
        self.code = self.encoder(inputs)
        return self.decoder(self.code)


def show_results(ds):
    fig = make_subplots(rows=1, cols=1, specs=[[{"type": "scatter"}]])
    reshaped = ds.reshape((-1, *input_shape))
    # x, y = get_coords(ds)
    x, y = reshaped[0, :, 0], reshaped[0, :, 1]
    fig.add_trace(go.Scatter(x=x, y=y, mode="markers", marker=dict(size=10)))
    fig.update_layout(autosize=False, width=800, height=800)
    fig.show()


def get_coords(ds):
    x = [a[0] for b in ds[0] for a in b]
    y = [a[1] for b in ds[0] for a in b]
    return x, y


def build_model(hp: HyperParameters):
    network = tuner_cae(hp, input_shape=2800)
    return network


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--id", type=str, help="ID of center data to load from obs folder", default=3, required=False)
    parser.add_argument("--map_size", type=int, help="Size of map data", default=40, required=False)
    parser.add_argument("--activ", type=str, help="Activation function", default="prelu", required=False)
    parser.add_argument("--folder", type=str, help="Folder to store model", default="autoencoder", required=False)
    parser.add_argument("--tuner", type=bool, default=False, help="Use tuner model", required=False)

    args = parser.parse_args()

    if args.tuner:
        hp = HyperParameters()
        tuner = Hyperband(
            build_model,
            objective="val_loss",
            max_epochs=50,
            factor=2,
            directory="../cae",
            project_name="mpr_logs",
            executions_per_trial=1,
            hyperparameters=hp,
            tune_new_entries=True,
        )

        model = tuner.get_best_models(4)[-1]
    else:
        model = ContractiveAutoEncoder(2800, args.activ)
        model.compile(Adam(learning_rate=1e-4), loss="mse", metrics=["MeanAbsoluteError"])

    training, validation = data_loader.load_tf_dataset(f"/home/jhonas/dev/mpr/obs/center{args.id}/env*", True)

    model.fit(
        training,
        epochs=150,
        validation_data=validation,
        batch_size=16,
        use_multiprocessing=True,
        callbacks=[
            tf.keras.callbacks.ModelCheckpoint(
                filepath=f"{os.path.abspath(__file__).split('mpr')[0]}mpr/models/{args.folder}", save_weights_only=True
            ),
            tf.keras.callbacks.ReduceLROnPlateau(patience=10, verbose=1, cooldown=5),
        ],
    )

    show_results(model.predict(validation))
