#!/usr/bin/env python
import math
from abc import ABC
from os import path

import numpy as np
import tensorflow as tf

from utils import data_serializer

from kerastuner import HyperParameters, Hyperband
from plotly import graph_objects as go
from plotly.subplots import make_subplots

from cae.encoder import ContractiveAutoEncoder as cae
from tensorflow.keras import Sequential, layers
from tensorflow.keras.optimizers import Adam, Adagrad
from tensorflow.keras.initializers import LecunNormal
import argparse
import os

project_path = f"{os.path.abspath(__file__).split('mpr')[0]}mpr"


class ContractiveAutoEncoder(Sequential, ABC):
    def __init__(self, hp: HyperParameters, input_shape=2800, *args, **kwargs):
        super().__init__(*args, **kwargs)

        dropout = hp.Boolean("dropout")
        dropout_value = hp.Float(
            "dropout_value", min_value=0.01, max_value=0.2, step=0.01, parent_name="dropout", parent_values=[True]
        )
        self.init_layers = []
        self.final_layers = []
        self.add(layers.InputLayer(input_shape))

        num_layers = hp.Int("num_layers", min_value=1, max_value=4)

        units = [
            hp.Int("units", min_value=512 - 60, max_value=512 + 60, step=12),
            hp.Int("units_1", min_value=200, max_value=256 + 64, step=12),
            hp.Int("units_2", min_value=128 - 60, max_value=128 + 60, step=12),
        ]
        for i in units[:num_layers]:
            self.init_layers.append(
                layers.Dense(
                    units=i, activation="selu", bias_initializer=LecunNormal(), kernel_initializer=LecunNormal()
                )
            )
            self.add(self.init_layers[-1])
            if dropout:
                self.add(layers.Dropout(dropout_value))

        self.encoder = layers.Dense(
            units=28,
            name="encoder",
            bias_initializer=LecunNormal(),
            kernel_initializer=LecunNormal(),
            activation="selu",
        )
        self.add(self.encoder)

        for i in units[:num_layers][::-1]:
            self.final_layers.append(
                layers.Dense(
                    units=i, activation="selu", bias_initializer=LecunNormal(), kernel_initializer=LecunNormal()
                )
            )
            self.add(self.final_layers[-1])
            if dropout:
                self.add(layers.Dropout(dropout_value))

        self.decoder = layers.Dense(
            units=input_shape, bias_initializer=LecunNormal(), kernel_initializer=LecunNormal(), activation="selu"
        )
        self.add(self.decoder)

        loss = self.contractive_loss

        self.lambd = hp.Choice("lambda", values=[1e-3, 1e-4, 1e-5])

        optimizer = hp.Choice("optimizer", values=["adam"])
        epsilon = hp.Choice("epsilon", values=[1e-7])

        lr = hp.Choice("learning_rate", values=[1e-4], parent_name="optimizer", parent_values=["adam"])
        optimizer_ = Adam

        self.compile(optimizer_(learning_rate=lr, epsilon=1e-7), loss=loss)
        self.code = None
        self.summary()

    def contractive_loss(self, predicted, reference):

        mse_error = tf.reduce_mean(tf.square(reference - predicted), axis=1)

        weights = self.get_layer("encoder").weights[0]
        weights = tf.transpose(weights)
        h = self.code
        dh = h * (1 - h)
        contractive = self.lambd * tf.reduce_sum(dh ** 2 * tf.reduce_sum(weights ** 2, axis=1), axis=1)
        return mse_error + contractive

    def call(self, inputs, training=None, mask=None):
        x = inputs
        for layer in self.init_layers:
            x = layer(x)
        self.code = self.encoder(x)
        x = self.code
        for layer in self.final_layers:
            x = layer(x)
        x = self.decoder(x)
        return x


def show_input(ds):
    fig = make_subplots(rows=1, cols=1, specs=[[{"type": "scatter"}]])
    reshaped = ds.reshape((-1, *input_shape))
    # x, y = get_coords(ds)
    x, y = reshaped[0, :, 0], reshaped[0, :, 1]
    fig.add_trace(go.Scatter(x=x, y=y, mode="markers", marker=dict(size=10)))
    fig.update_layout(autosize=False, width=800, height=800)
    fig.show()


def show_results(nns, ds, reshape=False, input_shape=None, prelu=False):
    if not isinstance(nns, list):
        nns = [nns]

    for nn in nns:
        predicted = nn(ds.reshape((1, -1))).numpy()
        print(f"Code Shape -- {nn.code.shape}\nPredicted Shape -- {predicted.shape}")
        if reshape:
            predicted = predicted.reshape((-1, 1400, 2))
        # if prelu:
        #     predicted = np.flip(predicted, axis=0)
        fig = go.Figure()
        # fig = make_subplots(rows=1, cols=2, specs=[[{"type": "scatter"}, {"type": "scatter"}]])
        x, y = get_coords(ds)
        fig.add_trace(go.Scatter(x=x, y=y, mode="markers", marker=dict(size=10)))
        p_x, p_y = get_coords(predicted)
        fig.add_trace(go.Scatter(x=p_x, y=p_y, mode="markers", marker=dict(size=10)))
        fig.update_layout(autosize=False, width=800, height=800)
        fig.show()


def get_coords(ds):
    ds = ds.reshape((-1, 2))
    x = ds[:, 0]
    y = ds[:, 1]
    return x, y


def get_autoencoder(_tuner=False, id=3):
    if _tuner:
        hp = HyperParameters()
        tuner_ = Hyperband(
            build_model,
            objective="val_loss",
            max_epochs=50,
            factor=2,
            directory=f"{project_path}/src/intelligence/src/cae/mpr_logs",
            project_name="mpr_logs",
            executions_per_trial=1,
            hyperparameters=hp,
            tune_new_entries=True,
        )

        models = []
        models.extend(tuner_.get_best_models(5))
        return models[id]
    else:
        return cae(2800, "prelu")


def build_model(hp: HyperParameters):
    network = ContractiveAutoEncoder(hp, input_shape=input_shape[0] * input_shape[1])
    return network


if __name__ == "__main__":
    input_shape = (1400, 2)

    parser = argparse.ArgumentParser()
    parser.add_argument("directory", type=str, help="training checkpoints output directory")
    parser.add_argument("--id", type=str, help="ID of center data to load from obs folder", default=0, required=False)
    parser.add_argument("--map_size", type=int, help="Size of map data", default=20, required=False)

    args = parser.parse_args()
    print(args)

    training, validation = data_serializer.read_cae()
    # training, validation = data_loader.load_tf_dataset(f"{project_path}/obs/center{args.id}/env*", True)
    hp = HyperParameters()
    hp.Fixed("num_layers", value=3)

    tuner = Hyperband(
        build_model,
        objective="val_loss",
        max_epochs=50,
        factor=4,
        directory=args.directory,
        project_name="mpr_logs",
        executions_per_trial=2,
        hyperparameters=hp,
        tune_new_entries=True,
    )
    tuner.search(
        training,
        validation_data=validation,
        batch_size=32,
        callbacks=[tf.keras.callbacks.ReduceLROnPlateau(patience=5, cooldown=3, verbose=1)],
    )
    tuner.results_summary()

    #    cae = get_autoencoder()
    #    ckpt = tf.train.latest_checkpoint(f"{project_path}/models/encoder")
    #    cae.load_weights(ckpt).expect_partial()

    ds = validation.take(1).unbatch().as_numpy_iterator()
    ds = next(ds)[0]
    models = tuner.get_best_models(5)
    # models.append(tuner.get_best_models(50)[-1])
    # show_results(
    #     models,
    #     ds,
    #     reshape=True,
    #     input_shape=input_shape,
    #     prelu=True,
    # )
