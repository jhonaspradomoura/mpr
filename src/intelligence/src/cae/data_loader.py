from cae import map_loader
import numpy as np
import tensorflow as tf


def load_from_npy(path, map_size):
    dataset = map_loader.from_generated_npy(path)
    dataset = dataset.reshape((-1, dataset.shape[1] * dataset.shape[2]))
    dataset = np.interp(dataset, (-map_size / 2, map_size / 2), (0, 1))
    dataset = dataset.astype(np.float32)
    training, validation = split_dataset(dataset, fraction=0.15)
    return training, validation


def load_tf_dataset(path, normalize):
    dataset = tf.data.Dataset.list_files(path)
    dataset = dataset.map(
        lambda x: tf.numpy_function(map_ds, [x, normalize], [tf.double, tf.double]), tf.data.experimental.AUTOTUNE
    )
    validation = dataset.take(int(55000 * 0.1))
    training = dataset.skip(int(55000 * 0.1))

    training = training.shuffle(1000).cache().batch(32).prefetch(tf.data.experimental.AUTOTUNE)
    validation = validation.shuffle(1000).cache().batch(32).prefetch(tf.data.experimental.AUTOTUNE)

    return training, validation


def map_ds(data, normalize):
    dataset = np.load(data, allow_pickle=True)
    if normalize:
        dataset = np.interp(dataset.reshape((-1,)), (-20, 20), (0, 1))
    return dataset, dataset


def split_dataset(images, fraction=0.25):
    choices = np.random.choice(len(images) - 1, int(len(images) * fraction), replace=False)
    validation = images[choices]
    training = np.delete(images, choices, axis=0)
    return training, validation
