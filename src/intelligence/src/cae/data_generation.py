import math
from datetime import datetime

import numpy as np
import os
from itertools import product, combinations

from geometry_msgs.msg import Point32
# from matplotlib import pyplot as plt
import argparse
import rospy
from path_generator.srv import changeMap, changeMapRequest
from geometry_msgs.msg import Point32
from mpnet.srv import npMapRequest, npMapList, npMapListResponse, npMapRequestResponse

center_path = f"{os.path.abspath(__file__).split('mpr')[0]}mpr/obs"


def gen_centers():
    xs = np.random.choice(np.arange(start=-map_size[0] / 2 + size[0] / 2, stop=map_size[0] / 2 - size[0] / 2,
                                    step=resolution),
                          num_obst_regions)
    ys = np.random.choice(np.arange(start=-map_size[1] / 2 + size[1] / 2, stop=map_size[1] / 2 - size[1] / 2,
                                    step=resolution),
                          num_obst_regions)
    centers = np.array(list(zip(xs, ys)))
    return centers


def save_center(centers: np.ndarray):
    dir_list = os.listdir(f"{center_path}")
    dir_list = [folder for folder in dir_list if os.path.isdir(f"{center_path}/{folder}") and "center" in folder]
    if dir_list:
        try:
            folder_id = max([int(folder.split("center")[1]) for folder in dir_list]) + 1
        except ValueError:
            folder_id = 0
    else:
        folder_id = 0
    os.mkdir(f"{center_path}/center{folder_id}")
    np.save(f"{center_path}/center{folder_id}/center", centers)
    return f"{center_path}/center{folder_id}"


def load_center(path: str):
    centers = np.load(f"{path}/center.npy")
    return centers


def permutate(centers: np.ndarray, work_path):
    obstacles_combinations = list(combinations(centers, r=7))
    chosen_env = np.random.choice(len(obstacles_combinations), num_envs, replace=False)
    chosen_env = [obstacles_combinations[x] for x in chosen_env]
    i = 0
    for env in chosen_env:
        region = fill_region(env)
        save_map(region, env, work_path)
        print(f"Progress: {i}/{num_envs}", end='\r')
        i += 1


def fill_region(obstacles: np.ndarray):
    region = np.empty(shape=cloud_shape, dtype=np.float32)
    region.fill(0)
    obs_list = []
    for center_x, center_y in obstacles:
        for _ in range(num_points_inside):
            size_x = size[0]
            size_y = size[1]
            # size_x = size[0] if abs(center_x) + abs(size[0] / 2) < map_size[0] / 2 else size[0] / 2 + map_size[
            #     0] / 2 - abs(center_x)
            # size_y = size[1] if abs(center_y) + abs(size[1] / 2) < map_size[1] / 2 else size[1] / 2 + map_size[
            #     1] / 2 - abs(center_y)
            random_x = np.round(np.random.rand() * size_x - size_x / 2 + center_x, 2)
            random_y = np.round(np.random.rand() * size_y - size_y / 2 + center_y, 2)
            obs_list.append((random_x, random_y))
    region[:] = obs_list
    return region


def plot_region(region, normalized=False):
    fig = plt.figure()
    plt.scatter(region[:, 0], region[:, 1])
    if normalized:
        plt.xlim(0, 1)
        plt.ylim(0, 1)
    else:
        plt.xlim(-map_size[0] / 2, map_size[0] / 2)
        plt.ylim(-map_size[1] / 2, map_size[1] / 2)
    fig.show()


def save_map(region, obstacles_perm, work_path):
    path_files = os.listdir(f"{work_path}")
    path_files = [file for file in path_files if os.path.isfile(f"{work_path}/{file}")]
    if path_files:
        try:
            file_id = max([int(f.split("env")[1].split(".npy")[0]) for f in path_files if "env" in f]) + 1
        except ValueError:
            file_id = 0
    else:
        file_id = 0
    
    map_info = np.array([size, num_obstacles, num_obst_regions, map_size, resolution, cloud_shape])
    
    np.save(f"{work_path}/env{file_id}.npy", region)
    np.save(f"{work_path}/perm{file_id}.npy", obstacles_perm)
    np.save(f"{work_path}/info{file_id}.npy", map_info)


def load_env(work_path, env_id):
    env = np.load(f"{work_path}/env{env_id}.npy")
    perms = np.load(f"{work_path}/perm{env_id}.npy")
    info = np.load(f"{work_path}/info{env_id}.npy", allow_pickle=True)
    return env, perms, info


def env_to_map_matrix(perms, width, height, map_size_, resolution_, size_):
    matrix = np.zeros(shape=width * height, dtype=np.uint8)
    for center in perms:
        for x in np.arange(center[0] - size_[0] / 2, center[0] + size_[0] / 2, resolution_):
            for y in np.arange(center[1] - size_[1] / 2, center[1] + size_[1] / 2, resolution_):
                ind_y = int(np.round_(4 * (y + map_size_[1] / 2)) / (4 * resolution_))
                ind_x = int(np.round(4 * (x + map_size_[0] / 2)) / (4 * resolution_))
                matrix[ind_y * width + ind_x] = 100
    return matrix


def send_to_server(env, perm, info):
    serv_proxy = rospy.ServiceProxy("/map/set", changeMap)
    serv_msg = changeMapRequest()
    size_, _, _, map_size_, resolution_, cloud_shape_ = info
    
    width = math.ceil(map_size_[0] / resolution_)
    height = math.floor(map_size_[1] / resolution_)
    
    serv_msg.map.data = env_to_map_matrix(perm, width, height, map_size_, resolution_, size_)
    
    serv_msg.map.info.resolution = resolution_
    serv_msg.map.info.origin.position.x = -map_size_[0] / 2
    serv_msg.map.info.origin.position.y = -map_size_[1] / 2
    serv_msg.map.info.origin.position.z = 0.108
    serv_msg.map.info.origin.orientation.w = 1
    serv_msg.map.info.width = width
    serv_msg.map.info.height = height
    serv_msg.map.header.stamp = rospy.Time.now()
    serv_msg.map.header.frame_id = "map"
    
    serv_proxy.call(serv_msg)
    return env


def service(request):
    env, perm, info = load_env(f"{center_path}/center{request.center_id}", request.map_id)
    env = send_to_server(env, perm, info)
    response = npMapRequestResponse()
    for data in env:
        p = Point32()
        p.x = data[0]
        p.y = data[1]
        response.obstacles.points.append(p)
    response.result = True
    for center in perm:
        p = Point32()
        p.x = center[0]
        p.y = center[1]
        response.centers.append(p)
    return response


def choose_maps(request):
    files_ls = os.listdir(f"{center_path}/center{request.center_id}")
    files_ls = np.array([files.split('env')[1].split('.npy')[0] for files in files_ls if 'env' in files])
    chosen = np.random.choice(files_ls, request.qtt, replace=False)
    ret = npMapListResponse()
    ret.chosenMaps = chosen.tolist()
    return ret


def main(args):
    if not args:
        rospy.init_node('data_gen')
        rospy.logwarn("Data generator initialized")
        map_request = rospy.Service("/map/numpy", npMapRequest, service)
        map_choice = rospy.Service("/map/choose", npMapList, choose_maps)
        rospy.spin()
    elif args.gen:
        centers = gen_centers()
        work_path = save_center(centers)
        permutate(centers, work_path)
    elif args.load:
        work_path = f"{center_path}/center{args.cfolder}"
        if args.load_type == "envs":
            env, perm, info = load_env(work_path, args.env_id)
            pass
    else:
        raise AttributeError("You must define either --gen,  --load or --ros")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--gen", nargs="?", const=True, help="Generate new centers and environments", type=bool,
                        required=False)
    
    parser.add_argument("--load", nargs="?", const=True, help="Load centers and generate environments", type=str,
                        required=False)
    parser.add_argument("--load_type", default="center", choices=["center", "envs"], required=False)
    parser.add_argument("--cfolder", type=str, help="Center folder to load from", required=False)
    parser.add_argument("--env_id", type=str, help="ID of env to be loaded", required=False)
    
    parser.add_argument("--obst_size", type=int, default=5, help="Size of squared obstacle side",
                        required=False)
    parser.add_argument("--num_obstacles", type=int, default=7, help="Number of obstacles", required=False)
    parser.add_argument("--num_obst_regions", type=int, default=20, help="Number of possible obstacle centers",
                        required=False)
    parser.add_argument("--num_envs", type=int, default=200, help="Number of environments to be generated",
                        required=False)
    
    parser.add_argument("--map_size", type=int, default=20, help="Size of map", required=False)
    parser.add_argument("--resolution", type=float, default=0.25, help="Map resolution", required=False)
    
    parser.add_argument("--cloud_shape", type=int, default=1400, help="Number of points in the point cloud",
                        required=False)
    
    args = parser.parse_args()
    
    size = args.obst_size, args.obst_size
    num_obstacles = args.num_obstacles
    num_obst_regions = args.num_obst_regions
    num_envs = args.num_envs
    map_size = args.map_size, args.map_size
    resolution = args.resolution
    cloud_shape = args.cloud_shape, 2
    
    array_shape = int(map_size[0] * 1 / resolution), int(map_size[1] * 1 / resolution)
    num_points_inside = int(cloud_shape[0] / num_obstacles)
    main(args)
