import os
from typing import Dict

import numpy as np
import yaml
# from matplotlib import pyplot as plt
# import cv2
import math


def open_image(map_file):
    with open(map_file, 'rb') as file:
        file.readline()
        (width, height) = [int(i) for i in file.readline().split()]
        depth = int(file.readline())
        assert depth <= 255
        
        raster = []
        for _ in range(height):
            row = []
            for y in range(width):
                row.append(ord(file.read(1)))
            raster.append(row)
        raster = np.array(raster)
        return raster


def open_map(file) -> Dict:
    map_params = {}
    with open(file) as file:
        f = yaml.load(file, yaml.FullLoader)
        map_params["resolution"] = f["resolution"]
        map_params["occupied_thresh"] = f["occupied_thresh"]
        map_params["free_thresh"] = f["free_thresh"]
        map_params["origin"] = f["origin"]
    return map_params


def image_to_xyz(img, params):
    xyz_map = []
    
    for x_rotation in (1, -1):
        for y_rotation in (1, -1):
            rotated_map = []
            for i in range(img.shape[0]):
                for j in range(img.shape[1]):
                    coord = [0, 0, 0.5]
                    coord[0] = x_rotation * (params["origin"][0] + j * params["resolution"])
                    coord[1] = y_rotation * (params["origin"][1] + i * params["resolution"])
                    if (255 - img[i][j]) / 255 < params["free_thresh"] \
                            and (255 - img[i - 1][j]) / 255 < params["free_thresh"] \
                            and (255 - img[i][j - 1]) / 255 < params["free_thresh"] \
                            and (255 - img[i - 1][j - 1]) / 255 < params["free_thresh"] \
                            and (255 - img[i + 1][j]) / 255 < params["free_thresh"] \
                            and (255 - img[i][j + 1]) / 255 < params["free_thresh"] \
                            and (255 - img[i + 1][j + 1]) / 255 < params["free_thresh"] \
                            and (255 - img[i + 1][j - 1]) / 255 < params["free_thresh"] \
                            and (255 - img[i - 1][j + 1]) / 255 < params["free_thresh"]:
                        rotated_map.append([coord[0], coord[1]])
        xyz_map.append(rotated_map)
    # xyz_map = np.array(xyz_map, dtype=np.float32)
    # plot(xyz_map)
    return xyz_map


def load_maps(as_image=False):
    xyz_maps = []
    root_dir = f"{os.path.dirname(os.path.abspath(__file__))}/../../../navigation/maps/cropped"
    maps = os.listdir(root_dir)
    maps = {f"{root_dir}/{_map[:_map.find('.')]}" for _map in maps}
    for map_file in maps:
        if not as_image:
            img = open_image(f"{map_file}.pgm")
            maps_params = open_map(f"{map_file}.yaml")
            xyz_maps.extend(image_to_xyz(img, maps_params))
        else:
            img = cv2.imread(f"{map_file}.pgm", cv2.IMREAD_GRAYSCALE)
            xyz_maps.append(img)
            for ops_x, ops_y in [(-1, 1), (1, -1), (-1, -1)]:
                new_img = img.copy()
                for i in range(len(new_img)):
                    for j in range(len(new_img[i])):
                        new_img[ops_x * i, ops_y * j] = img[i, j]
                xyz_maps.append(new_img)
    return xyz_maps


def plot_img(data, qtt):
    fig = plt.figure()
    num_rows = int(math.ceil(qtt / 2))
    for i in range(qtt):
        ax = fig.add_subplot(num_rows, 2, i + 1)
        ax.imshow(data[i])
    fig.show()


def plot(xyz_data):
    fig = plt.figure()
    idx = 1
    for data in xyz_data[0:4]:
        ax = fig.add_subplot(2, 2, idx, projection='3d')
        x = data[:, 0]
        y = data[:, 1]
        ax.scatter(x, y)
        ax.set_xlabel('X Label')
        ax.set_ylabel('Y Label')
        idx += 1
    plt.show()


def from_generated_npy(folder):
    npy_files = os.listdir(folder)
    env_files = [file for file in npy_files if 'env' in file]
    envs = []
    for file in env_files:
        envs.append(np.load(f"{folder}/{file}"))
    envs = np.array(envs)
    return envs


if __name__ == '__main__':
    arr = from_generated_npy("/home/jhonas/dev/mpr/obs/center0")
    plot(arr)
