import os
from planner import data_loader
from planner import model
from cae.encoder import ContractiveAutoEncoder
import numpy as np
import tensorflow as tf
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, TensorBoard, ReduceLROnPlateau
import argparse

project_path = f"{os.path.abspath(__file__).split('mpr')[0]}mpr"


def evaluate(data, reference, map_, planner, encoded, normalize):
    paths_end = reference[-1]
    result_1 = [reference[0].reshape((1, -1))]
    current_result_1 = np.array([0])
    current_input_1 = data[0]
    result_2 = [paths_end.reshape((1, -1))]
    current_result_2 = np.array([-1])
    current_input_2 = current_input_1.copy()
    tmp = current_input_1[-2:]
    current_input_2[-2:] = current_input_1[-4: -2]
    current_input_2[-4:-2] = tmp
    count = 0
    while count < 200 and np.linalg.norm(current_result_1 - current_result_2) > 1e-4:
        current_result_1 = planner(current_input_1.reshape((1, -1)), training=False)
        current_input_1[-4:-2] = current_result_1
        if np.all(current_result_1 != result_1[-1]):
            result_1.append(current_result_1.numpy())
        current_result_2 = planner(current_input_2.reshape((1, -1)), training=False)
        current_input_2[-4:-2] = current_result_2
        if np.all(current_result_2 != result_2[-1]):
            result_2.append(current_result_2.numpy())
        count += 1
    result_1.extend(result_2[::-1])
    result_1 = np.array(result_1).reshape((-1, 2))
    
    data_loader.plot(map_, reference, result_1, normalize, encoded)


def main(args):
    num_epochs = args.epochs
    batch_size = args.batch_size
    learning_rate = args.learning_rate
    normalize = True if args.normalize else False
    
    optimizer = Adam(learning_rate=learning_rate)
    
    planner = model.PlanningModule(input_shape=32, output_size=2, dropout=args.dropout,
                                   activation=args.activation,
                                   last_dp=args.last_dropout,
                                   no_output_actv=args.no_output_activ)
    
    # training = tf.data.Dataset.from_generator(data_loader.generate_data,args=[140, 4000, args.normalize,
    # args.center_id, 'training'],output_types=(tf.float32, tf.float32),output_shapes=((32,), (2,)))
    
    # validation = tf.data.Dataset.from_generator(data_loader.generate_data, args=[30, 1000, args.normalize,
    # args.center_id, 140, 4000, 'validation'],output_types=(tf.float32, tf.float32), output_shapes=((32,), (2,)))
    training, validation = data_loader.load_from_files(args.center_id, normalize, args.batch_size, args.ae)
    planner.summary()
    initial_epoch = 0
    if args.load:
        latest_ckpt = tf.train.latest_checkpoint(f"{project_path}/models/{args.f}")
        print(latest_ckpt)
        planner.load_weights(latest_ckpt)
    planner.compile(optimizer, loss=args.loss,
                    metrics=['Accuracy', 'RootMeanSquaredError', 'MeanAbsoluteError'])
    
    planner.fit(training, epochs=num_epochs, batch_size=batch_size, validation_data=validation, callbacks=[
        ModelCheckpoint(
                f"{project_path}/models/{args.f}/planner" + ".{epoch:03d}-{val_loss:.7f}",
                monitor='val_loss', save_weights_only=True, verbose=1, initial_epoch=initial_epoch),
        ReduceLROnPlateau(factor=0.2, patience=6, cooldown=1, min_delta=1e-4, verbose=1),
        EarlyStopping(min_delta=1e-4, patience=20, verbose=1),])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', default=50, type=int, help='Number of epochs to train')
    parser.add_argument('--batch_size', default=100, type=int, help='Batch size to train with')
    parser.add_argument('--learning_rate', default=1e-4, type=float, help='Learning rate')
    parser.add_argument('--loss', default='mse', type=str, help='Loss function')
    parser.add_argument('-f', default='02', type=str, help='Folder to save model in', required=False)
    parser.add_argument('--dropout', default=0.2, type=float, help='Dropout rate', required=False)
    parser.add_argument('--last_dropout', const=True, nargs="?", type=bool,
                        help='Dropout layer before output layer')
    parser.add_argument('--no_output_activ', const=True, nargs="?", type=bool, help='Dropout rate')
    parser.add_argument('--initializer', default="glorot", type=str, help='Kernel and bias initializer',
                        required=False)
    parser.add_argument('--activation', default="selu", type=str, help='Activation function',
                        required=False)
    parser.add_argument("--center_id", default=0, type=int, help='Which center folder to get obs from')
    parser.add_argument('--normalize', const=True, nargs="?", type=bool, help='Normalize data')
    parser.add_argument('--load', const=True, nargs="?", type=bool, help='Load ckpt')
    parser.add_argument('--eval', const=True, nargs="?", type=bool, help='Evaluate network')
    parser.add_argument('--ds', default="validation", type=str, help='Dataset to eval from')
    parser.add_argument('--ae', default="encoder", type=str, help='Autoencoder model to load from')
    
    args = parser.parse_args()
    
    main(args)
