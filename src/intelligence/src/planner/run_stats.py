import argparse
import os
from typing import Dict, List, Union
import numpy as np
import json

from nav_msgs.msg import MapMetaData, OccupancyGrid
from planner.model import PlanningModule as pnet
from planner.neural_planning import plan
from cae.encoder import ContractiveAutoEncoder
from cae.hyperparams_cae import ContractiveAutoEncoder as hp_cae

import tensorflow as tf
from kerastuner import HyperParameters, Hyperband

import roslaunch
import rospy
from path_generator.srv import changeMap, changeMapRequest
from mpnet.srv import npMapRequest, npMapRequestRequest
from nav_msgs.srv import GetPlanRequest, GetPlan
from sensor_msgs.msg import NavSatFix, Imu
from std_msgs.msg import Header

from search_space.search_space import SearchSpace
from rrt.rrt_star_bid import RRTStarBidirectional

from time import time

import plotly.graph_objects as go
from tf2_msgs.msg import TFMessage

project_path = f"{os.path.abspath(__file__).split('mpr')[0]}mpr"
debug = False


def get_input_envs(num_envs, dataset, env_seen):
    all_envs = os.listdir(f"{project_path}/paths/{dataset}")
    all_envs = [env for env in all_envs if env != "npy"]
    env_sizes = []
    for env in all_envs:
        size = len(os.listdir(f"{project_path}/paths/{dataset}/{env}"))
        env_sizes.append(True if size > 200 else False)
    env_chances = []
    for size_ref in env_sizes:
        # distribui as chances de cada ambiente para que sejam escolhidos 50% de ambientes ja visto e 50% de nao
        # vistos
        # env_chances.append(0.01 if size_ref else 0.005)
        if env_seen:
            env_chances.append(True if not size_ref else False)
        else:
            env_chances.append(True if size_ref else False)
    env_chances = np.array(env_chances)
    # chosen_envs = np.random.choice(all_envs, num_envs, replace=False, p=env_chances)
    chosen_envs = np.random.choice(all_envs, num_envs, replace=False, p=env_chances / np.count_nonzero(env_chances))
    return chosen_envs


def get_input_paths(env_files, num_paths_per_env, dataset):
    chosen_paths = []
    for env in env_files:
        paths = os.listdir(f"{project_path}/paths/{dataset}/{env}")
        chosen_paths.append(np.random.choice(paths, num_paths_per_env, replace=False))

    return chosen_paths


def load_input_envs(env_files, center_id, data_dict):
    """
    Load environment files from the obs/center folder.
    They are normalized to be use by the autoencoder module later.
    """
    for env in env_files:
        env_num = env.split("_")[-1]
        loaded_env = np.load(f"{project_path}/obs/center{center_id}/env{env_num}.npy", allow_pickle=True)
        perm = np.load(f"{project_path}/obs/center{center_id}/perm{env_num}.npy", allow_pickle=True)
        info = np.load(f"{project_path}/obs/center{center_id}/info{env_num}.npy", allow_pickle=True)
        norm_env = np.interp(loaded_env, (-20, 20), (0, 1))
        data_dict[env] = {"env": {"norm": norm_env, "original": loaded_env, "perm": perm, "info": info}}


def load_input_paths(env_files, path_files, dataset, data_dict):
    """
    Data dict has the following format:
        {env_123: {env: [1,2,3,4,....]}}
    This function returns it in the following format:
        {env_123: {env: [1,2,3,4,....], paths: [[10, 20, 30, 40], ....]}}
    """
    for env, paths_in_env in zip(env_files, path_files):
        data_dict[env]["paths"] = []
        for path in paths_in_env:
            loaded_path = np.load(f"{project_path}/paths/{dataset}/{env}/{path}")
            loaded_path = np.array([loaded_path[:2], loaded_path[-2:]])
            loaded_path = loaded_path.reshape([-1])
            norm_path = np.interp(loaded_path, (-20, 20), (0, 1))
            data_dict[env]["paths"].append(norm_path)
        data_dict[env]["paths"] = np.array(data_dict[env]["paths"])


def build_model(hp: HyperParameters):
    network = hp_cae(hp, input_shape=2800)
    return network


def setup_models(model_configs):
    models = []

    hp = HyperParameters()
    tuner_ = Hyperband(
        build_model,
        objective="val_loss",
        max_epochs=50,
        factor=2,
        directory="../cae",
        project_name="mpr_logs",
        executions_per_trial=1,
        hyperparameters=hp,
        tune_new_entries=True,
    )

    models_tuner = []
    if debug:
        tuner_encoder = None
    else:
        models_tuner.extend(tuner_.get_best_models(5))
        tuner_encoder = models_tuner[3]

        tuner_ckpt = tf.train.latest_checkpoint(f"{project_path}/models/selu_4")
        tuner_encoder.load_weights(tuner_ckpt).expect_partial()

    prelu_encoder = ContractiveAutoEncoder(2800, "prelu")
    prelu_ae_ckpt = tf.train.latest_checkpoint(f"{project_path}/models/encoder")
    prelu_encoder.load_weights(prelu_ae_ckpt).expect_partial()

    for planning_configs in model_configs["planning"]:
        planner = pnet(
            32,
            2,
            dropout=planning_configs["dropout"],
            activation=planning_configs["activation"],
            last_dp=planning_configs["lastdp"],
            no_output_actv=planning_configs["linear"],
        )
        latest_ckpt = tf.train.latest_checkpoint(
            f"{project_path}/models/{planning_configs['activation']}_{planning_configs['dropout']}_"
            f"{'lastdp_linear' if planning_configs['lastdp'] else 'linear'}"
            f"{'_ae' if planning_configs['encoder'] == 'tuner' else ''}_astar"
        )
        if not debug:
            planner.load_weights(latest_ckpt)
        encoder = tuner_encoder if planning_configs["encoder"] == "tuner" else prelu_encoder
        models.append((encoder, planner))
    return models


def change_map_ros(env_id, center_id):
    serv_proxy = rospy.ServiceProxy("/map/numpy", npMapRequest)
    serv_msg = npMapRequestRequest()
    serv_msg.center_id = center_id
    serv_msg.map_id = env_id

    serv_result = serv_proxy.call(serv_msg)

    rospy.wait_for_message("/map", OccupancyGrid, 1)
    rospy.wait_for_message("/map_metadata", MapMetaData, 1)

    gps_msg = NavSatFix()
    imu_msg = Imu()

    gps_msg.header.stamp = rospy.Time.now()
    gps_msg.header.frame_id = "pionner3at/gps"
    gps_msg.longitude = 0
    gps_msg.latitude = 0
    gps_msg.altitude = 0.108
    gps_msg.status.service = 1

    imu_msg.header.stamp = rospy.Time.now()
    imu_msg.header.frame_id = "pioneer3at/inertial_unit"
    imu_msg.angular_velocity_covariance = [-1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    imu_msg.linear_acceleration_covariance = [-1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    imu_msg.orientation.w = 1
    imu_msg.orientation.x = 0
    imu_msg.orientation.y = 0
    imu_msg.orientation.z = 0

    gps_pub = rospy.Publisher("/pioneer3at/gps/values", NavSatFix, latch=True, queue_size=1)
    imu_pub = rospy.Publisher("/pioneer3at/inertial_unit/roll_pitch_yaw", Imu, latch=True, queue_size=1)

    time_wait = time()
    while True:
        try:
            rospy.wait_for_message("/tf", TFMessage, 0.5)
            break
        except rospy.ROSException:
            pass

        gps_pub.publish(gps_msg)
        imu_pub.publish(imu_msg)
    return time() - time_wait


def request_path(path) -> List:
    path = np.interp(path, (0, 1), (-20, 20))
    serv_proxy = rospy.ServiceProxy("/move_base/make_plan", GetPlan)
    serv_msg = GetPlanRequest()
    serv_msg.start.header = Header()
    serv_msg.start.header.frame_id = "map"
    serv_msg.start.header.stamp = rospy.Time.now()
    serv_msg.start.pose.position.x = path[0]
    serv_msg.start.pose.position.y = path[1]
    serv_msg.start.pose.position.z = 0.108
    serv_msg.start.pose.orientation.y = 1

    serv_msg.goal.header = Header()
    serv_msg.goal.header.frame_id = "map"
    serv_msg.goal.header.stamp = rospy.Time.now()
    serv_msg.goal.pose.position.x = path[2]
    serv_msg.goal.pose.position.y = path[3]
    serv_msg.goal.pose.position.z = 0.108
    serv_msg.goal.pose.orientation.y = 1

    serv_msg.tolerance = 0.05

    plan_result = serv_proxy.call(serv_msg)
    return plan_result.plan.poses


def request_ros_planning(env_id, center_id, path) -> List:
    plan_result = request_path(path)

    return plan_result


def launch_ros():
    roslaunch.Master()
    rospy.init_node("run_stats", anonymous=True)
    uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
    roslaunch.configure_logging(uuid)
    launch = roslaunch.parent.ROSLaunchParent(uuid, [f"{project_path}/src/intelligence/launch/path_request.launch"])

    return launch


def plan_to_points(ros_plan):
    plan_in_points = []
    for pose in ros_plan:
        plan_in_points.append([pose.pose.position.x, pose.pose.position.y])

    return np.array(plan_in_points)


def define_rrt(search_space, origin, goal):
    edges = np.array([(8, 4)])
    max_samples = 10240
    smallest_edge = 1
    prc = 0.1
    rewire_count = 64
    rrt = RRTStarBidirectional(
        search_space, edges, tuple(origin), tuple(goal), max_samples, smallest_edge, prc, rewire_count
    )
    return rrt


def set_env_rrt(input_data):
    resolution = 0.1

    perm = input_data["env"]["perm"] / resolution
    info = input_data["env"]["info"]

    size, _, _, map_size, _, _ = info
    obst_size = size[0] / resolution

    lower_bounds = perm - obst_size / 2
    upper_bounds = perm + obst_size / 2
    bounds = []
    for lower, upper in zip(lower_bounds, upper_bounds):
        bounds.append((*lower, *upper))
    bounds = np.array(bounds)

    dimensions = np.array(
        [
            [-map_size[0] / (2 * resolution), map_size[0] / (2 * resolution)],
            [-map_size[1] / (2 * resolution), map_size[1] / (2 * resolution)],
        ]
    )

    search_space = SearchSpace(dimensions, bounds)
    return search_space, resolution


def plan_rrt(input, origin_goal):
    search_space, resolution = set_env_rrt(input)
    origin_goal = np.interp(origin_goal, (0, 1), (-20, 20)) / resolution
    rrt = define_rrt(search_space, origin_goal[:2], origin_goal[-2:])
    resulting_path = rrt.rrt_star_bidirectional()
    return np.array(resulting_path) * resolution if resulting_path is not None else None


def mpnet_plan(input_data, path, model) -> Union[List, str]:
    """
    Formato de input_data:
        input_data => {"env": {"norm": [...], "original": [...], "perm": [...], "info": [...]}, "paths": [0, 1,
        2, 3]}
    Formato de path:
        path => [0, 1, 2, 3]
    Formato de model:
        model => [(encoder, planner)]
    """
    encoded_env = model[0].encoder(input_data["env"]["norm"].reshape((1, -1)))

    mpnet_input = np.zeros((32,), dtype=np.float32)
    mpnet_input[:28] = encoded_env
    plan_result = plan(
        model[1], path[:2], path[-2:], np.interp(input_data["env"]["perm"], (-20, 20), (0, 1)), mpnet_input
    )
    return plan_result


def time_plan(function, *args, **kwargs):
    tic = time()
    planner_return = function(*args, **kwargs)
    toc = time() - tic
    return toc, planner_return


def plot(perm, info, paths: list, plot_configs: List[Dict]):
    fig = go.FigureWidget()
    obst_size = info[0][0]

    env = [[], []]
    for obst_center in perm:
        x, y = obst_center
        env[0].extend(
            [x - obst_size / 2, x - obst_size / 2, x + obst_size / 2, x + obst_size / 2, x - obst_size / 2, None]
        )
        env[1].extend(
            [y - obst_size / 2, y + obst_size / 2, y + obst_size / 2, y - obst_size / 2, y - obst_size / 2, None]
        )
    env = np.array(env)
    x = env[0]
    y = env[1]
    fig.add_trace(go.Scatter(x=x, y=y, fill="toself", fillcolor="black", name="obstacle"))
    for path, config in zip(paths, plot_configs):
        if path is not None:
            if config.pop("norm", False):
                path = np.interp(path, (0, 1), (-20, 20))
            path = path.reshape((-1, 2))

            res_x = path[:, 0]
            res_y = path[:, 1]
            fig.add_trace(go.Scatter(x=res_x, y=res_y, **config))
    fig.show()


def save_statistics(stats):
    with open(f"{project_path}/performance_data/statistics.json", "w") as file:
        json.dump(stats, file)


def run_statistics(args):
    # carregar argumentos de linha de comando
    dataset = args.ds
    center_id = args.cid

    # obter X ambientes de validação onde X/2 sao ambientes já vistos e X/2 sao ambiente nunca vistos
    seen_env_files = get_input_envs(50, dataset, True)
    unseen_env_files = get_input_envs(50, dataset, False)

    # obter Y caminhos de cada um desses ambientes
    seen_path_files = get_input_paths(seen_env_files, 40, dataset)
    unseen_path_files = get_input_paths(unseen_env_files, 40, dataset)

    # carregar arquivos de ambientes e caminhos
    input_data = {"seen": {}, "unseen": {}}

    load_input_envs(seen_env_files, center_id, input_data["seen"])
    load_input_paths(seen_env_files, seen_path_files, dataset, input_data["seen"])
    load_input_envs(unseen_env_files, center_id, input_data["unseen"])
    load_input_paths(unseen_env_files, unseen_path_files, dataset, input_data["unseen"])

    # definir cada um dos modulos de codificacao e planejamento
    model_config = {
        "planning": [
            {
                "dropout": 0.5,
                "activation": "prelu",
                "lastdp": False,
                "linear": True,
                "encoder": "prelu",
                "name": "prelu_0.5_linear",
            },
            {
                "dropout": 0.5,
                "activation": "prelu",
                "lastdp": False,
                "linear": True,
                "encoder": "tuner",
                "name": "prelu_0.5_linear_ae",
            },
            {
                "dropout": 0.2,
                "activation": "selu",
                "lastdp": True,
                "linear": True,
                "encoder": "prelu",
                "name": "selu_0.2_lastdp_linear",
            },
            {
                "dropout": 0.2,
                "activation": "selu",
                "lastdp": True,
                "linear": True,
                "encoder": "tuner",
                "name": "selu_0.2_lastdp_linear_ae",
            },
        ],
        "encoding": ["prelu", "tuner"],
    }
    models = setup_models(model_config)  # models => [(encoder, planner),...]

    # definir o planejadort-> ros
    launch = launch_ros()
    launch.start()

    rospy.wait_for_service("/map/numpy")

    # inicializa vetor de resultados
    statistics = {
        "seen": {
            "timing": {"rrt": [], "ros": []},
            "results": {"rrt": {"success": 0, "failure": 0}, "ros": {"success": 0, "failure": 0}},
        },
        "unseen": {
            "timing": {"rrt": [], "ros": []},
            "results": {"rrt": {"success": 0, "failure": 0}, "ros": {"success": 0, "failure": 0}},
        },
    }
    for model_desc in model_config["planning"]:
        statistics["seen"]["timing"][model_desc["name"]] = []
        statistics["seen"]["results"][model_desc["name"]] = {
            "success": 0,
            "replanning success": 0,
            "replanning failure": 0,
            "failure": 0,
        }
        statistics["unseen"]["timing"][model_desc["name"]] = []
        statistics["unseen"]["results"][model_desc["name"]] = {
            "success": 0,
            "replanning success": 0,
            "replanning failure": 0,
            "failure": 0,
        }

    count = 0
    # para cada conjunto (N: {X, Y}):
    for seen_key, seen_input_data in input_data.items():
        for env_id, sample_input in seen_input_data.items():
            count += 1
            print(f"{seen_key}: \t ID: {count}")
            time_ros_env, time_wait = time_plan(change_map_ros, env_id.split("_")[-1], center_id)
            time_ros_env -= time_wait
            for path in sample_input["paths"]:
                #  cronometragem ros
                ros_time, ros_plan_results = time_plan(request_ros_planning, env_id.split("_")[-1], center_id, path)
                statistics[seen_key]["timing"]["ros"].append(ros_time + time_ros_env)
                if len(ros_plan_results) > 0:
                    statistics[seen_key]["results"]["ros"]["success"] += 1
                else:
                    statistics[seen_key]["results"]["ros"]["failure"] += 1
                ros_plan_results = plan_to_points(ros_plan_results)

                #   cronometragem rrt
                rrt_time, rrt_plan_result = time_plan(plan_rrt, sample_input, path)
                statistics[seen_key]["timing"]["rrt"].append(rrt_time)
                if rrt_plan_result is not None:
                    statistics[seen_key]["results"]["rrt"]["success"] += 1
                else:
                    statistics[seen_key]["results"]["rrt"]["failure"] += 1

                #   cronometragem mpnet
                mpnet_paths = []
                for n, mpnet_model in enumerate(models):
                    mpnet_time, mpnet_result = time_plan(mpnet_plan, sample_input, path, mpnet_model)
                    mpnet_path, result = mpnet_result
                    model_name = model_config["planning"][n]["name"]
                    statistics[seen_key]["timing"][model_name].append(mpnet_time)
                    statistics[seen_key]["results"][model_name][result] += 1
                    mpnet_paths.append(mpnet_path)

                # plotar algumas amostras com todos os planejadores para verificar funcionamento
                if np.random.choice((True, False), 1, p=(0.005, 0.995)):
                    plot(
                        sample_input["env"]["perm"],
                        sample_input["env"]["info"],
                        [ros_plan_results, rrt_plan_result, *mpnet_paths],
                        [
                            {"name": "ros", "mode": "lines+markers", "line": {"color": "darkred"}},
                            {"name": "rrts", "mode": "lines+markers", "line": {"color": "blue"}},
                            *[
                                {
                                    "name": model["name"],
                                    "norm": True,
                                    "mode": "lines+markers",
                                    "line": {"color": color, "dash": "dash" if model["encoder"] else "dot"},
                                }
                                for model, color in zip(
                                    model_config["planning"], ["#021d00", "#023400", "#026c00", "#02b800"]
                                )
                            ],
                        ],
                    )
    save_statistics(statistics)
    # fechar tudo
    launch.shutdown()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--ds", default="validation", type=str, help="Dataset to load from")
    parser.add_argument("--cid", default=0, type=int, help="Center ID of environments")
    parser.add_argument(
        "--debug",
        default=False,
        nargs="?",
        const=True,
        type=bool,
        help="Debug code so it runs without requiring models",
    )

    args = parser.parse_args()
    debug = args.debug
    run_statistics(args)
