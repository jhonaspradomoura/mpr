import json
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, TensorBoard, ReduceLROnPlateau, History
from cae.hyperparams_cae import build_model
from planner import model
from planner.data_loader import load_from_files
import cae.data_loader as cae_data_loader
import argparse
import os
from kerastuner import HyperParameters, Hyperband
from cae.encoder import ContractiveAutoEncoder as cae
import tensorflow as tf
import numpy as np
from utils.data_serializer import read_pnet

project_path = f"{os.path.abspath(__file__).split('mpr')[0]}mpr"


def planner_data(args):
    # "prelu_0.5"                : {"dp"    : 0.5, "last_dp": False, "no_output_actv": False, "actv": "prelu",
    #                                        "reduce": False},

    num_itt = 10
    configs = {
        "selu_0.2_lastdp_linear_ae": {
            "dp": 0.2,
            "last_dp": True,
            "no_output_actv": True,
            "actv": "selu",
            "reduce": True,
        },
        "selu_0.2_lastdp_linear": {
            "dp": 0.2,
            "last_dp": True,
            "no_output_actv": True,
            "actv": "selu",
            "reduce": True,
        },
        "selu_0.2_lastdp_output_ae": {
            "dp": 0.2,
            "last_dp": True,
            "no_output_actv": False,
            "actv": "selu",
            "reduce": True,
        },
        "selu_0.2_lastdp_output": {"dp": 0.2, "last_dp": True, "no_output_actv": False, "actv": "selu", "reduce": True},
        "selu_0.2_linear_ae": {
            "dp": 0.2,
            "last_dp": False,
            "no_output_actv": True,
            "actv": "selu",
            "reduce": True,
        },
        "selu_0.2_linear": {
            "dp": 0.2,
            "last_dp": False,
            "no_output_actv": True,
            "actv": "selu",
            "reduce": True,
        },
        "selu_0.2_output_ae": {"dp": 0.2, "last_dp": False, "no_output_actv": False, "actv": "selu", "reduce": True},
        "selu_0.2_output": {"dp": 0.2, "last_dp": False, "no_output_actv": False, "actv": "selu", "reduce": True},
        "prelu_0.5": {"dp": 0.5, "last_dp": False, "no_output_actv": True, "actv": "prelu", "reduce": False},
        "prelu_0.5_ae": {
            "dp": 0.5,
            "last_dp": False,
            "no_output_actv": True,
            "actv": "prelu",
            "reduce": False,
        },
    }

    for name, config in configs.items():

        print(f"\nNova config -> {name}\n")

        training = read_pnet("paths_training_tuner_1", 100)
        validation = read_pnet("paths_validation_tuner_1", 100)
        # training, validation = load_from_files(
        #     args.center_id,
        #     normalize=True,
        #     batch_size=100,
        #     encoder_path=f"{args.encoder}" if "_ae" in name else "prelu",
        #     _tuner=False,
        #     id=3,
        #     folder_suffix="_rrt" if "rrt" in name else "",
        # )
        activation = config["actv"]
        no_output_actv = config.get("no_output_actv", False)
        last_dp = config.get("last_dp", False)
        dp = config["dp"]

        try:
            f = open(f"{project_path}/performance_data/planner/{name}.json", "r")
            try:
                file_dict = json.load(f)
                itt_left = num_itt - len(file_dict.get("loss", []))
            except json.JSONDecodeError:
                itt_left = num_itt
            f.close()
        except FileNotFoundError:
            print("Creating file")
            with open(f"{project_path}/performance_data/planner/{name}.json", "w+") as f:
                empty_dict = json.dumps({})
                f.write(empty_dict)
                itt_left = num_itt

        for i in range(itt_left):
            with open(f"{project_path}/performance_data/planner/{name}.json", "r") as f:
                try:
                    file_dict = json.load(f)
                    if "loss" not in file_dict.keys():
                        file_dict = {"loss": [], "epochs": []}
                except:
                    file_dict = {"loss": [], "epochs": []}

            print(f"\nIteracao {i}\n")

            history = History()
            optimizer = Adam(learning_rate=1e-4)

            planner = model.PlanningModule(
                input_shape=32,
                output_size=2,
                dropout=dp,
                activation=activation,
                last_dp=last_dp,
                no_output_actv=no_output_actv,
            )

            planner.compile(optimizer, loss="mse", metrics=["RootMeanSquaredError"])

            if config["reduce"]:
                planner.fit(
                    training,
                    epochs=100,
                    batch_size=100,
                    validation_data=validation,
                    callbacks=[
                        ModelCheckpoint(
                            f"{project_path}/models/{name}/{name}",
                            monitor="val_loss",
                            save_weights_only=True,
                            save_best_only=True,
                            verbose=1,
                        ),
                        ReduceLROnPlateau(factor=0.2, patience=6, cooldown=1, min_delta=1e-4, verbose=1),
                        EarlyStopping(min_delta=1e-4, patience=20, verbose=1),
                        history,
                    ],
                )
            else:
                planner.fit(
                    training,
                    epochs=100,
                    batch_size=100,
                    validation_data=validation,
                    callbacks=[
                        ModelCheckpoint(
                            f"{project_path}/models/{name}/{name}",
                            monitor="val_loss",
                            save_weights_only=True,
                            save_best_only=True,
                            verbose=1,
                        ),
                        EarlyStopping(min_delta=1e-4, patience=20, verbose=1),
                        history,
                    ],
                )

            with open(f"{project_path}/performance_data/planner/{name}.json", "w") as f:
                val_loss = np.array(history.history["val_loss"])
                min_loss_idx = np.where(np.min(val_loss) == val_loss)[0][0]
                file_dict["loss"].append(val_loss[min_loss_idx])
                file_dict["epochs"].append(history.epoch[min_loss_idx])
                json.dump(file_dict, f)


def encoder(args):
    hp = HyperParameters()
    tuner = Hyperband(
        build_model,
        objective="val_loss",
        max_epochs=50,
        factor=2,
        directory="../cae/mpr_logs",
        project_name="mpr_logs",
        executions_per_trial=1,
        hyperparameters=hp,
        tune_new_entries=True,
    )
    # model = [cae(2800, "prelu")]
    models = [tuner.get_best_models(90)[-1]]
    models = []
    models.extend(tuner.get_best_models(5))
    # names = ["prelu", "tuner_1", "tuner_2", "tuner_3", "tuner_4"]
    names = ["prelu", "tuner_1", "tuner_2", "tuner_3", "tuner_4", "tuner_5"]

    print(models)
    training, validation = cae_data_loader.load_tf_dataset(f"{project_path}/obs/center{args.center_id}/env*", True)
    for name, model in zip(names, models):
        print(f"\nNova config -> {name}\n")

        num_itt = 25

        try:
            f = open(f"{project_path}/performance_data/ae/{name}.json", "r")
            try:
                file_dict = json.load(f)
                itt_left = num_itt - len(file_dict.get("loss", []))
            except json.JSONDecodeError:
                itt_left = num_itt
            f.close()
        except FileNotFoundError:
            print("Creating file")
            with open(f"{project_path}/performance_data/ae/{name}.json", "w+") as f:
                empty_dict = json.dumps({})
                f.write(empty_dict)
                itt_left = num_itt

        for i in range(itt_left):
            print(f"\nIteracao {i}\n")

            with open(f"{project_path}/performance_data/ae/{name}.json", "r") as f:
                try:
                    file_dict = json.load(f)
                    if "loss" not in file_dict.keys():
                        file_dict = {"loss": [], "epochs": []}
                except:
                    file_dict = {"loss": [], "epochs": []}

            new_model = tf.keras.models.clone_model(model)

            history = History()
            optimizer = Adam(learning_rate=1e-4)

            new_model.compile(optimizer, loss="mse", metrics=["MeanAbsoluteError"])

            if name == "prelu":
                new_model.fit(
                    training,
                    epochs=100,
                    batch_size=32,
                    validation_data=validation,
                    callbacks=[
                        EarlyStopping(min_delta=2e-4, patience=20, verbose=1),
                        history,
                        ModelCheckpoint(
                            f"{project_path}/models/{name}/encoder",
                            monitor="val_loss",
                            save_weights_only=True,
                            verbose=1,
                            save_best_only=True,
                        ),
                    ],
                ),
            else:
                new_model.fit(
                    training,
                    epochs=100,
                    batch_size=32,
                    validation_data=validation,
                    callbacks=[
                        EarlyStopping(min_delta=2e-4, patience=15, verbose=1),
                        ReduceLROnPlateau(factor=0.2, patience=6, cooldown=2, min_delta=1e-4, verbose=1),
                        history,
                        ModelCheckpoint(
                            f"{project_path}/models/{name}/encoder",
                            monitor="val_loss",
                            save_weights_only=True,
                            verbose=1,
                            save_best_only=True,
                        ),
                    ],
                )

            # tries.append({'loss': history.history['val_loss'], "epochs": history.epoch})
            with open(f"{project_path}/performance_data/ae/{name}.json", "w") as f:
                val_loss = np.array(history.history["val_loss"])
                min_loss_idx = np.where(np.min(val_loss) == val_loss)[0][0]
                file_dict["loss"].append(val_loss[min_loss_idx])
                file_dict["epochs"].append(history.epoch[min_loss_idx])
                json.dump(file_dict, f)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--center_id", default=3, type=int, help="Which center folder to get obs from")
    parser.add_argument("--mode", default="planner", type=str, help="Gather data from autoencoder or planner")
    parser.add_argument("--encoder", default="selu_4", type=str, help="AE to be used")

    args = parser.parse_args()

    if args.mode == "planner":
        planner_data(args)
    else:
        encoder(args)
