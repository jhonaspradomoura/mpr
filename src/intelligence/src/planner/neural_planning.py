import argparse
import numpy as np
import os
import tensorflow as tf
from kerastuner import HyperParameters, Hyperband
from planner.model import PlanningModule
from planner import data_loader
from planner.data_gathering import build_model
import math
from time import time

project_path = f"{os.path.abspath(__file__).split('mpr')[0]}mpr"

normalize = True


def is_in_collision(x, centers):
    global normalize
    obstacle_area = 2.5 if not normalize else 5 / (40 * 2)
    for center in centers:
        dists = np.abs(center - x)
        if dists[0] < obstacle_area and dists[1] < obstacle_area:
            return True
    return False


def steer_to(start, end, centers):
    global normalize
    resolution = 0.0025 if normalize else 0.1
    dists = end - start

    dist_total = np.linalg.norm(dists)
    increment_total = dist_total / resolution

    dists /= increment_total

    num_segments = math.floor(increment_total)

    states = []
    state_curr = start.copy()
    for _ in range(0, num_segments):

        if is_in_collision(state_curr, centers):
            return False

        state_curr += dists
        states.append(state_curr.copy())

    if is_in_collision(end, centers):
        return False

    return True


# checks the feasibility of entire path including the path edges
def feasibility_check(path, centers):
    for i in range(0, len(path) - 1):
        valid = steer_to(path[i], path[i + 1], centers)
        if not valid:
            return False
    return True


# lazy vertex contraction
def lvc(path, centers):
    for i in range(0, len(path) - 1):
        for j in range(len(path) - 1, i + 1, -1):
            valid = steer_to(path[i], path[j], centers)
            if valid:
                pc = []
                for k in range(0, i + 1):
                    pc.append(path[k])
                for k in range(j, len(path)):
                    pc.append(path[k])
                return lvc(pc, centers)

    return path


def replan_path(path_original, centers, inp, mlp):
    new_path = []
    path = [path_original[0].copy()]
    for i in range(1, len(path_original) - 1):
        if not is_in_collision(path_original[i], centers) and not np.any(path_original[i] == path):
            path.append(path_original[i].copy())
    path.append(path_original[-1].copy())
    for i in range(0, len(path) - 1):
        start = path[i].copy()
        goal = path[i + 1].copy()
        target_reached = steer_to(start.copy(), goal.copy(), centers)
        if target_reached:
            new_path.append(start)
            new_path.append(goal)
        else:
            step = 0
            path_1 = [start]
            path_2 = [goal]
            tree = False
            inp[-2:] = goal.copy()
            inp_2 = inp.copy()
            inp_2[-2:] = start.copy()
            while not target_reached and step < 50:
                step += 1
                if not tree:
                    inp[-4:-2] = start.copy()
                    inp[-2:] = goal.copy()
                    start = mlp(inp.reshape((1, -1))).numpy().reshape((-1,))
                    path_1.append(start)
                    tree = True
                else:
                    inp_2[-4:-2] = goal.copy()
                    inp_2[-2:] = start.copy()
                    goal = mlp(inp_2.reshape((1, -1))).numpy().reshape((-1,))
                    path_2.append(goal)
                    tree = False
                target_reached = steer_to(start, goal, centers)
            if target_reached:
                for point in path_1:
                    new_path.append(point)
                for point in path_2[::-1]:
                    new_path.append(point)
            else:
                return False, new_path
            # return target_reached, new_path

    return True, np.array(new_path)


def plan(planner, start, end, perm, planner_input):
    step = 0
    direction = False  # which direction to plan for next
    target_reached = steer_to(start, end, perm)

    start_input = planner_input.copy()
    start_input[-4:-2] = start.copy()
    start_input[-2:] = end.copy()

    end_input = start_input.copy()
    end_input[-4:-2] = end.copy()
    end_input[-2:] = start.copy()

    path = []
    path_1, path_2 = [start], [end]
    while not target_reached and step < 80:
        step += 1
        if not direction:
            start_input[-4:-2] = start.copy()
            start_input[-2:] = end.copy()
            start = planner(start_input.reshape(1, -1)).numpy().reshape((-1,))
            path_1.append(start)
        else:
            end_input[-4:-2] = end.copy()
            end_input[-2:] = start.copy()
            end = planner(end_input.reshape(1, -1)).numpy().reshape((-1,))
            path_2.append(end)
        direction = not direction
        target_reached = steer_to(start, end, perm)

    for point in path_1:
        path.append(point)
    for point in path_2[::-1]:
        path.append(point)

    path = np.array(lvc(np.array(path), perm))
    feasible = feasibility_check(path, perm)
    if target_reached or feasible:
        if feasible:
            return path, "success"
        else:
            sp = 0
            new_path = path.copy()
            while not feasible and sp < 10:
                sp = sp + 1
                result, new_path = replan_path(new_path, perm, start_input, planner)  # replanning at coarse level
                if result:
                    new_path = np.array(lvc(new_path, perm))
                    feasible = feasibility_check(new_path, perm)

                    if feasible:
                        return new_path, "replanning success"
                else:
                    return new_path, "replanning failure"
            else:
                return new_path, "replanning failure"
    elif not target_reached and not feasible:
        return path, "failure"


def main(args):
    global normalize
    normalize = args.normalize
    envs_list = os.listdir(f"{project_path}/paths/{args.ds}")
    envs_list = [env for env in envs_list if "env" in env]
    env_id = np.random.choice(envs_list, 50)
    data_combinations = []
    for env in env_id:
        data_combinations.append((env, np.random.choice(os.listdir(f"{project_path}/paths/{args.ds}/{env}"), 40)))

    model_names = [
        "prelu_0.5_linear_rrt",
        "prelu_0.5_linear_ae_rrt",
        "selu_0.2_lastdp_linear_ae_rrt",
        "selu_0.2_lastdp_linear_ae_astar",
    ]
    num_aes = set()
    for name in model_names:
        if "ae" in name:
            num_aes.update({"selu_4"})
        else:
            num_aes.update({"encoder"})

    num_aes = list(num_aes)
    data = []
    for ae in num_aes:
        ae_data = []
        for env, paths in data_combinations:
            env_data = []
            for path in paths:
                eval_data = data_loader.pre_process_eval(
                    f"{project_path}/paths/{args.ds}/{env}/{path}",
                    f"{project_path}/obs/center{args.center_id}/env" f"{env.split('_')[1]}.npy",
                    args.normalize,
                    ae,
                    "selu" in ae,
                    id_=3,
                    rrt=True,
                )
                env_data.append(eval_data)
            ae_data.append(env_data)
        data.append(ae_data)
    data = np.array(data)
    data = data.transpose((1, 2, 3, 0))
    maps = []
    centers = []
    for env in env_id:
        if normalize:
            maps.append(
                np.interp(
                    np.load(f"{project_path}/obs/center{args.center_id}/env{env.split('_')[1]}.npy"), (-20, 20), (0, 1)
                )
            )
            centers.append(
                np.interp(
                    np.load(f"{project_path}/obs/center{args.center_id}/perm{env.split('_')[1]}.npy"), (-20, 20), (0, 1)
                )
            )
        else:
            maps.append(np.load(f"{project_path}/obs/center{args.center_id}/env{env.split('_')[1]}.npy"))
            centers.append(np.load(f"{project_path}/obs/center{args.center_id}/perm{env.split('_')[1]}.npy"))

    models = {}
    for name in model_names:
        dropout = name.split("_")[1]
        activation = name.split("_")[0]
        last_dp = "lastdp" in name
        linear = "linear" in name

        mlp = PlanningModule(32, 2, dropout=dropout, activation=activation, no_output_actv=linear, last_dp=last_dp)
        latest_ckpt = tf.train.latest_checkpoint(f"{project_path}/models/{name}")
        mlp.load_weights(latest_ckpt)
        models[name] = mlp

    results = {}
    plots = 0
    for name in model_names:
        results[name] = {
            "simple": 0,
            "success": 0,
            "replanning success": 0,
            "replanning failure": 0,
            "total failure": 0,
        }
    for ds, center in zip(data, centers):
        for original_path in ds:
            redraw = True
            for name, mlp in models.items():
                # dropout = name.split('_')[1]
                # activation = name.split('_')[0]
                # last_dp = 'lastdp' in name
                # linear = 'linear' in name
                if "ae" in name:
                    data_ae_idx = 0 if "selu" in num_aes[0] else 1
                else:
                    data_ae_idx = 1 if "selu" in num_aes[0] else 0

                # mlp = PlanningModule(32, 2, dropout=dropout, activation=activation, no_output_actv=linear,
                #                      last_dp=last_dp)
                # latest_ckpt = tf.train.latest_checkpoint(f"{project_path}/models/{name}")
                # mlp.load_weights(latest_ckpt)

                inp = original_path[0][data_ae_idx][0].copy()
                reference = np.insert(original_path[1][data_ae_idx], 0, inp[-4:-2].copy(), axis=0)
                start = inp[-4:-2].copy()
                goal = inp[-2:].copy()

                start_1 = start.copy()
                start_2 = goal.copy()

                reverse_input = inp.copy()

                reverse_input[-4:-2] = goal.copy()
                reverse_input[-2:] = start.copy()

                path_1 = [start]
                path_2 = [goal]

                mode_result = {}
                step = 0
                path = []  # stores end2end path by concatenating path1 and path2
                tree = False
                tic = time()

                if redraw:
                    data_loader.plot(center, reference, {}, normalize, renew_fig=True)
                    redraw = False

                target_reached = steer_to(start_1, start_2, center)
                while not target_reached and step < 80:
                    step += 1
                    if not tree:
                        inp[-4:-2] = start_1.copy()
                        inp[-2:] = start_2.copy()
                        start_1 = mlp(inp.reshape(1, -1)).numpy().reshape((-1,))
                        path_1.append(start_1)
                        tree = True
                    else:
                        reverse_input[-4:-2] = start_2.copy()
                        reverse_input[-2:] = start_1.copy()
                        start_2 = mlp(reverse_input.reshape(1, -1)).numpy().reshape((-1,))
                        path_2.append(start_2)
                        tree = False
                    target_reached = steer_to(start_1, start_2, center)

                if target_reached:
                    for point in path_1:
                        path.append(point)
                    for point in path_2[::-1]:
                        path.append(point)

                    path = np.array(path)
                    path = np.array(lvc(path, center))
                    feasible = feasibility_check(path, center)
                    if feasible:
                        toc = time()
                        print(f"Spent {toc - tic}s")
                        if len(path) > 2:
                            mode_result[name] = path
                            results[name]["success"] += 1
                            data_loader.plot(center, reference, mode_result, normalize)
                        else:
                            results[name]["simple"] += 1
                    else:
                        mode_result[name] = path
                        # data_loader.plot(center, reference, mode_result, normalize)
                        sp = 0
                        new_path = path.copy()
                        while not feasible and sp < 10:
                            sp = sp + 1
                            result, new_path = replan_path(new_path, center, inp, mlp)  # replanning at coarse level
                            if result:
                                new_path = np.array(lvc(new_path, center))
                                feasible = feasibility_check(new_path, center)

                                if feasible:
                                    toc = time()
                                    print(f"Spent {toc - tic}s")
                                    mode_result[name] = new_path
                                    results[name]["replanning success"] += 1
                                    data_loader.plot(center, reference, mode_result, normalize)
                                    break
                                if sp % 2 == 0 or feasible:
                                    mode_result[name] = new_path
                                    # data_loader.plot(center, reference, result, normalize)
                            else:
                                print("Failed! Invalid path")
                                results[name]["replanning failure"] += 1
                                break
                        else:
                            print("Failure! Failure!")
                            mode_result[name] = new_path
                            results[name]["replanning failure"] += 1
                            data_loader.plot(center, reference, mode_result, normalize)
                else:
                    for point in path_1:
                        path.append(point)
                    for point in path_2[::-1]:
                        path.append(point)

                    mode_result[name] = np.array(path)
                    results[name]["total failure"] += 1
                    data_loader.plot(center, reference, mode_result, normalize)
                    print("MPnet sent states that were inside an obstacle")
            # else:
            #     print(f"Target not reached! Failure! Failure!")
            #     path = np.array(path)
            #     data_loader.plot(center, original_path[1], path, True)
            plots += 1
            if len(path) > 2 and plots % 20 == 0:
                data_loader.show_plot()
    print(results)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--ds", default="validation", type=str, help="Dataset to eval from")
    parser.add_argument("--center_id", default=0, type=int, help="Which center folder to get obs from")
    # parser.add_argument("--ae", default="encoder", type=str, help='AE folder')
    parser.add_argument("--normalize", const=True, nargs="?", type=bool, help="Normalize data")
    # parser.add_argument('--tuner', default=True, type=bool, help='Use tuner autoencoder', required=False)
    args = parser.parse_args()
    main(args)
