import os
import numpy as np
from cae.encoder import ContractiveAutoEncoder
import tensorflow as tf
import argparse
import plotly.graph_objects as go
from kerastuner import HyperParameters, Hyperband
from cae.hyperparams_cae import build_model

project_path = f"{os.path.abspath(__file__).split('mpr')[0]}mpr"
bag_path = f"{project_path}/bags"
data_path = f"{project_path}/path_dataset_np"
tf.compat.v1.enable_eager_execution(True)
tuner = None
cae = None
fig = None


def get_autoencoder(_tuner=False, id=3):
    if _tuner:
        hp = HyperParameters()
        tuner_ = Hyperband(
            build_model,
            objective="val_loss",
            max_epochs=50,
            factor=2,
            directory=f"{project_path}/src/intelligence/src/cae/mpr_logs",
            project_name="mpr_logs",
            executions_per_trial=1,
            hyperparameters=hp,
            tune_new_entries=True,
        )

        models = []
        models.extend(tuner_.get_best_models(id + 1))
        return models[id]
    else:
        return ContractiveAutoEncoder(2800, "prelu")


def get_encoded(obstacles):
    global cae
    return cae.encoder(obstacles.reshape((1, -1))).numpy()


def get_reconstructed(obstacles):
    global cae
    return cae(obstacles.reshape((1, -1))).numpy()


def plot(centers, reference, result: dict, normalize, renew_fig=False):
    global fig
    if renew_fig:
        fig = go.FigureWidget()
        if normalize:
            fig.update_xaxes(range=[0, 1])
            fig.update_yaxes(range=[0, 1])
            size = 5 / 40
        else:
            fig.update_xaxes(range=[-20, 20])
            fig.update_yaxes(range=[-20, 20])
            size = 5
        env = [[], []]
        for center in centers:
            x, y = center
            env[0].extend([x - size / 2, x - size / 2, x + size / 2, x + size / 2, x - size / 2, None])
            env[1].extend([y - size / 2, y + size / 2, y + size / 2, y - size / 2, y - size / 2, None])
        env = np.array(env)
        x = env[0]
        y = env[1]
        fig.add_trace(go.Scatter(x=x, y=y, fill="toself", fillcolor="black", name="obstacle"))
        ref_x = reference[:, 0]
        ref_y = reference[:, 1]
        fig.add_trace(go.Scatter(x=ref_x, y=ref_y, mode="lines", name="reference", line=dict(color="green")))
        fig.add_trace(
            go.Scatter(
                x=[ref_x[0]],
                y=[ref_y[0]],
                mode="markers",
                name="start",
                marker=dict(size=8, color="green", line=dict(width=2, color="darkgreen")),
            )
        )
        fig.add_trace(
            go.Scatter(
                x=[ref_x[-1]],
                y=[ref_y[-1]],
                mode="markers",
                name="finish",
                marker=dict(size=8, color="red", line=dict(width=2, color="darkred")),
            )
        )
        fig.update_layout(width=1000, height=1000)
    for key in result.keys():
        color = "blue" if "selu" in key else "magenta"
        dash = True if "ae" in key else False
        key_result = result[key].reshape((-1, 2))
        res_x = key_result[:, 0]
        res_y = key_result[:, 1]
        if dash:
            fig.add_trace(go.Scatter(x=res_x, y=res_y, mode="lines", name=key, line=dict(color=color, dash="dash")))
        else:
            fig.add_trace(go.Scatter(x=res_x, y=res_y, mode="lines", name=key, line=dict(color=color)))


def show_plot():
    global fig
    fig.show()


def load_from_files(center_id, normalize, batch_size, encoder_path, _tuner=False, id=3, folder_suffix=""):
    global cae, tuner

    tuner = _tuner
    cae = get_autoencoder(tuner, id)
    print(encoder_path)
    if not _tuner:
        checkpoint = tf.train.latest_checkpoint(f"{project_path}/models/{encoder_path}")
        cae.load_weights(checkpoint).expect_partial()

    training = get_dataset(
        center_id,
        normalize,
        batch_size,
        folder_name=f"training{folder_suffix}",
        rrt=True if "rrt" in folder_suffix else False,
    )
    validation = get_dataset(
        center_id,
        normalize,
        batch_size,
        folder_name=f"validation{folder_suffix}",
        rrt=True if "rrt" in folder_suffix else False,
    )
    return training, validation


def get_dataset(center_id, normalize, batch_size, folder_name, rrt=False):
    dataset = tf.data.Dataset.list_files(f"{project_path}/paths/{folder_name}/npy/*")
    dataset = dataset.map(
        lambda x: tf.py_function(get_tf_str, [x, center_id], [tf.string, tf.string]), tf.data.experimental.AUTOTUNE
    )

    def map_func(x, y):
        return tf.numpy_function(pre_process, [x, y, center_id, normalize], [tf.float32, tf.float32])

    # map_func = lambda x, y: tf.numpy_function(pre_process, [x, y, center_id, normalize], [tf.float32,
    # tf.float32])
    dataset = dataset.map(
        lambda x, y: tf.numpy_function(pre_process, [x, y, normalize, rrt], [tf.float32, tf.float32]),
        tf.data.experimental.AUTOTUNE,
    )
    dataset = dataset.flat_map(lambda x, y: dataset.from_tensor_slices((x, y)))
    if batch_size:
        dataset = dataset.shuffle(1000).cache().batch(batch_size).prefetch(tf.data.experimental.AUTOTUNE)
    else:
        dataset = dataset.shuffle(1000).cache().prefetch(tf.data.experimental.AUTOTUNE)
    return dataset


def get_tf_str(path, center):
    env = tf.strings.split(path, "env_")[1]
    env = tf.strings.split(env, ".npy")[0]
    return path, tf.strings.join([f"{project_path}/obs/center{center}/env", env, ".npy"])


def pre_process(paths, env_path, norm, rrt):
    global cae, tuner
    interp_values = (-20, 20) if not rrt else (-400, 400)
    paths_data = np.load(paths, allow_pickle=True)
    env = np.load(env_path)
    env = np.interp(env, (-20, 20), (0, 1))
    if tuner:
        cae(env.reshape((1, -1)))
        encoded = cae.code.numpy()
    else:
        encoded = cae.encoder(env.reshape((1, -1))).numpy()
    input_data = []
    reference = []
    for path in paths_data:
        for idx, data in enumerate(path[:-1]):
            if norm:
                data = np.interp(data, interp_values, (0, 1))
                next_data = np.interp(path[idx + 1], interp_values, (0, 1))
                goal = np.interp(path[-1], interp_values, (0, 1))
            else:
                next_data = path[idx + 1]
                goal = path[-1]
            input_data.append(np.array([*encoded.flatten(), *data, *goal]).flatten())
            reference.append(np.array([*next_data]).flatten())
    return np.array(input_data, dtype=np.float32), np.array(reference, dtype=np.float32)


def pre_process_eval(paths, env_path, norm, encoder_path, tuner_=False, id_=3, rrt=False):
    global tuner, cae
    interp_values = (-20, 20) if not rrt else (-400, 400)
    if tuner_ != tuner:
        cae = get_autoencoder(tuner_, id_)
        checkpoint = tf.train.latest_checkpoint(f"{project_path}/models/{encoder_path}")
        cae.load_weights(checkpoint).expect_partial()
        tuner = tuner_

    paths_data = np.load(paths, allow_pickle=True).reshape((-1, 2))
    env = np.load(env_path)
    env = np.interp(env, (-20, 20), (0, 1))
    if tuner_:
        cae(env.reshape((1, -1))).numpy()
        encoded = cae.code.numpy()
    else:
        encoded = cae.encoder(env.reshape((1, -1))).numpy()
    input_data = []
    reference = []
    for idx, data in enumerate(paths_data[:-1]):
        if norm:
            data = np.interp(data, interp_values, (0, 1))
            next_data = np.interp(paths_data[idx + 1], interp_values, (0, 1))
            goal = np.interp(paths_data[-1], interp_values, (0, 1))
        else:
            next_data = paths_data[idx + 1]
            goal = paths_data[-1]
        input_data.append(np.array([*encoded.flatten(), *data, *goal]).flatten())
        reference.append(np.array([*next_data]).flatten())
    return np.array(input_data, dtype=np.float32), np.array(reference, dtype=np.float32)


def main(args):
    pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--gen", const=True, nargs="?", type=bool, help="Generates npy from bag files")

    args = parser.parse_args()
    load_from_files(3, True, 1000)
