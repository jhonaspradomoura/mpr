from abc import ABC

from tensorflow.keras import Sequential, layers, initializers, activations


class PlanningModule(Sequential, ABC):
    def __init__(self, input_shape, output_size, dropout=0.2, activation='selu',
                 last_dp=False, no_output_actv=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        initializer = lambda: "glorot_uniform" if activation != 'selu' else initializers.LecunNormal()
        activation = lambda: "selu" if activation == 'selu' else layers.PReLU()
        
        self.add(layers.Dense(input_shape=(input_shape,), units=1280, activation=activation(),
                              kernel_initializer=initializer(), bias_initializer=initializer()))
        self.add(layers.Dropout(rate=dropout))
        
        self.add(layers.Dense(1024, activation=activation(), kernel_initializer=initializer(),
                              bias_initializer=initializer()))
        self.add(layers.Dropout(rate=dropout))
        
        self.add(layers.Dense(896, activation=activation(), kernel_initializer=initializer(),
                              bias_initializer=initializer()))
        self.add(layers.Dropout(rate=dropout))
        
        self.add(layers.Dense(768, activation=activation(), kernel_initializer=initializer(),
                              bias_initializer=initializer()))
        self.add(layers.Dropout(rate=dropout))
        
        self.add(layers.Dense(512, activation=activation(), kernel_initializer=initializer(),
                              bias_initializer=initializer()))
        self.add(layers.Dropout(rate=dropout))
        
        self.add(layers.Dense(384, activation=activation(), kernel_initializer=initializer(),
                              bias_initializer=initializer()))
        self.add(layers.Dropout(rate=dropout))
        
        self.add(layers.Dense(256, activation=activation(), kernel_initializer=initializer(),
                              bias_initializer=initializer()))
        self.add(layers.Dropout(rate=dropout))
        
        self.add(layers.Dense(256, activation=activation(), kernel_initializer=initializer(),
                              bias_initializer=initializer()))
        self.add(layers.Dropout(rate=dropout))
        
        self.add(layers.Dense(128, activation=activation(), kernel_initializer=initializer(),
                              bias_initializer=initializer()))
        self.add(layers.Dropout(rate=dropout))
        
        self.add(layers.Dense(64, activation=activation(), kernel_initializer=initializer(),
                              bias_initializer=initializer()))
        self.add(layers.Dropout(rate=dropout))
        
        self.add(layers.Dense(32, activation=activation(), kernel_initializer=initializer(),
                              bias_initializer=initializer()))
        if last_dp:
            self.add(layers.Dropout(rate=dropout))
        if no_output_actv:
            activation = (lambda: None)
        self.add(layers.Dense(output_size, activation=activation(), kernel_initializer=initializer(),
                              bias_initializer=initializer()))
    
    def call(self, inputs, training=None, mask=None):
        x = self.layers[0](inputs)
        for layer in self.layers[1:]:
            x = layer(x)
        return x
