# !/usr/bin/env python3
from rrt.rrt_star_bid import RRTStarBidirectional
from search_space.search_space import SearchSpace
from utils.plotting import Plot
import numpy as np
import argparse
import os
from multiprocessing import Process, BoundedSemaphore, Pool

work_path = f"{os.path.abspath(__file__).split('mpr')[0]}mpr/obs"
paths_path = f"{os.path.abspath(__file__).split('mpr')[0]}mpr/paths"

semaphore = BoundedSemaphore(15)


def define_probabilities(search_space, obstacles, resolution, map_size):
    obst_size = 5 / resolution
    probabilities = np.zeros(int(map_size / resolution) ** 2, dtype=np.float32)
    
    valid_idxs = [idx for idx, point in enumerate(search_space) if
                  not np.any(np.all(np.abs(point - obstacles) <= obst_size / 2, axis=1))]
    probabilities[valid_idxs] = 1 / len(valid_idxs)
    return probabilities


def obstacle_bounds(obstacles, resolution):
    obst_size = 5 / resolution
    lower_bounds = obstacles - obst_size / 2
    upper_bounds = obstacles + obst_size / 2
    bounds = np.column_stack((lower_bounds, upper_bounds))
    return bounds


def main(args):
    envs_list = os.listdir(f"{work_path}/center{args.center}")
    envs_list = [env for env in envs_list if "perm" in env]
    chosen_envs = np.random.choice(envs_list, args.num_envs + args.eval_only_envs)
    
    dimensions = np.array([[-args.map_size / (2 * args.resolution), args.map_size / (2 * args.resolution)],
                           [-args.map_size / (2 * args.resolution), args.map_size / (2 * args.resolution)]])
    
    edges = np.array([(8, 4)])
    max_samples = 1024
    rewire_count = 32
    prc = 0.1
    smallest_edge = 1
    
    for n, env in enumerate(chosen_envs):
        env_id = env.split('perm')[-1].split('.')[0]
        obstacles, points, probs = get_obstacles(args, env)
        
        search_space = SearchSpace(dimensions, obstacles)
        
        if n < args.num_envs:
            if not os.path.exists(f"{paths_path}/training_rrts/env{env_id}"):
                os.mkdir(f"{paths_path}/training_rrts/env{env_id}")
            origins = points[np.random.choice(len(points), args.origins + args.seen_origins, p=probs)].astype(
                    int)
            goals = points[np.random.choice(len(points), args.goals + args.unseen_origins, p=probs)].astype(
                    int)
            path_id = 0
            processes = []
            for origin in origins[:args.origins]:
                for goal in goals[:args.goals]:
                    folder_name = "training_rrts"
                    to_be_closed = []
                    while len(processes) >= 15:
                        for proc in processes:
                            if proc.is_alive():
                                proc.join(0.001)
                            else:
                                proc.close()
                                to_be_closed.append(proc)
                        [processes.remove(x) for x in to_be_closed]
                    plot = path_id % 15 == 0
                    p = Process(target=plan, args=(edges, env_id, folder_name, goal, max_samples, obstacles,
                                                   origin, path_id, prc, rewire_count, search_space,
                                                   smallest_edge, dimensions, plot))
                    processes.append(p)
                    p.start()
                    path_id += 1
            [p.join() for p in processes if p.is_alive()]
            processes = []
            path_id = 0
            for origin in origins[-args.seen_origins:]:
                for goal in goals[-args.seen_goals:]:
                    folder_name = "validation_rrts"
                    to_be_closed = []
                    while len(processes) >= 15:
                        for proc in processes:
                            if proc.is_alive():
                                proc.join(0.001)
                            else:
                                proc.close()
                                to_be_closed.append(proc)
                        [processes.remove(x) for x in to_be_closed]
                    if path_id % 5 == 0:
                        plot = True
                    else:
                        plot = False
                    p = Process(target=plan, args=(edges, env_id, folder_name, goal, max_samples, obstacles,
                                                   origin, path_id, prc, rewire_count, search_space,
                                                   smallest_edge, dimensions, False))
                    processes.append(p)
                    p.start()
                    path_id += 1
            [p.join() for p in processes if p.is_alive()]
        else:
            if not os.path.exists(f"{paths_path}/validation_rrts/env{env_id}"):
                os.mkdir(f"{paths_path}/validation_rrts/env{env_id}")
            origins = points[np.random.choice(len(points), args.unseen_origins, p=probs)].astype(int)
            goals = points[np.random.choice(len(points), args.unseen_goals, p=probs)].astype(int)
            path_id = 0
            processes = []
            for origin in origins:
                for goal in goals:
                    folder_name = "validation_rrts"
                    to_be_closed = []
                    while len(processes) >= 15:
                        for proc in processes:
                            if proc.is_alive():
                                proc.join(0.001)
                            else:
                                proc.close()
                                to_be_closed.append(proc)
                        [processes.remove(x) for x in to_be_closed]
                    if path_id % 5 == 0:
                        plot = True
                    else:
                        plot = False
                    p = Process(target=plan, args=(edges, env_id, folder_name, goal, max_samples, obstacles,
                                                   origin, path_id, prc, rewire_count, search_space,
                                                   smallest_edge, dimensions, False))
                    processes.append(p)
                    p.start()
                    path_id += 1


def get_obstacles(args, env):
    obstacles = np.load(f"{work_path}/center{args.center}/{env}")
    obstacles /= args.resolution
    points = np.mgrid[-args.map_size / 2:args.map_size / 2:args.resolution,
             -args.map_size / 2:args.map_size / 2: args.resolution].reshape((-1, 2)) / args.resolution
    probs = define_probabilities(points, obstacles, args.resolution, args.map_size)
    obstacles = obstacles.astype(np.int)
    obstacles = obstacle_bounds(obstacles, args.resolution)
    return obstacles, points, probs


def plan(edges, env_id, folder_name, goal, max_samples, obstacles, origin, path_id, prc, rewire_count,
         search_space, smallest_edge, dimensions, will_plot):
    with semaphore:
        rrt = RRTStarBidirectional(search_space, edges, tuple(origin), tuple(goal), max_samples,
                                   smallest_edge, prc, rewire_count)
        path = rrt.rrt_star_bidirectional()
        if path is not None:
            path = np.array(path)
            with open(f"{paths_path}/{folder_name}/env{env_id}/path{path_id}.npy", "wb") as f:
                np.save(f, path)
            if will_plot:
                plot = Plot(f"path{path_id}", dimensions)
                plot.plot_tree(search_space, rrt.trees)
                if path is not None:
                    plot.plot_path(search_space, path)
                plot.plot_obstacles(search_space, obstacles)
                plot.plot_start(search_space, tuple(origin))
                plot.plot_goal(search_space, tuple(goal))
                plot.draw(auto_open=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--map_size', default=40, type=int, required=False)
    parser.add_argument('--resolution', default=0.05, type=float, required=False)
    parser.add_argument('--num_envs', default=100, type=int, required=False)
    parser.add_argument('--eval_only_envs', default=10, type=int, required=False)
    parser.add_argument('--origins', default=80, type=int, required=False)
    parser.add_argument('--goals', default=50, type=int, required=False)
    parser.add_argument('--seen_origins', default=20, type=int, required=False)
    parser.add_argument('--seen_goals', default=10, type=int, required=False)
    parser.add_argument('--unseen_origins', default=50, type=int, required=False)
    parser.add_argument('--unseen_goals', default=40, type=int, required=False)
    parser.add_argument('--center', default=3, type=int, required=False)
    
    args = parser.parse_args()
    
    print(args)
    main(args)
