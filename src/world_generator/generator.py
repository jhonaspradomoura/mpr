#!/usr/bin/python3
import math
import os
import re

from nodes.constraints import ConstraintError
from nodes.constraints import Constraint
from nodes.generic import Node
from wbton import Wbton
import numpy as np
from grid import Grid

world_info = None
viewpoint = None
text_bg = None
text_bg_light = None


def round_down(number, dec):
    return number // 10 ** -dec / 10 ** dec


def round_up(number, dec):
    return math.ceil(number * 10 ** dec) / 10 ** dec


def add_basic_nodes(_wbt):
    global world_info, viewpoint, text_bg, text_bg_light
    world_info = _wbt.add_node("WorldInfo")
    viewpoint = _wbt.add_node("Viewpoint")
    text_bg = _wbt.add_node("TexturedBackground")
    text_bg_light = _wbt.add_node("TexturedBackgroundLight")


def add_robot_and_goal(_wbt):
    robot = _wbt.add_node("Robot", file="Pioneer3at")
    start, goal_pos = grid.determine_start_goal(robot.size)
    robot.position(f"{start[1]} {robot.size[1] / 2} {start[0]}", f"0 1 0 0")
    print(f"Robot position is {robot.lines}")


def add_obstacles(_wbt, qtt, scale="same"):
    global arena
    print(f"adicionando {qtt} obstaculos")
    qtt = list(range(qtt))
    for element in qtt:
        box = _wbt.add_node("Box")
        
        box.set_size_constraint([arena["floorSize"][0] - Node.unit, 0.5, arena.diagonal() - 5 * Node.unit])
        
        try:
            if scale == "same":
                size = 0.75
                start, end, angle = grid.determine_obj_pos(np.round(size, 2), angled=True)
                box["size"] = [size, 0.5, size]
            else:
                size = np.random.uniform(box.constraints["size"].min[0],
                                         grid.available_area() / (5 * arena["floorSize"][1]))
                start, end, width, angle = grid.determine_obj_pos_size(np.round(size, 2))
                box["size"] = [np.round(size, 2), 0.5, width]
            
            box.constraints["translation"] = Constraint(
                    [arena["translation"]["x"] - arena["floorSize"][0] / 2 - box["size"][0] / 2,
                     box["size"][1] / 2,
                     arena["translation"]["z"] - arena["floorSize"][1] / 2 - box["size"][2] / 2, ],
                    [arena["translation"]["x"] + arena["floorSize"][0] / 2 + box["size"][0] / 2,
                     box["size"][1] / 2,
                     arena["translation"]["z"] + arena["floorSize"][1] / 2 + box["size"][2] / 2, ], )
            box["translation"] = {"x": np.round(np.mean([start[1], end[1]]), 2), "y": box["size"][1] / 2,
                                  "z": np.round(np.mean([start[0], end[0]]), 2), }
            box["rotation"]["angle"] = angle
            grid.update(start, end, box["size"][0])
            print(f"Obstaculo adicionado {start, end, angle}")
        except (ConstraintError, ValueError) as err:
            print(err)
            _wbt.remove_item("Box", element)
            qtt.insert(element + 1, element)


if __name__ == "__main__":
    worlds = os.listdir(os.path.abspath(__file__ + "/../../../worlds"))
    pattern = re.compile("[0-9]*?(?=\.)")
    indexes = []
    for world in worlds:
        indexes.append(int(pattern.search(world).group()))
    indexes = [file for file in indexes if file[0] != '.']
    indexes.append(0) if len(indexes) == 0 else None
    for i in range(50):
        world_index = max(indexes) + 1
        print(f"Creating {i} world.")
        wbt = Wbton(f"{world_index}")
        
        add_basic_nodes(wbt)
        
        arena = wbt.add_node("Rectangle")
        grid = Grid((arena["translation"]["z"] - round_up(arena["floorSize"][1] / 2, 2),
                     arena["translation"]["z"] + round_down(arena["floorSize"][1] / 2, 2),), (
                        arena["translation"]["x"] - round_up(arena["floorSize"][0] / 2, 2),
                        arena["translation"]["x"] + round_down(arena["floorSize"][0] / 2, 2),), Node.unit, )
        
        add_robot_and_goal(wbt)
        add_obstacles(wbt, np.random.choice(range(3, 8)))
        indexes.append(world_index)
        wbt.save()
