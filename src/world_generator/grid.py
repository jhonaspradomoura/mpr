import math
from math import ceil, sqrt

import numpy as np
from shapely.geometry import LineString, Polygon, Point, MultiPoint


class GridPoint(object):
    def __init__(self, x, z, valid):
        self.x = x
        self.z = z
        self.valid = valid
    
    def __str__(self):
        return self.z, self.x, self.valid
    
    def __repr__(self):
        return repr(self.__str__())


class Grid(object):
    def __init__(self, z_bounds, x_bounds, unit):
        self._grid = []
        self._bounds = (z_bounds, x_bounds)
        self._unit = unit
        self._polygons = []
        self.poly = Polygon([(z_bounds[0], x_bounds[0]), (z_bounds[0], x_bounds[1]), (z_bounds[1], x_bounds[1]),
                             (z_bounds[1], x_bounds[0])])
        self.coords_x = np.arange(x_bounds[0], x_bounds[1], unit)
        self.coords_z = np.arange(z_bounds[0], z_bounds[1], unit)
        self.coords_x = np.append(self.coords_x, x_bounds[1])
        self.coords_z = np.append(self.coords_z, z_bounds[1])
        for z in self.coords_z:
            tmp_grid = []
            for x in self.coords_x:
                point = GridPoint(np.round(x, 2), np.round(z, 2), True)
                tmp_grid.append(point)
            self._grid.append(tmp_grid)
        self._valid = [(coord.z, coord.x) for _z in self._grid for coord in _z if coord.valid]
    
    def coord2index(self, key):
        assert self._bounds[0][0] <= key[0] <= self._bounds[0][1] + self._unit, f"{key[0]} is not in {self._bounds[0]}"
        assert self._bounds[1][0] <= key[1] <= self._bounds[1][1] + self._unit, f"{key[1]} is not in {self._bounds[1]}"
        ind_z = ceil((np.round(key[0], 2) - self._bounds[0][0]) / self._unit)
        ind_x = ceil((np.round(key[1], 2) - self._bounds[1][0]) / self._unit)
        return ind_z, ind_x
    
    def diagonal(self):
        return math.sqrt(
                (self._bounds[0][0] - self._bounds[0][1]) ** 2 + (self._bounds[1][0] - self._bounds[1][1]) ** 2)
    
    @staticmethod
    def get_valid(arr):
        return arr[np.random.choice(len(arr))]
    
    def get_size_valid(self, size, style=3):
        size_val = self._valid.copy()
        for coords in self._valid:
            points = Point(coords).buffer(size, cap_style=style)
            if not self.poly.contains(points):
                size_val.remove(coords)
            else:
                for polygon in self._polygons:
                    if polygon.contains(points):
                        size_val.remove(coords)
        return size_val
    
    def determine_start_goal(self, robot_size):
        valid = self.get_size_valid(max(robot_size) / 2)
        pol = Polygon(
                [(.6 * self._bounds[0][0], .6 * self._bounds[1][0]),
                 (.6 * self._bounds[0][0], .6 * self._bounds[1][1]),
                 (.6 * self._bounds[0][1], .6 * self._bounds[1][1]),
                 (.6 * self._bounds[0][1], .6 * self._bounds[1][0])])
        valid = [v for v in valid if not Point(v).within(pol)]
        robot_start = self.get_valid(valid)
        robot_polygon = Point(robot_start).buffer(max(robot_size) / 2)
        self._polygons.append(robot_polygon)
        self.update_valid()
        valid = self.get_size_valid(max(robot_size) / 2, 1)
        result_coords = []
        for coord in valid:
            if Point(coord).distance(robot_polygon) > self.diagonal() / 2:
                result_coords.append(coord)
        robot_goal = self.get_valid(result_coords)
        goal_polygon = Point(robot_goal).buffer(max(robot_size) / 2, cap_style=1)
        self._polygons.append(goal_polygon)
        self.update_valid()
        return robot_start, robot_goal
    
    def determine_obj_pos_size(self, x_size):
        valid = self.get_size_valid(x_size)
        start = self.get_valid(valid)
        possible_coords = []
        for end in valid:
            if end is not start:
                line = LineString([start, end])
                line_pol = line.buffer(x_size, cap_style=3)
                for polygon in self._polygons:
                    if line_pol.intersects(polygon) or not self.poly.contains(line_pol.buffer(-1e-10, cap_style=3)):
                        break
                else:
                    if self.poly.contains(line_pol.buffer(-1e-10, cap_style=3)):
                        if end[0] - start[0] == 0:
                            angle = 0
                        else:
                            angle = np.arctan((end[1] - start[1]) / (end[0] - start[0]))
                        possible_coords.append((start, end, np.round(line.length, 2), np.round(angle, 2)))
        return possible_coords[np.random.choice(len(possible_coords))]

    def determine_obj_pos(self, size, angled=False):
        valid = self.get_size_valid(size)
        start = self.get_valid(valid)
        if angled:
            angle = np.random.choice(range(0,360))
        else:
            angle = 0
        end = start[0] + size * math.cos(angle), start[1] + size * math.sin(angle)
        return start, end, angle
    
    def update(self, start, end, width):
        line = LineString([start, end])
        polygon = line.buffer(width + .25, cap_style=3)
        self._polygons.append(polygon)
        self.update_valid()
    
    def update_valid(self):
        for z in self.coords_z:
            for x in self.coords_x:
                for polygon in self._polygons:
                    if polygon.contains(Point(np.round(z, 2), np.round(x, 2))) and \
                            self[(np.round(z, 2), np.round(x, 2))]:
                        self[(np.round(z, 2), np.round(x, 2))] = False
        self._valid = [(coord.z, coord.x) for _z in self._grid for coord in _z if coord.valid]
    
    def available_area(self):
        available = self.poly.area
        for poly in self._polygons:
            available -= poly.area
        return available
    
    def __setitem__(self, key, value):
        assert isinstance(value, bool), f'{value} is not boolean.'
        assert isinstance(key, tuple), f"{key} is not a tuple."
        index = self.coord2index(key)
        try:
            self._grid[index[0]][index[1]].valid = value
        except IndexError:
            pass
    
    def __getitem__(self, item):
        index = self.coord2index(item)
        return self._grid[index[0]][index[1]]
    
    def __iter__(self):
        for z in range(len(self._grid)):
            for x in range(len(self._grid[z])):
                yield self._grid[z][x]
