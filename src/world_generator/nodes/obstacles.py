from nodes.appearance import PBRAppearance
from nodes.constraints import Constraint
from nodes.generic import Node


class Transform(Node):
    def __init__(self, geometry, color, *args, **kwargs):
        super().__init__('Transform')
        self['translation'] = {'x': 0, 'y': 0, 'z': 0}
        self['rotation'] = {'x': 0, 'y': 1, 'z': 0, 'angle': 0}
        self['children'] = {}
        self['children']['shape'] = (Shape(geometry, color))


class Shape(Node):
    def __init__(self, geometry, color: dict, *args, **kwargs):
        super().__init__('Shape')
        assert not isinstance(color, tuple) and \
               not isinstance(geometry, tuple), 'Must define geometry and color to create a shape.'
        self['appearance'] = PBRAppearance()
        self['appearance']['baseColor'] = color
        self['appearance']['baseColorMap'] = None
        self['appearance']['transparency'] = 0.15
        self['geometry'] = geometry()


class Cylinder(Node):
    def __init__(self, *args, **kwargs):
        super().__init__('Cylinder')
        self['height'] = .5
        self['radius'] = .25


class BoxGeometry(Node):
    def __init__(self, *args, **kwargs):
        super().__init__('Box', *args, **kwargs)
        self['size'] = [.5, .5, .5]


class SolidBox(Node):
    def __init__(self, name='Box', *args, **kwargs):
        assert name in ('Box', 'Pipe', 'RoundedBox', 'Torus'), 'Box must be in (Box, Pipe, RoundedBox, Torus)'
        super(SolidBox, self).__init__(f"Solid{name}")
        self['translation'] = {'x': 0, 'y': 0, 'z': 0}
        self['rotation'] = {'x': 0, 'y': 1, 'z': 0, 'angle': 0}
        self['contactMaterial'] = "default"
        self['appearance'] = PBRAppearance(random=True)
        self['appearance']['baseColor'] = {'red': 0.768627, 'green': 0.62451, 'blue': 0}
        self['castShadows'] = True


class Box(SolidBox):
    def __init__(self, *args, **kwargs):
        super(Box, self).__init__('Box')
        self.constraints['size'] = Constraint(3 * [Node.unit])
        self['size'] = 3 * [Node.unit]
    
    def set_size_constraint(self, _max):
        self.constraints['size'].set(None, _max)


class Pipe(SolidBox):
    def __init__(self, *args, **kwargs):
        super(Pipe, self).__init__('Pipe')
        self.constraints['height'] = Constraint(Node.unit)
        self.constraints['radius'] = Constraint(Node.unit)
        self.constraints['thickness'] = Constraint(Node.unit)
        self.constraints['accuracy'] = Constraint(0)
        self['height'] = Node.unit
        self['radius'] = Node.unit
        self['thickness'] = Node.unit
        self['accuracy'] = .0001


class RoundedBox(SolidBox):
    def __init__(self, *args, **kwargs):
        super(RoundedBox, self).__init__('RoundedBox')
        self.constraints['size'] = Constraint(3 * [2 * Node.unit])
        self['size'] = 3 * [2 * Node.unit]
        
        self.constraints['borderRadius'] = Constraint(min(self.constraints['size'].min) / 2, _max=min(self['size']) / 2)
        self.add_reference('borderRadius', "None, min(self['size'])/2")
        self['borderRadius'] = Node.unit
    
    def set_size_constraint(self, _max):
        self.constraints['size'].set(None, _max)


class Torus(SolidBox):
    def __init__(self, *args, **kwargs):
        super(Torus, self).__init__('Torus')
        self.constraints['minorRadius'] = Constraint(Node.unit)
        self['minorRadius'] = Node.unit
        
        self.constraints['majorRadius'] = Constraint(self['minorRadius'] + Node.unit)
        self.add_reference('majorRadius', "self['minorRadius'] + .1, None")
        self['majorRadius'] = 2 * Node.unit
