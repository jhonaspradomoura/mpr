import random

from nodes.constraints import Constraint
from nodes.generic import Node
from os import path


class ImageTexture(Node):
    def __init__(self, texture):
        super(ImageTexture, self).__init__('ImageTexture')
        self['url'] = f"textures/{texture}.jpg"
        if not path.exists(f"/usr/local/webots/projects/default/worlds/{self['url']}"):
            raise FileNotFoundError


class PBRAppearance(Node):
    def __init__(self, *args, **kwargs):
        super(PBRAppearance, self).__init__('PBRAppearance')
        self.constraints['transparency'] = Constraint(0, 1)
        self.constraints['roughness'] = Constraint(0, 1)
        self.constraints['metalness'] = Constraint(0, 1)
        self.constraints['IBLStrength'] = Constraint(0, 1)
        
        self['baseColor'] = {'red': 1, 'green': 1, 'blue': 1}
        
        if kwargs.get('random', True):
            textures = ['red_brick_wall', 'old_brick_wall', 'gray_brick_wall']
            texture = random.choice(textures)
        elif kwargs.get('texture', None):
            texture = kwargs['texture']
        else:
            raise ValueError('Se nao for randomico, voce deve disponibilizar o nome da textura')
        
        self['baseColorMap'] = ImageTexture(texture=texture)
        
        self['transparency'] = 0
        self['roughness'] = 0.5
        self['metalness'] = 0.5
        self['IBLStrength'] = 0.5
