from typing import Iterable


class Constraint:
    def __init__(self, _min=-float("inf"), _max=float("inf")):
        if not isinstance(_min, list):
            self.min = [_min]
        else:
            self.min = _min
        if not isinstance(_max, list):
            self.max = [_max]
        else:
            self.max = _max

    def set(self, _min=None, _max=None):
        if isinstance(_min, Iterable) and isinstance(_max, Iterable):
            for ind, minmax in enumerate(zip(_min, _max)):
                assert (not minmax[0] and self.min[ind] < minmax[1]) or (not minmax[1] and minmax[0] < self.max[ind]) or \
                       (minmax[0] <= minmax[1]), f'min {minmax[0]} nao eh menor que max {minmax[1]}'
        if _min and not isinstance(_min, Iterable):
            _min = [_min]
        if _max and not isinstance(_max, Iterable):
            _max = [_max]
        if _min:
            self.min = _min
        if _max:
            self.max = _max

    def enforce(self, value):
        if value is not None:
            if not isinstance(value, Iterable):
                value = [value]
            if not isinstance(value, str) and not isinstance(value, dict):
                for val, mi, ma in zip(value, self.min, self.max):
                    if not (mi <= val <= ma):
                        raise ConstraintError(f"{value} does not obey constraint: {self}")
            elif isinstance(value, dict):
                for val, mi, ma in zip(value.values(), self.min, self.max):
                    if not (mi <= val <= ma):
                        raise ConstraintError(f"{value} does not obey constraint: {self}")

    def __repr__(self):
        return f"[{self.min}, {self.max}]"


class ConstraintError(Exception):
    pass
