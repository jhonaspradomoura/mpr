import os

from nodes.obstacles import *
import numpy as np


def create(name=None, geometry=None, color=None, file=None):
    if name not in ["Rectangle", "Circle"]:
        obj = eval(name)
        assert issubclass(obj, Node), f"{name} is supposed to be a subclass of Node, instead it is {obj}"
    else:
        obj = Arena
    return obj(name=name, geometry=geometry, color=color, file=file)


class WorldInfo(Node):
    def __init__(self, *args, **kwargs):
        super(WorldInfo, self).__init__("WorldInfo")
        self.constraints["ERP"] = Constraint(0.1, 0.8)
        self.constraints["basicStepTime"] = Constraint(0)
        self.constraints["lineScale"] = Constraint(0)
        self.constraints["randomSeed"] = Constraint(-1)

        self["title"] = "Training World"
        self["gravity"] = {"x": 0, "y": -9.81, "z": 0}
        self["ERP"] = 0.2
        self["basicTimeStep"] = 32
        self["FPS"] = 60
        self["randomSeed"] = 0
        self["lineScale"] = 0.1
        self["physicsDisableTime"] = 1
        self["physicsDisableLinearThreshold"] = 0.01
        self["physicsDisableAngularThreshold"] = 0.01


class Viewpoint(Node):
    def __init__(self, *args, **kwargs):
        super().__init__("Viewpoint")
        self["fieldOfView"] = 0.785
        self["orientation"] = {"x": 1, "y": 0, "z": 0, "angle": 4.71239}
        self["position"] = {"x": 0, "y": 20, "z": 0}
        self["follow"] = "Pioneer 3-AT"


class TexturedBackground(Node):
    def __init__(self, *args, **kwargs):
        super().__init__("TexturedBackground")
        self["texture"] = "mountains"


class TexturedBackgroundLight(Node):
    def __init__(self, *args, **kwargs):
        super().__init__("TexturedBackgroundLight")
        self["texture"] = "mountains"
        self["castShadows"] = True


class Arena(Node):
    def __init__(self, name="Rectangle", *args, **kwargs):
        assert name in ["Rectangle", "Circle"], "Floor must be either Circle or Rectangle"
        super().__init__(f"{name}Arena")
        self.constraints["floorTileSize"] = Constraint(0)

        self["translation"] = {"x": 0, "y": 0, "z": 0}
        self["wallThickness"] = 0.01
        self["wallHeight"] = 0.75
        self["contactMaterial"] = "default"

        if name is "Rectangle":
            self.constraints["floorSize"] = Constraint(2 * [20 * Node.unit], 2 * [500 * Node.unit])
            self["floorSize"] = (10, 10)

            self["floorTileSize"] = [2 * coord for coord in self["floorSize"]]
            self["rotation"] = {"x": 0, "y": 1, "z": 0, "angle": 0}
        else:
            self.constraints["radius"] = Constraint(10 * Node.unit, 500 * Node.unit)
            self["radius"] = np.round(
                np.random.uniform(self.constraints["radius"].min, self.constraints["radius"].max, size=1), 2
            )
            self["floorTileSize"] = 2 * [2 * self["radius"]]

    def diagonal(self):
        return np.round(np.sqrt(self["floorSize"][0] ** 2 + self["floorSize"][1] ** 2), 2)


class Lidar(Node):
    def __init__(self, name, *args, **kwargs):
        super().__init__(name)

    def position(self, translation, rotation):
        self["translation"] = translation
        self["rotation"] = rotation


class Robot(Node):
    def __init__(self, file, *args, **kwargs):
        super().__init__("DEF PIONEER3_AT Pioneer3at")
        with open(f"{os.getcwd().split('src/')[0]}protos/{file}.wbo", "r") as f:
            self.lines = f.readlines()[1:-1]
        self["name"] = "Pioneer 3-AT"
        self.size = [0.497, 0.277, 0.508]
        self.lines.append("  supervisor TRUE\n}")

    def position(self, translation, rotation=None):
        no_braces = 0
        for ind, line in enumerate(self.lines):
            if "{" in line:
                no_braces += 1
            if "}" in line:
                no_braces -= 1
            if "translation" in line and no_braces == 1:
                self.lines[ind] = f"  translation {translation}\n"
                break
        if rotation:
            self.lines.insert(-1, f"  rotation {rotation}\n")

    def __str__(self):
        res = "".join(self.lines)
        return res
