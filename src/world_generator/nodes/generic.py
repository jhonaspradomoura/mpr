from typing import Iterable

from nodes.constraints import Constraint


class Node(dict):
    unit = 0.05
    
    def __init__(self, name, *args, **kwargs):
        super().__init__()
        self.name = name
        self.constraints = {}
        self.int_reference_args = {}
        self.int_reference_watcher = {}
    
    def add_reference(self, key, args: str):
        if ',' not in args:
            args += ", float('inf')"
        self.int_reference_args[key] = args
        str_args = args.split(',')
        for arg in str_args:
            if 'self' in arg:
                res_key = arg.split('[')[1].split(']')[0].replace('\'', '')
                if res_key not in self.int_reference_watcher.keys():
                    self.int_reference_watcher[res_key] = []
                if key not in self.int_reference_watcher and res_key in self:
                    self.int_reference_watcher[res_key].append(key)
    
    def update_reference(self, key):
        self.constraints[key].set(*eval(self.int_reference_args[key]))
    
    def __hash__(self):
        return hash(str(self))
    
    def __setitem__(self, key, value):
        if key in self.constraints.keys():
            if key in self.int_reference_args:
                self.update_reference(key)
            super(Node, self).__setitem__(key, value)
            self.constraints[key].enforce(value)
            
            if key in self.int_reference_watcher.keys():
                for val in self.int_reference_watcher[key]:
                    self.update_reference(val)
                    if isinstance(self[val], Iterable) and not isinstance(self[val], dict):
                        for it in range(len(self[val])):
                            if self[val][it] < self.constraints[val].min[it]:
                                self[val][it] = self.constraints[val].min[it]
                            elif self[val][it] > self.constraints[val].max[it]:
                                self[val][it] = self.constraints[val].max[it]
                    elif isinstance(self[val], dict):
                        for it, ks in enumerate(self[val].keys()):
                            if self[val][ks] < self.constraints[val].min[it]:
                                self[val][ks] = self.constraints[val].min[it]
                            elif self[val][ks] > self.constraints[val].max[it]:
                                self[val][ks] = self.constraints[val].max[it]
                    else:
                        if self[val] < min(self.constraints[val].min):
                            self[val] = self.constraints[val].min
                        elif self[val] > min(self.constraints[val].max):
                            self[val] = self.constraints[val].max
        
        else:
            super(Node, self).__setitem__(key, value)
            self.constraints[key] = Constraint()
    
    def __str__(self):
        res = f"{self.name} ""{"
        for item, value in self.items():
            if value is not None:
                if not isinstance(value, Iterable) or isinstance(value, Node) or isinstance(value, str):
                    res += f"\n{item}"
                    
                    if isinstance(value, str):
                        res += f" \"{value}\""
                    elif isinstance(value, bool):
                        if value:
                            res += " TRUE"
                        else:
                            res += " FALSE"
                    else:
                        res += f" {value}"
                else:
                    if isinstance(value, list) and isinstance(value[0], Node):
                        res += f" \n{item} [\n\t"
                    else:
                        res += f"\n{item}"
                    if isinstance(value, dict):
                        value = value.values()
                    for element in value:
                        if isinstance(element, str):
                            res += f" \"{element}\""
                        elif isinstance(element, Iterable) and not isinstance(element, Node):
                            
                            for el in element:
                                if isinstance(el, str):
                                    res += f" \"{el}\""
                                elif isinstance(value, bool):
                                    if value:
                                        res += " TRUE"
                                    else:
                                        res += " FALSE"
                                else:
                                    res += f" {el}"
                        else:
                            res += f" {element}"
                    if isinstance(value, list) and isinstance(value[0], Node):
                        res += "\n]"
        res = res.replace("\n", "\n  ")
        res += "\n}"
        
        return res
    
    def __repr__(self):
        return self.__str__()
