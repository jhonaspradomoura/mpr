from nodes import node, obstacles


class Wbton:
    def __init__(self, filename, header="#VRML_SIM R2019b utf8"):
        self._filename = f"{__file__.split('/src/world_generator')[0]}/worlds/world_{filename}.wbt"
        self._version = header
        self._nodes = {}

    def __getitem__(self, item):
        return self._nodes[item]

    def add_node(self, key, geometry=None, color=None, file=None):
        fake_key = key
        if key in ('Rectangle', 'Circle'):
            fake_key = 'Arena'
        if key in self._nodes.keys():
            count_key = len(self._nodes[key])
            self._nodes[fake_key].append(node.create(
                key, geometry=geometry, color=color, file=file))
            self._nodes[fake_key][-1]['name'] = f'{self._nodes[key][0].name}({count_key})'
        else:
            self._nodes[fake_key] = []
            self._nodes[fake_key].append(node.create(
                key, geometry=geometry, color=color, file=file))
        return self._nodes[fake_key][-1]

    def remove_item(self, _node, item):
        self._nodes[_node].pop(item)

    def save(self):
        with open(self._filename, 'w') as file:
            file.write(repr(self))

    def __str__(self):
        res = f'{self._version}\n'
        for node_list in self._nodes.values():
            for _node in node_list:
                res += f'{_node}\n'
        return res

    def __repr__(self):
        return self.__str__()
