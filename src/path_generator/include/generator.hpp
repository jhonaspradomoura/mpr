#include "webots_ros/supervisor_get_from_def.h"
#include "webots_ros/node_get_field.h"
#include "webots_ros/field_set_vec3f.h"
#include "webots_ros/field_get_vec3f.h"
#include "webots_ros/field_get_rotation.h"
#include "webots_ros/field_set_vec3f.h"
#include "webots_ros/set_string.h"
#include "webots_ros/get_bool.h"
#include "webots_ros/set_int.h"

#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Vector3.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/Path.h>
#include <nav_msgs/GetPlan.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Header.h>
#include <std_msgs/Empty.h>
#include <tf/tf.h>
#include <ros/ros.h>
#include <rosbag/bag.h>

#include "path_generator/availableMaps.h"
#include "path_generator/changeMap.h"
#include "path_generator/changeMapFile.h"
#include "path_generator/Paths.h"
#include "path_generator/PixelPath.h"
#include "path_generator/Pixel.h"

#include "mpnet/ready.h"
#include "mpnet/npMapList.h"
#include "mpnet/npMapRequest.h"

#include "cnpy.h"

#include <random>
#include <functional>
#include <mutex>
#include <condition_variable>
#include <cmath>
#include <filesystem>

namespace fs = std::filesystem;

class PathGenerator
{
	struct Coords
	{
		int width;
		int height;
	};
	
	struct XYZCoords
	{
		float x;
		float y;
		float z;
		
	};
public:
	PathGenerator();
	
	~PathGenerator();

private:
	ros::NodeHandle         nh;
	nav_msgs::OccupancyGrid occupGridMsg;
	
	ros::Publisher gpsPub;
	ros::Publisher imuPub;
	
	uint64_t            nodeId{};
	std::vector<int8_t> occupancyMap;
	
	int      centerId;
	uint16_t numMaps;
	uint16_t numOrigins;
	uint16_t numGoals;
	
	uint16_t numUnseenMaps;
	uint16_t numValidOriginsUnseen;
	uint16_t numValidGoalsUnseen;
	
	uint16_t numValidOriginsSeen;
	uint16_t numValidGoalsSeen;
	
	static uint64_t getRandomPoint(const std::vector<std::vector<int8_t>::iterator> &validIndexes);
	
	XYZCoords mapToCoords(const std::vector<std::vector<int8_t>::iterator> &validIndexes, const uint64_t &chosenIndex);
	
	std::vector<std::vector<int8_t>::iterator> getValid();
	
	PathGenerator::XYZCoords
	getRandomGoals(XYZCoords currentPos, const std::vector<std::vector<int8_t>::iterator> &validIndexes);
	
	
	nav_msgs::Path generatePaths(const XYZCoords currentPos, const XYZCoords &goal);
	
	void sendAgentPosition(XYZCoords currentPos);
	
	void run(std::string envIdx, bool training, std::vector<geometry_msgs::Point32> centers);
	
	static bool validatePath(nav_msgs::Path &path);
	
	static std::vector<path_generator::PixelPath>
	pathsToImg(const std::vector<nav_msgs::Path> &, nav_msgs::MapMetaData);
	
	static void save(const std::vector<nav_msgs::Path> &paths, const std::vector<path_generator::PixelPath> &imgPaths,
	                 const nav_msgs::OccupancyGrid &map, const mpnet::Obstacles &obstacles);
	
	bool
	savePath(const nav_msgs::Path path, std::string envIdx, bool training, std::vector<geometry_msgs::Point32> centers);
	
	std::vector<float> lvc(std::vector<float> pathData, std::vector<geometry_msgs::Point32> centers);
	
	bool steerTo(std::vector<float> start, std::vector<float> end, std::vector<geometry_msgs::Point32> centers);
	
	void loadNumpyMaps();
	
};




