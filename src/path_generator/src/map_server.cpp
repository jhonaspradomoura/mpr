/*
 * Copyright (c) 2008, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/* Author: Brian Gerkey */

#define USAGE "\nUSAGE: map_server <map.yaml>\n" \
              "  map.yaml: map description file\n" \
              "DEPRECATED USAGE: map_server <map> <resolution>\n" \
              "  map: image file to load\n"\
              "  resolution: map resolution [meters/pixel]"

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <boost/filesystem.hpp>

#include "ros/ros.h"
#include "ros/console.h"
#include "image_loader.h"
#include "nav_msgs/MapMetaData.h"
#include "yaml-cpp/yaml.h"
#include "path_generator/changeMapFile.h"
#include "path_generator/availableMaps.h"
#include "path_generator/changeMap.h"
#include <filesystem>

namespace fs = std::filesystem;

#ifdef HAVE_YAMLCPP_GT_0_5_0


// The >> operator disappeared in yaml-cpp 0.5, so this function is
// added to provide support for code written under the yaml-cpp 0.3 API.
template<typename T>
void operator>>(const YAML::Node &node, T &i)
{
	
	i = node.as<T>();
}


#endif

class MapServer
{
public:
	/** Trivial constructor */
	MapServer()
	{
		
		queryMapsService = n.advertiseService("/map/list", &MapServer::queryMaps);
		staticMapService = n.advertiseService("static_map", &MapServer::mapCallback, this);
		changeMapService = n.advertiseService("/map/change", &MapServer::readFile, this);
		setMapService    = n.advertiseService("/map/set", &MapServer::receiveMap, this);
		
		metadata_pub = n.advertise<nav_msgs::MapMetaData>("map_metadata", 1, true);
		map_pub      = n.advertise<nav_msgs::OccupancyGrid>("map", 1, true);
	}
	
	
	static bool queryMaps(path_generator::availableMaps::Request &req, path_generator::availableMaps::Response &res)
	{
		
		std::string originalPath = __FILE__;
		std::size_t mprSubstring = originalPath.find("mpr");
		std::string path         = originalPath.substr(0, mprSubstring + 4) + "src/navigation/maps/cropped";
		for (auto   &entry : fs::directory_iterator(path))
		{
			if (entry.path().string().find(".yaml") != std::string::npos)
			{
				res.maps.push_back(entry.path());
			}
		}
		return true;
	}
	
	
	bool receiveMap(path_generator::changeMap::Request &req, path_generator::changeMap::Response &resp)
	{
		
		ROS_INFO("Received new map..");
		map_resp_.map.info.map_load_time = ros::Time::now();
		map_resp_.map.info.width         = req.metadata.width;
		map_resp_.map.info.height        = req.metadata.height;
		map_resp_.map.info.resolution    = req.metadata.resolution;
		map_resp_.map.info.origin        = req.metadata.origin;
		map_resp_.map.data               = req.map.data;
		map_resp_.map.header             = req.map.header;
		
		metadata_pub.publish(req.metadata);
		map_pub.publish(req.map);
		
		resp.result = true;
		return true;
	}
	
	
	bool readFile(path_generator::changeMapFile::Request &req, path_generator::changeMapFile::Response &resp)
	{
		
		std::string     fname = req.map.data;
		std::string     mapfname;
		double          origin[3];
		int             negate;
		double          occ_th, free_th;
		MapMode         mode  = TRINARY;
		std::string     frame_id;
		ros::NodeHandle private_nh("~");
		private_nh.param("frame_id", frame_id, std::string("map"));
		std::ifstream fin(fname.c_str());
		double        res;
		if (fin.fail())
		{
			ROS_ERROR("Map_server could not open %s.", fname.c_str());
			exit(-1);
		}
#ifdef HAVE_YAMLCPP_GT_0_5_0
		// The document loading process changed in yaml-cpp 0.5.
		YAML::Node doc = YAML::Load(fin);
#else
		YAML::Parser parser(fin);
		YAML::Node   doc;
		parser.GetNextDocument(doc);
#endif
		try
		{
			doc["resolution"] >> res;
		}
		catch (YAML::InvalidScalar &)
		{
			ROS_ERROR("The map does not contain a resolution tag or it is invalid.");
			exit(-1);
		}
		try
		{
			doc["negate"] >> negate;
		}
		catch (YAML::InvalidScalar &)
		{
			ROS_ERROR("The map does not contain a negate tag or it is invalid.");
			exit(-1);
		}
		try
		{
			doc["occupied_thresh"] >> occ_th;
		}
		catch (YAML::InvalidScalar &)
		{
			ROS_ERROR("The map does not contain an occupied_thresh tag or it is invalid.");
			exit(-1);
		}
		try
		{
			doc["free_thresh"] >> free_th;
		}
		catch (YAML::InvalidScalar &)
		{
			ROS_ERROR("The map does not contain a free_thresh tag or it is invalid.");
			exit(-1);
		}
		try
		{
			std::string modeS;
			doc["mode"] >> modeS;
			
			if (modeS == "trinary")
			{
				mode = TRINARY;
			}
			else if (modeS == "scale")
			{
				mode = SCALE;
			}
			else if (modeS == "raw")
			{
				mode = RAW;
			}
			else
			{
				ROS_ERROR("Invalid mode tag \"%s\".", modeS.c_str());
				exit(-1);
			}
		}
		catch (YAML::Exception &)
		{
			ROS_DEBUG("The map does not contain a mode tag or it is invalid... assuming Trinary");
			mode = TRINARY;
		}
		try
		{
			doc["origin"][0] >> origin[0];
			doc["origin"][1] >> origin[1];
			doc["origin"][2] >> origin[2];
		}
		catch (YAML::InvalidScalar &)
		{
			ROS_ERROR("The map does not contain an origin tag or it is invalid.");
			exit(-1);
		}
		try
		{
			doc["image"] >> mapfname;
			// TODO: make this path-handling more robust
			if (mapfname.empty())
			{
				ROS_ERROR("The image tag cannot be an empty string.");
				exit(-1);
			}
			uint8_t slashIndex = mapfname.find('/');
			
			mapfname = mapfname.substr(slashIndex + 1);
			
			boost::filesystem::path mapfpath(mapfname);
			if (!mapfpath.is_absolute())
			{
				boost::filesystem::path dir(fname);
				dir      = dir.parent_path();
				mapfpath = dir / mapfpath;
				mapfname = mapfpath.string();
			}
		}
		catch (YAML::InvalidScalar &)
		{
			ROS_ERROR("The map does not contain an image tag or it is invalid.");
			exit(-1);
		}
		
		ROS_INFO("Loading map from image \"%s\"", mapfname.c_str());
		try
		{
			map_server::loadMapFromFile(&map_resp_, mapfname.c_str(), res, negate, occ_th, free_th, origin, mode);
		}
		catch (std::runtime_error &e)
		{
			ROS_ERROR("%s", e.what());
			exit(-1);
		}
		ros::Time::waitForValid();
		map_resp_.map.info.map_load_time = ros::Time::now();
		map_resp_.map.header.frame_id    = frame_id;
		map_resp_.map.header.stamp       = ros::Time::now();
		ROS_INFO("Read a %d X %d map @ %.3lf m/cell", map_resp_.map.info.width, map_resp_.map.info.height,
		         map_resp_.map.info.resolution);
		meta_data_message_ = map_resp_.map.info;
		
		// Latched publisher for metadata
		metadata_pub.publish(meta_data_message_);
		
		// Latched publisher for data
		map_pub.publish(map_resp_.map);
		
		resp.result = true;
		
		return true;
	}

private:
	ros::NodeHandle    n;
	ros::Publisher     map_pub;
	ros::Publisher     metadata_pub;
	ros::ServiceServer staticMapService;
	ros::ServiceServer changeMapService;
	ros::ServiceServer queryMapsService;
	ros::ServiceServer setMapService;
	
	
	/** Callback invoked when someone requests our service */
	bool mapCallback(nav_msgs::GetMap::Request &req, nav_msgs::GetMap::Response &res)
	{
		// request is empty; we ignore it
		
		// = operator is overloaded to make deep copy (tricky!)
		res = map_resp_;
		ROS_INFO("Sending map");
		
		return true;
	}
	
	
	/** The map data is cached here, to be sent out to service callers
	 */
	nav_msgs::MapMetaData      meta_data_message_;
	nav_msgs::GetMap::Response map_resp_;
	
	/*
	void metadataSubscriptionCallback(const ros::SingleSubscriberPublisher& pub)
	{
	  pub.publish( meta_data_message_ );
	}
	*/
	
};


int main(int argc, char **argv)
{
	
	ros::init(argc, argv, "map_server", ros::init_options::AnonymousName);
	try
	{
		MapServer ms;
		ros::spin();
	}
	catch (std::runtime_error &e)
	{
		ROS_ERROR("map_server exception: %s", e.what());
		return -1;
	}
	
	return 0;
}
