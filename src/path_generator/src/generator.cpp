#include "generator.hpp"
#include <cinttypes>
#include <numeric>
#include <utility>

PathGenerator::PathGenerator() {

    nh = ros::NodeHandle();

    gpsPub = nh.advertise<sensor_msgs::NavSatFix>("/pioneer3at/gps/values", 1, true);
    imuPub = nh.advertise<sensor_msgs::Imu>("/pioneer3at/inertial_unit/roll_pitch_yaw", 1, true);

    numOrigins = nh.param("/generator/origins", 80);
    numGoals = nh.param("/generator/goals", 50);
    numMaps = nh.param("/generator/maps", 0);
    centerId = nh.param("/generator/center", 0);

    numUnseenMaps = nh.param("/generator/unseen", 40);
    numValidOriginsUnseen = nh.param("/generator/unseen_origins", 50);
    numValidGoalsUnseen = nh.param("/generator/unseen_goals", 40);

    numValidOriginsSeen = nh.param("/generator/seen_origins", 20);
    numValidGoalsSeen = nh.param("/generator/seen_goals", 10);

    ROS_INFO("Parametros:\n%d\t%d\t%d\t%d", numOrigins, numGoals, numMaps, centerId);

    ROS_INFO("Começando gerador");
    loadNumpyMaps();
}

PathGenerator::~PathGenerator() = default;

std::vector<std::vector<int8_t>::iterator> PathGenerator::getValid() {

    std::vector<std::vector<int8_t>::iterator> validIndexes;
    auto occlusionIter = occupancyMap.begin();
    std::vector<int> distances;
    while ((occlusionIter = std::find_if(occlusionIter, occupancyMap.end(), [](int8_t iter) { return iter == 0; })) !=
           occupancyMap.end()) {
        validIndexes.push_back(occlusionIter);
        distances.push_back(std::distance(occupancyMap.begin(), occlusionIter));
        occlusionIter++;
    }
    return validIndexes;
}

uint64_t PathGenerator::getRandomPoint(const std::vector<std::vector<int8_t>::iterator> &validIndexes) {

    ROS_INFO("Num of valid indexes %zu", validIndexes.size());
    std::mt19937::result_type seed = time(nullptr);
    auto dice_rand = std::bind(std::uniform_int_distribution<int>(0, validIndexes.size() - 1), std::mt19937(seed));

    return dice_rand();
}

PathGenerator::XYZCoords PathGenerator::mapToCoords(const std::vector<std::vector<int8_t>::iterator> &validIndexes,
                                                    const uint64_t &chosenIndex) {

    int coordWidth, coordHeight;

    std::vector<Coords> chosenCoords;
    // Transformar as posições aleatórias escolhidas no costmap para coordenadas
    long dataIndexMap = std::distance(occupancyMap.begin(), validIndexes.at(chosenIndex));
    coordHeight = floor(dataIndexMap / occupGridMsg.info.width);
    coordWidth = (int)(dataIndexMap - coordHeight * occupGridMsg.info.width);
    Coords coord{};
    coord.width = coordWidth;
    coord.height = coordHeight;
    chosenCoords.push_back(coord);

    XYZCoords originCoord{};

    originCoord.x = (float)(occupGridMsg.info.origin.position.x + coord.width * occupGridMsg.info.resolution);
    originCoord.y = (float)(occupGridMsg.info.origin.position.y + coord.height * occupGridMsg.info.resolution);
    originCoord.z = 0.108;
    return originCoord;
}

PathGenerator::XYZCoords PathGenerator::getRandomGoals(XYZCoords currentPos,
                                                       const std::vector<std::vector<int8_t>::iterator> &validIndexes) {
    // Transforma o sistema cartesiano de currentPos: Webots -> RViz (y = x, x = -z, z = y)
    //	currentPos.y                        = currentPos.x;
    //	currentPos.x                        = -currentPos.z;
    //	currentPos.z                        = 0.108;

    std::random_device device;
    std::mt19937 generator(device());
    std::uniform_int_distribution<int> distribution(0, validIndexes.size() - 1);

    std::vector<uint64_t> chosenIndexes;
    XYZCoords coords{};
    do {
        int idx = std::distance(occupancyMap.begin(), validIndexes.at(distribution(generator)));
        Coords matrixCoord{};
        matrixCoord.height = floor(idx / occupGridMsg.info.width);
        matrixCoord.width = (int)(idx - matrixCoord.height * occupGridMsg.info.width);

        coords.x = occupGridMsg.info.origin.position.x + matrixCoord.height * occupGridMsg.info.resolution;
        coords.y = occupGridMsg.info.origin.position.y + matrixCoord.width * occupGridMsg.info.resolution;
        coords.z = 0.108;
    } while (sqrt(pow(coords.x - currentPos.x, 2) + pow(coords.y - currentPos.y, 2)) < 2);

    return coords;
}

nav_msgs::Path PathGenerator::generatePaths(const XYZCoords currentPos, const XYZCoords &goal) {

    std::vector<nav_msgs::Path> paths;

    ros::ServiceClient planSrv = nh.serviceClient<nav_msgs::GetPlan>("/move_base/make_plan");
    nav_msgs::GetPlan planMsg;
    planMsg.request.start.header = std_msgs::Header();
    planMsg.request.start.header.frame_id = "map";

    planMsg.request.goal.header = std_msgs::Header();
    planMsg.request.goal.header.frame_id = "map";
    planMsg.request.start.header.stamp = ros::Time::now();
    planMsg.request.start.pose.position.x = currentPos.x;
    planMsg.request.start.pose.position.y = currentPos.y;
    planMsg.request.start.pose.position.z = 0.108;

    planMsg.request.start.pose.orientation.y = 1;

    planMsg.request.goal.header.stamp = ros::Time::now();

    planMsg.request.goal.pose.position.x = goal.x;
    planMsg.request.goal.pose.position.y = goal.y;
    planMsg.request.goal.pose.position.z = goal.z;

    planMsg.request.goal.pose.orientation.y = 1;

    planMsg.request.tolerance = 0.05;
    while (!planSrv.call(planMsg)) {
    }
    //		sleep(1);
    return planMsg.response.plan;
}

void PathGenerator::sendAgentPosition(XYZCoords currentPos) {

    sensor_msgs::NavSatFix gpsMsg;
    sensor_msgs::Imu imuMsg;

    gpsMsg.header.stamp = ros::Time::now();
    gpsMsg.header.frame_id = "pionner3at/gps";
    gpsMsg.longitude = -currentPos.x;
    gpsMsg.latitude = currentPos.y;
    gpsMsg.altitude = 0.108;
    gpsMsg.status.service = 1;

    imuMsg.header.stamp = ros::Time::now();
    imuMsg.header.frame_id = "pioneer3at/inertial_unit";
    imuMsg.angular_velocity_covariance = {-1., 0., 0., 0., 0., 0., 0., 0., 0.};
    imuMsg.linear_acceleration_covariance = {-1., 0., 0., 0., 0., 0., 0., 0., 0.};
    imuMsg.orientation.w = 1;
    imuMsg.orientation.x = 0;
    imuMsg.orientation.y = 0;
    imuMsg.orientation.z = 0;

    gpsPub.publish(gpsMsg);
    imuPub.publish(imuMsg);
}

void PathGenerator::loadNumpyMaps() {

    std::string filepath = __FILE__;
    std::size_t strIdx = filepath.find("mpr");
    filepath = filepath.substr(0, strIdx + 3) + "/paths";

    ROS_INFO("Start loading maps");

    std::vector<std::string> mapsList;

    ros::ServiceClient mapsListSrv = nh.serviceClient<mpnet::npMapList>("/map/choose");
    mpnet::npMapList mapsListMsg;
    mapsListMsg.request.qtt = numMaps + numUnseenMaps;
    mapsListMsg.request.center_id = centerId;

    mapsListSrv.waitForExistence();
    mapsListSrv.call(mapsListMsg);
    mapsList = mapsListMsg.response.chosenMaps;

    ros::ServiceClient changeMapFileSrv = nh.serviceClient<mpnet::npMapRequest>("/map/numpy");
    mpnet::npMapRequest changeMapFileMsg;

    uint8_t envIdx = 1;

    for (const std::string &map : mapsList) {
        ROS_INFO_STREAM("Map " << envIdx << "/" << (int)(mapsList.size()));
        if (envIdx <= numMaps) {
            if (!fs::is_directory(filepath + "/training_/env_" + map)) {
                fs::create_directory(filepath + "/training_/env_" + map);
            }
            if (!fs::is_directory(filepath + "/validation_/env_" + map)) {
                fs::create_directory(filepath + "/validation_/env_" + map);
            }
        } else {
            if (!fs::is_directory(filepath + "/validation_/env_" + map)) {
                fs::create_directory(filepath + "/validation_/env_" + map);
            }
        }
        // Carregar o mapa
        changeMapFileMsg.request.center_id = centerId;
        changeMapFileMsg.request.map_id = map;
        changeMapFileSrv.call(changeMapFileMsg);
        if (!changeMapFileMsg.response.result) {
            ROS_ERROR("Erro ao mudar o mapa");
        } else {
            boost::shared_ptr<const nav_msgs::OccupancyGrid> mapMsg =
                ros::topic::waitForMessage<nav_msgs::OccupancyGrid>("/map");
            boost::shared_ptr<const nav_msgs::MapMetaData> mapMetadataMsg =
                ros::topic::waitForMessage<nav_msgs::MapMetaData>("/map_metadata");

            ROS_INFO("Mapa carregado");
            sendAgentPosition(XYZCoords({0, 0, 0}));
            run(map, envIdx <= numMaps, changeMapFileMsg.response.centers);
            //			sleep(2);
        }
        envIdx++;
    }
}

void PathGenerator::run(std::string envIdx, bool training, std::vector<geometry_msgs::Point32> centers) {

    uint16_t originQtt;
    uint16_t goalQtt;
    if (training) {
        originQtt = numOrigins + numValidOriginsSeen;
        goalQtt = numGoals;
    } else {
        originQtt = numValidOriginsUnseen;
        goalQtt = numValidGoalsUnseen;
    }

    boost::shared_ptr<nav_msgs::OccupancyGrid const> occupancyMsg;
    while (!occupancyMsg) {
        occupancyMsg = ros::topic::waitForMessage<nav_msgs::OccupancyGrid>("/move_base/global_costmap/costmap", nh,
                                                                           ros::Duration(2.0));
        sendAgentPosition(XYZCoords());
    }

    occupGridMsg = *occupancyMsg;
    occupancyMap = occupGridMsg.data;

    ROS_INFO("Occupancy map len --> %ld", occupancyMap.size());

    std::vector<std::vector<int8_t>::iterator> validIndexes = getValid();

    bool validationResult;
    XYZCoords goal{};
    XYZCoords coord{};

    uint16_t countOrigin = 0;
    uint16_t numRepetitions;
    uint64_t chosenIndex;
    bool validOrigin;

    bool trainingBatch = true;
    for (int i = 0; i < originQtt; i++) {
        ROS_INFO_STREAM("Origin " << countOrigin << "/" << originQtt);
        chosenIndex = getRandomPoint(validIndexes);
        coord = mapToCoords(validIndexes, chosenIndex);
        numRepetitions = 0;
        validOrigin = false;

        if (i >= numOrigins && training) {

            ROS_WARN_COND(trainingBatch, "Validation Data");
            trainingBatch = false;
            goalQtt = numValidGoalsSeen;
        }

        for (uint16_t j = 0; j < goalQtt; j++) {
            do {
                sendAgentPosition(coord);
                goal = getRandomGoals(coord, validIndexes);
                auto path = generatePaths(coord, goal);
                validationResult = validatePath(path);
                if (validationResult) {
                    numRepetitions = 0;
                    validOrigin = true;
                    validationResult = savePath(path, envIdx, training && trainingBatch, centers);
                } else {
                    numRepetitions++;
                    if (numRepetitions > 10 && !validOrigin) {
                        break;
                    }
                }
            } while (!validationResult);
            ROS_INFO_STREAM_COND(j % 5 == 0, "Goal " << j << "/" << goalQtt);
            if (numRepetitions > 10) {
                break;
            }
        }
        if (numRepetitions > 10) {
            i--;
        }
        countOrigin++;
    }
}

bool PathGenerator::savePath(const nav_msgs::Path path, std::string envIdx, bool training,
                             std::vector<geometry_msgs::Point32> centers) {

    std::string centersFile = __FILE__;
    std::size_t cFileIdx = centersFile.find("mpr");
    centersFile =
        centersFile.substr(0, cFileIdx + 3) + "/obs/center" + std::to_string(centerId) + "/perm" + envIdx + ".npy";

    std::vector<float> pathData;
    uint16_t count = 0;
    for (auto pose = path.poses.begin(); pose != path.poses.end(); pose++) {
        pathData.push_back(pose->pose.position.x);
        pathData.push_back(pose->pose.position.y);
    }

    auto newPath = lvc(pathData, centers);

    if (newPath.size() <= 2) {
        return false;
    }

    std::string filepath = __FILE__;
    std::size_t strIdx = filepath.find("mpr");
    if (training) {
        filepath = filepath.substr(0, strIdx + 3) + "/paths/training_" + "/env_" + envIdx;
    } else {
        filepath = filepath.substr(0, strIdx + 3) + "/paths/validation_" + "/env_" + envIdx;
    }
    int16_t maxId = -1;
    for (const auto &entry : fs::directory_iterator(filepath)) {
        std::string fname = entry.path().filename();
        uint8_t underlineIdx = fname.find('_');
        fname = fname.substr(underlineIdx + 1, fname.find('.') - underlineIdx - 1);
        long int fnameId = std::strtol(fname.c_str(), nullptr, 0);
        if (fnameId > maxId) {
            maxId = fnameId;
        }
    }
    cnpy::npy_save(filepath + "/path_" + std::to_string(maxId + 1), newPath);
    return true;
}

std::vector<float> PathGenerator::lvc(std::vector<float> pathData, std::vector<geometry_msgs::Point32> centers) {

    std::vector<float> start(2);
    std::vector<float> end(2);

    for (size_t i = 0; i < pathData.size(); i += 2) {
        start[0] = pathData[i];
        start[1] = pathData[i + 1];
        for (size_t j = pathData.size() - 2; j > i + 2; j -= 2) {
            end[0] = pathData[j];
            end[1] = pathData[j + 1];
            if (steerTo(start, end, centers)) {
                std::vector<float> pc;

                for (int k = 0; k < i + 2; k += 2) {
                    pc.push_back(pathData[k]);
                    pc.push_back(pathData[k + 1]);
                }
                for (int k = j; k < pathData.size(); k += 2) {
                    pc.push_back(pathData[k]);
                    pc.push_back(pathData[k + 1]);
                }
                return lvc(pc, centers);
            }
        }
    }
    return pathData;
}

bool PathGenerator::steerTo(std::vector<float> start, std::vector<float> end,
                            std::vector<geometry_msgs::Point32> centers) {

    float resolution = 0.1;
    float dists[2] = {end[0] - start[0], end[1] - start[1]};
    float distTotal = sqrt(pow(dists[0], 2) + pow(dists[1], 2));
    float incrementTotal = distTotal / resolution;

    dists[0] = dists[0] / incrementTotal;
    dists[1] = dists[1] / incrementTotal;

    int numSegments = (int)incrementTotal;

    float currentState[2] = {start[0], start[1]};
    for (int i = 0; i < numSegments; i++) {
        for (auto center : centers) {
            if (abs(currentState[0] - center.x) < 2.5 and abs(currentState[1] - center.y) < 2.5) {
                return false;
            }
        }

        currentState[0] += dists[0];
        currentState[1] += dists[1];
    }

    return std::all_of(centers.begin(), centers.end(), [end](geometry_msgs::Point32 center) {
        return abs(end[0] - center.x) > 2.5 or abs(end[1] - center.y) > 2.5;
    });
}

void PathGenerator::save(const std::vector<nav_msgs::Path> &paths,
                         const std::vector<path_generator::PixelPath> &imgPaths, const nav_msgs::OccupancyGrid &map,
                         const mpnet::Obstacles &obstacles) {

    std::string filepath = __FILE__;
    std::size_t strId = filepath.find("mpr");
    filepath = filepath.substr(0, strId + 3) + "/bags/";
    uint16_t max_id = 0;

    // Get the ID of the next bag file (max id + 1)
    for (const auto &entry : fs::directory_iterator(filepath)) {
        std::string fname = entry.path().filename();
        uint8_t underlineId = fname.find('_');
        fname = fname.substr(underlineId + 1, fname.find('.') - underlineId - 1);
        long int fnameId = std::strtol(fname.c_str(), nullptr, 0);
        if (fnameId > max_id) {
            max_id = fnameId;
        }
    }
    filepath += "bag_" + std::to_string(max_id + 1) + ".bag";
    ROS_INFO("Salvando no arquivo %s", filepath.c_str());

    rosbag::Bag bag(filepath, rosbag::bagmode::Write);

    path_generator::Paths pathsMsg;
    pathsMsg.paths = paths;
    pathsMsg.pixelPaths = imgPaths;
    pathsMsg.map = map;
    pathsMsg.obstacles = obstacles;
    bag.write("/paths_generated", ros::Time::now(), pathsMsg);
}

std::vector<path_generator::PixelPath> PathGenerator::pathsToImg(const std::vector<nav_msgs::Path> &paths,
                                                                 nav_msgs::MapMetaData metadata) {

    std::vector<path_generator::PixelPath> imgPath;
    path_generator::PixelPath pixPath;

    float resolution = metadata.resolution;
    float origin_x = metadata.origin.position.x;
    float origin_y = metadata.origin.position.y;

    for (auto &path : paths) {
        for (auto &pose : path.poses) {
            path_generator::Pixel pixel;
            pixel.x = std::round((pose.pose.position.x - origin_x) / resolution);
            pixel.y = std::round((pose.pose.position.y - origin_y) / resolution);
            pixPath.pixelPath.push_back(pixel);
        }
        imgPath.push_back(pixPath);
    }
    return imgPath;
}

bool PathGenerator::validatePath(nav_msgs::Path &path) {

    if (path.poses.empty()) {
        return false;
    } else {
        return true;
    }
}

int main(int argc, char *argv[]) {

    ros::init(argc, argv, "generator");
    PathGenerator gen;
    ros::spin();
}
