from abc import ABC
from kerastuner import HyperParameters
from tensorflow.keras import Sequential, layers
from tensorflow.keras.initializers import LecunNormal
from tensorflow.keras.optimizers import Adam
import tensorflow as tf


class ContractiveAutoEncoder(Sequential, ABC):
    def __init__(self, hp: HyperParameters, input_shape=2800, *args, **kwargs):
        super().__init__(*args, **kwargs)

        dropout = hp.Boolean("dropout")
        dropout_value = hp.Float(
            "dropout_value", min_value=0.01, max_value=0.2, step=0.01, parent_name="dropout", parent_values=[True]
        )
        self.init_layers = []
        self.final_layers = []
        self.add(layers.InputLayer(input_shape))

        num_layers = hp.Int("num_layers", min_value=1, max_value=4)

        units = [
            hp.Int("units", min_value=512 - 60, max_value=512 + 60, step=12),
            hp.Int("units_1", min_value=200, max_value=256 + 64, step=12),
            hp.Int("units_2", min_value=128 - 60, max_value=128 + 60, step=12),
        ]
        for i in units[:num_layers]:
            self.init_layers.append(
                layers.Dense(
                    units=i, activation="selu", bias_initializer=LecunNormal(), kernel_initializer=LecunNormal()
                )
            )
            self.add(self.init_layers[-1])
            if dropout:
                self.add(layers.Dropout(dropout_value))

        self.encoder = layers.Dense(
            units=28,
            name="encoder",
            bias_initializer=LecunNormal(),
            kernel_initializer=LecunNormal(),
            activation="selu",
        )
        self.add(self.encoder)

        for i in units[:num_layers][::-1]:
            self.final_layers.append(
                layers.Dense(
                    units=i, activation="selu", bias_initializer=LecunNormal(), kernel_initializer=LecunNormal()
                )
            )
            self.add(self.final_layers[-1])
            if dropout:
                self.add(layers.Dropout(dropout_value))

        self.decoder = layers.Dense(
            units=input_shape, bias_initializer=LecunNormal(), kernel_initializer=LecunNormal(), activation="selu"
        )
        self.add(self.decoder)

        loss = self.contractive_loss

        self.lambd = hp.Choice("lambda", values=[1e-3, 1e-4, 1e-5])

        optimizer = hp.Choice("optimizer", values=["adam"])
        epsilon = hp.Choice("epsilon", values=[1e-7])

        lr = hp.Choice("learning_rate", values=[1e-4], parent_name="optimizer", parent_values=["adam"])
        optimizer_ = Adam

        self.compile(optimizer_(learning_rate=lr, epsilon=1e-7), loss=loss)
        self.code = None
        # self.summary()

    def contractive_loss(self, predicted, reference):

        mse_error = tf.reduce_mean(tf.square(reference - predicted), axis=1)

        weights = self.get_layer("encoder").weights[0]
        weights = tf.transpose(weights)
        h = self.code
        dh = h * (1 - h)
        print(dh.shape, weights.shape, mse_error.shape)
        contractive = self.lambd * tf.reduce_sum(dh ** 2 * tf.reduce_sum(weights ** 2, axis=1), axis=1)
        return mse_error + contractive

    def call(self, inputs, training=None, mask=None):
        x = inputs
        for layer in self.init_layers:
            x = layer(x)
        self.code = self.encoder(x)
        x = self.code
        for layer in self.final_layers:
            x = layer(x)
        x = self.decoder(x)
        return x


def build_model(hp: HyperParameters):
    network = ContractiveAutoEncoder(hp, input_shape=2800)
    return network