import numpy as np
import tensorflow as tf
import io
from tensorflow.python.lib.io import file_io


def load_tf_dataset(path, normalize):
    dataset = tf.data.Dataset.list_files(path)
    dataset = dataset.map(
        lambda x: tf.numpy_function(map_ds, [x, normalize], [tf.double, tf.double]), tf.data.experimental.AUTOTUNE
    )
    validation = dataset.take(int(55000 * 0.1))
    training = dataset.skip(int(55000 * 0.1))

    training = training.shuffle(1000).cache().batch(32).prefetch(tf.data.experimental.AUTOTUNE)
    validation = validation.shuffle(1000).cache().batch(32).prefetch(tf.data.experimental.AUTOTUNE)

    return training, validation


def map_ds(data, normalize):
    f = io.BytesIO(file_io.read_file_to_string(data, binary_mode=True))
    dataset = np.load(f, allow_pickle=True)
    if normalize:
        dataset = np.interp(dataset.reshape((-1,)), (-20, 20), (0, 1))
    return dataset, dataset


def split_dataset(images, fraction=0.25):
    choices = np.random.choice(len(images) - 1, int(len(images) * fraction), replace=False)
    validation = images[choices]
    training = np.delete(images, choices, axis=0)
    return training, validation
