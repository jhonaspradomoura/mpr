from setuptools import setup, find_packages

setup(
    name="trainer",
    version=0.1,
    install_requires=["tensorflow>2", "tensorflow-gpu>2", "numpy", "gcloud"],
    packages=find_packages(),
    include_package_data=True,
)
