import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, InputLayer, Dropout, PReLU
from tensorflow.keras.initializers import LecunNormal, Constant


class Pnet(Sequential):
    def __init__(self, activation, drpt_rate, last_dp, linear, *args, **kwargs):
        super().__init__(*args, **kwargs)

        units = [1280, 1024, 896, 768, 512, 384, 256, 256, 128, 64, 32]
        self.add(InputLayer(input_shape=(32,)))
        for unit in units:
            self.add(
                Dense(unit, activation=activation, kernel_initializer=LecunNormal(), bias_initializer=LecunNormal())
            )
            if not activation:
                self.add(PReLU(alpha_initializer=Constant(0.25)))

            if unit != 32:
                self.add(Dropout(rate=drpt_rate))
            elif last_dp:
                self.add(Dropout(rate=drpt_rate))

        last_activation = None if linear else activation
        self.add(Dense(2, activation=last_activation, kernel_initializer=LecunNormal(), bias_initializer=LecunNormal()))

    def call(self, inputs, *args, **kwargs):
        return super().call(inputs, *args, **kwargs)