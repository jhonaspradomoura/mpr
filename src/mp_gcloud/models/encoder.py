import tensorflow as tf
from tensorflow.keras.layers import Dense
from tensorflow.python.keras.engine.base_layer import Layer
from tensorflow.python.keras.engine.input_layer import InputLayer
from tensorflow.python.keras.initializers.initializers_v2 import Constant, LecunNormal
from tensorflow.python.keras.layers.advanced_activations import PReLU
from tensorflow.python.keras.layers.core import Dropout


class Encoder(Layer):
    def __init__(self, activation_, drpt, drpt_rate, units_, **kwargs):
        super().__init__(**kwargs)

        self.first_layer = InputLayer((2800,))

        self.hidden_layers = []
        self.prelu_layers = []
        self.dropout_layers = []
        for unit in units_:
            self.hidden_layers.append(
                Dense(unit, activation=activation_, kernel_initializer=LecunNormal(), bias_initializer=LecunNormal())
            )
            if not activation_:
                self.prelu_layers.append(PReLU(alpha_initializer=Constant(0.25)))
            if drpt:
                self.dropout_layers.append(Dropout(drpt_rate))

        self.classifier = Dense(28, bias_initializer=LecunNormal(), kernel_initializer=LecunNormal())

        self.code = None

    def call(self, inputs, **kwargs):
        x = self.first_layer(inputs)
        for n, hidden in enumerate(self.hidden_layers):
            x = hidden(x)
            if self.prelu_layers:
                x = self.prelu_layers[n](x)
            if self.dropout_layers:
                x = self.dropout_layers[n](x)
        self.code = self.classifier(x)
        return self.code


class Decoder(Layer):
    def __init__(self, activation_, drpt, drpt_rate, units_, **kwargs):
        super().__init__(**kwargs)
        self.hidden_layers = []
        self.prelu_layers = []
        self.dropout_layers = []
        for unit in units_[::-1]:
            self.hidden_layers.append(
                Dense(unit, activation=activation_, kernel_initializer=LecunNormal(), bias_initializer=LecunNormal())
            )
            if not activation_:
                self.prelu_layers.append(PReLU(alpha_initializer=Constant(0.25)))
            if drpt:
                self.dropout_layers.append(Dropout(drpt_rate))

        self.reconstruction = Dense(2800, bias_initializer=LecunNormal(), kernel_initializer=LecunNormal())

    def call(self, inputs, **kwargs):
        x = inputs
        for n, hidden in enumerate(self.hidden_layers):
            x = hidden(x)
            if self.prelu_layers:
                x = self.prelu_layers[n](x)
            if self.dropout_layers:
                x = self.dropout_layers[n](x)
        x = self.reconstruction(x)
        return x


class Enet(tf.keras.Sequential):
    # @tf.function
    def __init__(self, activation, drpt, drpt_rate, units, lambd):
        super(Enet, self).__init__()
        self.lambd = lambd
        self.code = None

        self.encoder = Encoder(activation, drpt, drpt_rate, units)
        self.add(self.encoder)

        self.decoder = Decoder(activation, drpt, drpt_rate, units)
        self.add(self.decoder)

    def call(self, inputs, **kwargs):
        self.code = self.encoder(inputs)
        x = self.decoder(self.code)
        return x

    def contractive_loss(self, predicted, reference):

        mse_error = tf.reduce_mean(tf.square(reference - predicted), axis=1)

        weights = self.encoder.classifier.weights[0]
        weights = tf.transpose(weights)
        h = self.code
        dh = h * (1 - h)
        contractive = self.lambd * tf.reduce_sum(dh ** 2 * tf.reduce_sum(weights ** 2, axis=1), axis=1)
        return mse_error + contractive