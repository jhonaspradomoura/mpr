import numpy as np
import logging
import json
import tensorflow as tf

# from loader.data_serializer import read_pnet
from utils.data_serializer import read_pnet
import argparse
from models.pnet import Pnet
from tensorflow.keras.callbacks import History, ReduceLROnPlateau, EarlyStopping
from tensorflow.keras.optimizers import Adam

# from gcloud import storage


project_path = "gs://mpnet"


class ValLossMetricCallback(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs=None):
        tf.summary.scalar("val_loss", logs["val_loss"], epoch)


def main(args):
    client = storage.Client(project="mpnet-311523")
    bucket = client.get_bucket("mpnet")

    configs = {
        "prelu": (None, 0.5, False, True),
        "prelu_tuner": (None, 0.5, False, True),
        "selu_1": ("selu", 0.2, False, False),
        "selu_2": ("selu", 0.2, True, False),
        "selu_3": ("selu", 0.2, False, True),
        "selu_4": ("selu", 0.2, True, True),
        "selu_1_tuner": ("selu", 0.2, False, False),
        "selu_2_tuner": ("selu", 0.2, True, False),
        "selu_3_tuner": ("selu", 0.2, False, True),
        "selu_4_tuner": ("selu", 0.2, True, True),
    }

    model_name = args.model if not "tuner" in args.ae else f"{args.model}_{args.ae.split('_')[0]}"
    config = configs[model_name]

    training = read_pnet(f"paths_training_{args.ae}", 100)
    validation = read_pnet(f"paths_validation_{args.ae}", 100)

    file_dict = {"loss": [], "epochs": []}
    for itt in range(args.num_it):
        history = History()
        blob = bucket.blob(f"performance_data/planner/{model_name}_{itt + args.sample_id}.json")
        blob.upload_from_string(data=json.dumps(file_dict), content_type="application/json")

        planner = Pnet(*config)
        optimizer = Adam(learning_rate=1e-4)

        planner.compile(optimizer, loss="mse")

        if args.reduce:
            planner.fit(
                training,
                batch_size=100,
                epochs=100,
                validation_data=validation,
                callbacks=[
                    ReduceLROnPlateau(factor=0.2, patience=6, cooldown=2, min_delta=1e-4, verbose=1),
                    EarlyStopping(min_delta=1e-4, patience=20, verbose=1),
                    history,
                ],
            )
        else:
            planner.fit(
                training,
                batch_size=100,
                epochs=100,
                validation_data=validation,
                callbacks=[
                    EarlyStopping(min_delta=1e-4, patience=20, verbose=1),
                    history,
                ],
            )

        if not storage.Blob(bucket=bucket, name=f"models/ae/{model_name}/").exists():
            planner.save(f"{project_path}/models/ae/{model_name}")
        val_loss = np.array(history.history["val_loss"])
        min_loss_idx = np.where(np.min(val_loss) == val_loss)[0][0]
        file_dict["loss"] = val_loss[min_loss_idx]
        file_dict["epochs"] = history.epoch[min_loss_idx]
        logging.warning(f"Val loss: {file_dict['loss']} \t Epochs: {file_dict['epochs']}")
        blob.upload_from_string(data=json.dumps(file_dict))


def train_prelu():

    training = read_pnet("paths_training_prelu", 100)
    validation = read_pnet("paths_validation_prelu", 100)
    planner = Pnet(None, 0.5, False, True)
    planner.compile(Adam(1e-4), loss="mse")
    planner.fit(
        training,
        batch_size=100,
        epochs=100,
        validation_data=validation,
        callbacks=[
            ReduceLROnPlateau(factor=0.2, patience=6, cooldown=2, min_delta=1e-4, verbose=1),
            EarlyStopping(min_delta=1e-4, patience=20, verbose=1),
        ],
    )
    planner.save("/home/dev/jhonas/mpr/models/planner/prelu")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--job-dir", type=str, help="Job dir", default=".", required=False)
    parser.add_argument("--model", default="prelu", type=str, help="Model to be trained")
    parser.add_argument("--ae", default="prelu", type=str, help="ENet model to use")
    parser.add_argument("--num_it", default=40, type=int, help="Number of samples from each model")
    parser.add_argument("--sample_id", default=1, type=int, help="Machine ID")
    parser.add_argument("--reduce", const=True, nargs="?", type=bool, help="Reduce LR on plateau")

    args = parser.parse_args()

    # main(args)
    train_prelu()