import logging
import tensorflow as tf
from tensorflow.keras.callbacks import History, EarlyStopping, ReduceLROnPlateau
from tensorflow.keras.optimizers import Adam
import numpy as np
import argparse
import json
from models.encoder import Enet
from gcloud import storage

from tensorflow.python.keras.callbacks import ModelCheckpoint
from loader.data_serializer import read_cae

project_path = "gs://mpnet"


# def contractive_loss(model, predicted, reference):
#     mse_error = tf.reduce_mean(tf.square(reference - predicted), axis=1)

#     weights = model.encoder.classifier.weights[0]
#     weights = tf.transpose(weights)
#     h = model.code
#     dh = h * (1 - h)
#     contractive = model.lambd * tf.reduce_sum(dh ** 2 * tf.reduce_sum(weights ** 2, axis=1), axis=1)
#     return mse_error + contractive


class ValLossMetricCallback(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs=None):
        tf.summary.scalar("val_loss", logs["val_loss"], epoch)


def main(args):
    client = storage.Client(project="mpnet-311523")
    bucket = client.get_bucket("mpnet")
    model = args.model

    batch_size = 64
    training, validation = read_cae(batch_size)

    models_config = {
        "prelu": (None, False, None, [512, 256, 128], 1e-5),
        "tuner_1": ("selu", False, None, [572, 236, 128], 1e-5),
        "tuner_2": ("selu", True, 0.14, [488, 260, 164], 1e-5),
        "tuner_3": ("selu", True, 0.02, [560, 320, 164], 1e-5),
        "tuner_4": ("selu", True, 0.12, [488, 224, 116], 1e-5),
    }
    config = models_config[model]
    file_dict = {"loss": [], "epochs": []}

    for itt in range(args.num_it):
        history = History()
        blob = bucket.blob(f"performance_data/ae/{model}_{itt + args.sample_id}.json")
        blob.upload_from_string(data=json.dumps(file_dict), content_type="application/json")
        cae = Enet(*config)

        optimizer = Adam(learning_rate=1e-4)
        cae.compile(optimizer, loss=cae.contractive_loss)
        if not args.reduce:
            cae.fit(
                training,
                epochs=100,
                batch_size=batch_size,
                validation_data=validation,
                callbacks=[EarlyStopping(min_delta=2e-4, patience=20, verbose=1), history, ValLossMetricCallback()],
            )
        else:
            cae.fit(
                training,
                epochs=100,
                batch_size=batch_size,
                validation_data=validation,
                callbacks=[
                    EarlyStopping(min_delta=2e-4, patience=20, verbose=1),
                    history,
                    ReduceLROnPlateau(factor=0.2, patience=6, cooldown=2, min_delta=1e-4, verbose=1),
                    ValLossMetricCallback(),
                ],
            )

        if not storage.Blob(bucket=bucket, name=f"models/ae/{model}/").exists():
            cae.save(f"{project_path}/models/ae/{model}")
        val_loss = np.array(history.history["val_loss"])
        min_loss_idx = np.where(np.min(val_loss) == val_loss)[0][0]
        file_dict["loss"] = val_loss[min_loss_idx]
        file_dict["epochs"] = history.epoch[min_loss_idx]
        logging.warning(f"Val loss: {file_dict['loss']} \t Epochs: {file_dict['epochs']}")
        blob.upload_from_string(data=json.dumps(file_dict))




if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--job-dir", type=str, help="Job dir", default=".", required=False)
    parser.add_argument("--model", default="prelu", type=str, help="Model to be trained")
    parser.add_argument("--num_it", default=40, type=int, help="Number of samples from each model")
    parser.add_argument("--sample_id", default=1, type=int, help="Machine ID")
    parser.add_argument("--reduce", const=True, nargs="?", type=bool, help="Reduce LR on plateau")

    args = parser.parse_args()

    main(args)