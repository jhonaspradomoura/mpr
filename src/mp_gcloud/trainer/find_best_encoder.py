#!/usr/bin/env python
from abc import ABC

import tensorflow as tf
from tensorflow.python.keras.callbacks import ReduceLROnPlateau, TensorBoard

from loader import data_serializer

from tensorflow.keras import Sequential, layers
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.initializers import LecunNormal
import argparse

project_path = "gs://mpnet"
resolver = tf.distribute.cluster_resolver.TPUClusterResolver(tpu="")
tf.config.experimental_connect_to_cluster(resolver)
# This is the TPU initialization code that has to be at the beginning.
tf.tpu.experimental.initialize_tpu_system(resolver)
print("All devices: ", tf.config.list_logical_devices("TPU"))


class ValLossMetricCallback(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs=None):
        tf.summary.scalar("val_loss", logs["val_loss"], epoch)


class ContractiveAutoEncoder(Sequential, ABC):
    def __init__(self, args, input_shape=2800, *pargs, **kwargs):
        super().__init__(*pargs, **kwargs)

        dropout = args.dropout
        dropout_value = args.dropout_value

        self.init_layers = []
        self.final_layers = []
        self.add(layers.InputLayer(input_shape))

        units = [args.units, args.units_2, args.units_3]
        for i in units:
            self.init_layers.append(
                layers.Dense(
                    units=i, activation="selu", bias_initializer=LecunNormal(), kernel_initializer=LecunNormal()
                )
            )
            self.add(self.init_layers[-1])
            if dropout:
                self.add(layers.Dropout(dropout_value))

        self.encoder = layers.Dense(
            units=28,
            name="encoder",
            bias_initializer=LecunNormal(),
            kernel_initializer=LecunNormal(),
            activation="selu",
        )
        self.add(self.encoder)

        for i in units[::-1]:
            self.final_layers.append(
                layers.Dense(
                    units=i, activation="selu", bias_initializer=LecunNormal(), kernel_initializer=LecunNormal()
                )
            )
            self.add(self.final_layers[-1])
            if dropout:
                self.add(layers.Dropout(dropout_value))

        self.decoder = layers.Dense(
            units=input_shape, bias_initializer=LecunNormal(), kernel_initializer=LecunNormal(), activation="selu"
        )
        self.add(self.decoder)

        self.lambd = args.lambd * 1e-5

        self.code = None
        self.summary()

    def contractive_loss(self, predicted, reference):

        mse_error = tf.reduce_mean(tf.square(reference - predicted), axis=1)

        weights = self.get_layer("encoder").weights[0]
        weights = tf.transpose(weights)
        h = self.code
        dh = h * (1 - h)
        contractive = self.lambd * tf.reduce_sum(dh ** 2 * tf.reduce_sum(weights ** 2, axis=1), axis=1)
        return mse_error + contractive

    def call(self, inputs, training=None, mask=None):
        x = inputs
        for layer in self.init_layers:
            x = layer(x)
        self.code = self.encoder(x)
        x = self.code
        for layer in self.final_layers:
            x = layer(x)
        x = self.decoder(x)
        return x


def train(args, training_ds, validation_ds):
    global strategy
    with strategy.scope():
        cae = ContractiveAutoEncoder(args, input_shape=2800)

        cae.compile(
            Adam(learning_rate=1e-4, epsilon=1e-7),
            loss=cae.contractive_loss,
        )
    cae.fit(
        training_ds,
        validation_data=validation_ds,
        batch_size=100,
        callbacks=[
            TensorBoard(f"{project_path}/logs/tuner/", histogram_freq=1),
            ReduceLROnPlateau("val_loss", patience=10, cooldown=5),
            ValLossMetricCallback(),
        ],
        epochs=25,
        verbose=1,
    )

    max_id = None
    try:
        cae_names = tf.data.Dataset.list_files(f"{project_path}/models/ae/cae*")
        cae_names = list(cae_names.as_numpy_iterator())
        max_id = [int(name.split("_")[-1].split(".")[0]) for name in cae_names]
        max_id = max(max_id)
    except Exception:
        max_id = 0
    cae.save(f"{project_path}/models/ae/cae_{max_id + 1}")
    print(f"Model exported to: {project_path}/models/ae/cae_{max_id + 1}")


if __name__ == "__main__":
    input_shape = (1400, 2)

    parser = argparse.ArgumentParser()
    parser.add_argument("--job-dir", type=str, help="Job dir", default=".", required=False)
    parser.add_argument("--id", type=str, help="ID of center data to load from obs folder", default=3, required=False)
    parser.add_argument("--dropout", type=bool, default=False)
    parser.add_argument("--dropout_value", type=float, default=0.01)
    parser.add_argument("--units", type=int, default=452)
    parser.add_argument("--units_2", type=int, default=200)
    parser.add_argument("--units_3", type=int, default=68)
    parser.add_argument("--lambd", type=float, default=1e-3)

    args = parser.parse_args()

    training, validation = data_serializer.read_cae()

    strategy = tf.distribute.TPUStrategy(resolver)

    train(args, training, validation)
