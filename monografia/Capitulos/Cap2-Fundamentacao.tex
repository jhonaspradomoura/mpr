\chapter{Fundamentação Teórica}\label{chap:fundamentação-teórica}

Este capítulo se dispõe a descrever conceitos teóricos que são necessários para a realização deste projeto.
Para que seja definida uma base de referência para o problema ao qual este trabalho busca uma solução, os conceitos de agentes e ambiente de tarefas são definidos.
Além disso, explica-se sobre a solução utilizada neste problema e seus diferentes métodos de uso.
A fim de detalhar sua implementação também é apresentada o modelo de rede neural que esta solução utiliza para reduzir a dimensionalidade dos dados de entrada.

\section{Agentes}\label{sec:agentes}

Um agente é tudo o que pode ser considerado capaz de perceber seu ambiente por meio de sensores e de agir sobre esse ambiente por intermédio de atuadores~\cite{RusselAI2009}, conforme ilustrado pela Figura~\ref{fig:agentes}.
Para um agente robótico, são exemplos de sensores: câmeras, giroscópios e acelerômetros.
Motores são exemplos de atuadores.

O termo percepção, definido por~\cite{RusselAI2009}, é usado quando se referencia às entradas perceptivas do agente em um dado instante.
Já a sequência de percepções do agente é a história completa de tudo o que o agente já percebeu.
A maneira como o agente age no ambiente pode depender de toda sequência de percepções recebidas até o instante corrente.
O comportamento do agente é descrito pela função do agente que mapeia qualquer sequência de percepções específica para uma ação.

\begin{figure}[H]
    \includegraphics[width=0.7\textwidth, height=60mm]{Figuras/agentes.png}
    \centering
    \caption{Relação entre agente e ambiente~\cite{RusselAI2009}.}\label{fig:agentes}
\end{figure}


No contexto do presente trabalho, englobando a área de robótica, o modelo utilizado para estruturar o comportamento do agente é o modelo referencial funcional~\cite{Putz1993}.
Este modelo, ilustrado na Figura~\ref{fig:camadas_nav}, define uma representação unificada de funções essenciais para o controle do agente, dividida nas seguintes camadas: habilidades, tarefas e missão.
Cada camada soluciona problemas relacionados a um nível de abstração.
Por exemplo, considerando a Figura~\ref{fig:camadas_nav} e a área da robótica, a camada de habilidades soluciona problemas relacionados ao nível de hardware, chamado de baixo nível.
Enquanto que a camada de missão soluciona problemas que abrangem as tarefas a serem realizadas pelo agente, sendo que cada tarefa é composta por um conjunto de habilidades.
As camadas são descritas a seguir:

\begin{itemize}
    \item \textbf{Camada de missão}: define quais tarefas devem ser executadas e sua ordem de execução a fim de solucionar o problema.
          Conforme descrito em~\cite{hurmuzlu2017mechanical}, missões são instruções orientadas ao processo que não expecificam explicitamente a maneira que uma tarefa deve ser executada, isso fica a cargo da camada de tarefas;
    \item \textbf{Camada de tarefas}: detalha as ações que devem ser executadas para cada tarefa definida na camada de missão.
          Deste modo, cada tarefa possui um conjunto de ações.
          É nessa camada que são definidos cteu no fim de
          omportamentos para problemas isolados, como abrir uma porta, navegar pelo ambiente ou se recuperar de uma queda;
    \item \textbf{Camada de habilidades}: transforma o conjunto de ações, definido na camada de tarefas, em comandos de controle dos atuadores.
          Na área de robótica, os atuadores são compostos pelo hardware do agente.
          O conjunto de habilidades disponíveis para o agente equivale ao conjunto de ações que ele pode efetuar.
          Algumas das habilidades comumente presentes são a de se locomover pelo ambiente, por exemplo mexer para direita, esquerda, cima e baixo, e de manipular objetos, como levantar um bloco usando um garra;
\end{itemize}

\begin{figure}[H]
    \includegraphics[width=0.4\textwidth, height=65mm]{Figuras/camadas_nav.jpg}
    \centering
    \caption{Estrutura do comportamento do agente utilizando o modelo referencial funcional.}\label{fig:camadas_nav}
\end{figure}

Este trabalho foca em solucionar um problema de planejamento de trajetórias, que é uma funcionalidade modelada pela camada de tarefas.
Por isso, não é necessário um detalhamento sobre como os atuadores do agente executam as instruções determinadas pela camada de tarefas.
Mesmo que o modo que os atuadores realizam as instruções não seja relevante para o problema, as ações que o agente é capaz de realizar são.

Além do seu comportamento, outro ponto que deve ser bem definido são os ambientes de tarefas, que~\cite{RusselAI2009} define como os problemas para os quais os agentes são as soluções.
Com o intuito de modelar agentes eficientes, é essencial que seja feita uma descrição precisa do ambiente.


O ambiente de tarefas é definido por~\cite{RusselAI2009} como os problemas para os quais o agente é a solução.
A fim de modelar agentes eficientes, é essencial que seja feita uma descrição precisa do ambiente.
Por isso~\cite{RusselAI2009} define as seguintes propriedades para se classificar um ambiete:

\begin{itemize}
    \item \textbf{Completamente observável versus parcialmente observável}: um ambiente completamente observável é aquele em que os sensores de um agente permite acesso preciso a todas informações do estado do ambiente que sejam relevantes a tomada de decisão a cada instante. Já um parcialmente observável é todo aquele em que isso não aconteça.
    \item \textbf{Agente único versus multiagente}: um ambiente de agente único é aquele em que não existem outros agentes interagindo nele. Intuitivamente, um ambiente de multiagentes é aquele em que existe mais de um agente interagindo nele. Essa interação pode ser tanto cooperativa quanto competitiva. Essa propriedade é muito importante, visto que em ambientes de multiagentes novos problemas surgem como a comunicação entre os agentes que estejam cooperando.
    \item \textbf{Determinístico versus estocástico}: quando o próximo estado do ambiente é completamente determinado pelo estado atual e pela ação executada pelo agente, esse ambiente é classificado como determinístico. Caso contrário, ele é estocástico.
    \item \textbf{Episódico versus sequencial}: quando a experiência do agente é dividida em episódios atômicos, o ambiente é classificado como episódico. Cada episódio consiste percepção do agente seguida por uma ação. Além disso, o episódio seguinte não pode depender de episódios anteriores, dependendo somente dele mesmo. Em ambientes sequenciais, a decisão atual pode afetar todas as decisões futuras.
    \item \textbf{Estático versus dinâmico}: um ambiente dinâmico é aquele que pode sofrer alterações enquanto o agente está deliberando. Caso contrário, ele é um ambiente estático.
    \item \textbf{Discreto versus contínuo}: a distinção entre discreto e contínuo aplica-se ao estado do ambiente em relação ao modo como o tempo é tratado, e ainda às percepções e ações do agente.
    \item \textbf{Conhecido versus desconhecido}: esta distinção se refere ao estado de conhecimento do agente em relação as leis que regem o ambiente. Em um ambiente conhecido, são fornecidas todas as saídas para todas as ações. No entanto, em um ambiente desconhecido, o agente deve aprender quais as consequências de suas ações, para que assim consiga tomar boas decisões.
\end{itemize}


\section{Ambiente de Tarefas}\label{rev:sec:ambiente}

Todos os ambientes de tarefas utilizados no presente trabalho possuem as mesmas características.
São elas:

\begin{itemize}
    \item \textbf{Completamente observável}: o ambiente é mapeado previamente antes de ser processado e representado pela posição de cada obstáculo.
    \item \textbf{Agente único}: somente um agente presente no ambiente. Este agente é capaz de se locomover através de um ambiente 2D, ou seja, pode se mover para esquerda, direita, frente e trás.
    \item \textbf{Determinístico}: todas as ações tomadas pelo agente possuem reações que são conhecidas, ou seja, o próximo estado do ambiente é determinado pelo estado atual e pela ação executada pelo agente.
    \item \textbf{Sequencial}: o ambiente é sequencial, já que a decisões atuais podem afetar as decisões futuras.
    \item \textbf{Estático}: o ambiente é estático, desta maneira não pode sofrer alterações.
    \item \textbf{Discreto}: o estado do agente é considerado discreto, já que ele se altera dentro de um espaço finito de possibilidades, o tempo também é considerado discreto, por ser contabilizado a partir do número de iterações.
    \item \textbf{Conhecido}: conhece-se todas as reações do ambiente para cada ação possível do agente.
\end{itemize}



\section{Autoencoders}\label{sec:autoencoders}

O \gls{ae} é definido por~\cite{Bank2020} como um tipo específico de rede neural, a qual é
principalmente designada para codificar a entrada em uma representação comprimida e significativa, e então reconstruí-la fazendo com que essa versão seja similar aos dados originais, conforme ilustrado na Figura~\ref{autoencoder_repr:1}.

Formalmente, um autoencoder convencional é composto por duas camadas, sendo elas um codificador $h = f(x)$ e um decodificador $r = g(h)$.
Seu objetivo é encontrar uma representação codificada para cada amostra minimizando a função de perda.
No presente trabalho são utilizadas três funções de perda. São elas: ~\gls{mse}, \textit{Binary Cross-Entropy} e o~\gls{mse} com contração.

Formalmente, o~\gls{mse} é descrito por:
\begin{equation}
    \min_{W,U} \frac{1}{n} \sum_{i=1}^{n}{\norm{g(f(x_i)) - x_i}_2^2}
    \label{eq:mse}
\end{equation}
no qual $x_i$ representa a i-ésima amostra.
A \textit{Binary Cross-Entropy} é caracterizada pela junção da função de ativação sigmoide com função de perda \textit{Cross-Entropy}, e é descrita pela equação a seguir.
\begin{equation}
    -t_i\log{(f(s_i))} - (1 - t_i)\log{(1-f(s_i))}
    \label{eq:bin_cross}
\end{equation}
onde $t_i$ representa o dado de referência utilizado, $s_i$ é o resultado obtido pela rede neural e $f$ se refere a função sigmoide descrita pela Equação~\eqref{eq:sigmoid}.
\begin{equation}
    f(s_i) = \frac{1}{1 + \exp^{-s_i}}
    \label{eq:sigmoid}
\end{equation}
A função de perda~\gls{mse} com contração é explicada na seção~\ref{subsec:contractive-autoencoder} deste trabalho.

Para um autoencoder totalmente conectado,
\begin{equation}
    \begin{split}
        f(x) &= \sigma(Wx) \equiv h \\
        g(h) &= \sigma(Uh)
        \label{eq:funcs_ae}
    \end{split}
\end{equation}
onde $x$ representa o vetor dos dados de entrada, $h$ representa a codificação das amostras, e $\sigma$ é a função de ativação.
Foi omitido o viés por conveniência na descrição.
Após o treinamento, a codificação $h$ serve como a representação das amostras de entrada.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.6]{Figuras/Autoencoder.png}
    \caption{Exemplo da operação realizada por um autoencoder}
    \label{autoencoder_repr:1}
\end{figure}

Existem similaridades com a~\gls{rbm}, possuindo apenas algumas diferenças.
O objetivo de um~\gls{ae} é de minimizar o erro de reconstrução, encontrando uma representação compacta eficiente dos dados de entrada.
Já no caso de uma~\gls{rbm}, seu objetivo é quantificar a intensidade de conexão entre dois conjuntos de variáveis, as visíveis e as escondidas.
Outro fator que os difere é a existência de dois viéses nas~\gls{rbm}s, o viés da camada escondida, responsável por produzir ativações no avanço da rede, e o da camada visível, que auxilia no aprendizado das reconstruções no retorno da rede.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.55]{Figuras/rbm_ae.png}
    \caption{Exemplo da representação de uma~\gls{rbm} e um~\gls{ae}.}\label{fig:rbm_ae}
\end{figure}

\gls{ae}s são principalmente usados para reduzir a dimensionalidade dos dados, onde somente as características mais
relevantes são mantidas.
Também é utilizado com a finalidade de realizar modelagem generativa e como uma codificação eficiente de dados.
O \gls{ae} tem se mostrado eficiente no aprendizado de representações de características em diversas áreas, como
pré-treinamento não supervisionado de redes neurais~\cite{Sagheer2019}, reconhecimento de imagens~\cite{Peng2017},
visão computacional~\cite{Dehghan2014}, e sistemas de recomendação~\cite{Zhang2020}.

Na fase de treinamento de um \gls{ae}, existe a possibilidade da camada codificada resultar em uma função
identidade, gerando um \textit{overfitting} (quando o modelo treinado atinge um ótimo desempenho sobre os dados de treinamento, porém não generaliza bem para novos dados).
A maneira mais comumente usada de evitar esse problema é fazer com que a dimensão da camada de codificação seja menor do que a dos dados de entrada, se tornando assim uma espécie de gargalo por onde os dados têm que fluir.
Essa abordagem também resulta na redução da dimensionalidade dos dados.
Em casos em que a camada escondida tenha uma dimensão igual ou maior do que a entrada, existe o risco de que o codificador aprenda a função identidade, o que não é interessante pois a estrutura aprenderia a isolar cada dado, não trazendo nenhuma informação relevante.

Um dos principais dilemas que envolvem os \gls{ae}s é o dilema de viés-variância.
Por um lado, deseja-se que a arquitetura seja capaz de reconstruir os dados de entrada eficientemente.
Isto é, minimizar o erro entre a reconstrução e a entrada, tornando-as similares.
Por outro lado, também deseja-se que a representação codificada generalize para uma representação significativa.

Na literatura, existem diversas arquiteturas distintas de \gls{ae} que lidam com os desafios mencionados anteriormente.
Dentre elas, as duas relevantes para o presente trabalho são: \textit{Contractive Autoencoder} e o \textit{Convolutional Autoencoder}.

\subsection{Contractive Autoencoder}\label{subsec:contractive-autoencoder}
~\nocite{Goodfellow2016}
Contractive Autoencoders~\cite{Rifai2011} têm enfase em diminuir a sensitividade da extração de características à
pequenas pertubações, ou seja, tornar a representação codificada robusta a pequenas mudanças ou variações em torno dos exemplos de treinamento.
Isso é feito forçando o codificador a penalizar mudanças na entrada que não sejam relevantes para a reconstrução efetuada pelo decodificador.
Essa penalização é imposta na Jacobiana da rede neural, sendo aplicada no ajuste de pesos da rede.
A sensitividade é medida utilizando a norma de Frobenius da Jacobiana $J_f(x)$, sendo calculada na camada codificadora.
Formalmente, se a entrada $x\;\epsilon\;\mathbb{R}^{d_x}$ é mapeada pela função $f$ para a representação escondida $h \: \epsilon \:
    \mathbb{R}^{d_h}$, o termo da regularização da sensitividade é dado pela soma do quadrado de todas as derivadas parciais das características extraídas:
\begin{equation}
    \norm{J_f(x)}_F^2 = \sum_{ij}\left(\frac{\partial h_j(x)}{\partial x_i}\right)^2
    \label{eq:cae_jac}
\end{equation}

Mesmo que isoladamente o Jacobiano possa encorajar o mapeamento para uma representação constante menos significativa, isso é contrabalanceado na fase de treinamento de um \gls{ae} pela necessidade que a representação aprendida tem de permitir uma boa reconstrução da entrada.

O Contractive Autoencoder adiciona o termo da penalização da sensitividade~\eqref{eq:cae_jac} à função de perda~\eqref{eq:mse}, que se transforma em:
\begin{equation}
    \min_{W,U} \frac{1}{n} \sum_{i=1}^{n}{\norm{g(f(x_i)) - x_i}_2^2} + \lambda\norm{J_A(x)}_2^2
    \label{eq:op_cae}
\end{equation}
onde $\lambda \norm{J_a(x)}_2^2$ se refere a fator de regularização, na qual $\lambda$ é uma taxa de ajuste para a regularização, tendo valores normalmente na faixa de $10^{-2}-10^{-4}$.

Os fatores de reconstrução (descrito pela equação~\eqref{eq:mse}) e de regularização puxam o resultado para direções
opostas.
Ao minimizar a norma Jacobiana quadrática, todas as representações codificadas da entrada tendem a ser mais similares entre si, e assim fazendo a reconstrução mais difícil, já que isso reduz as diferenças entre as representações.
A ideia principal é que as variações nas representações codificadas que não sejam importantes para as reconstruções poderiam ser reduzidas pelo fator de regularização, enquanto variações importante seriam mantidas por ter um impacto maior no erro de reconstrução.

\subsection{Convolutional Autoencoder}\label{subsec:convolutional-autoencoder}
~\cite{Masci2011} explica que o Convolutional Autoencoder, ilustrado pela Figura~\ref{convolutional_repr:1}, se
difere de um \gls{ae} convencional pelo fato de seu vetor de pesos ser compartilhado por todas as regiões da amostra, o que resulta na preservação da estrutura espacial da imagem.
Essa preservação é realizada pela operação de convolução, na qual cada região da amostra é convoluída com uma matriz de pesos, denominada filtro.
Para uma entrada $x$, a codificação da k-ésima camada é dada pela seguinte equação
\begin{equation}
    h^k = \sigma(x * W^k + b^k)
    \label{eq:conv_activ_func}
\end{equation}
onde $b$ representa o viés, mantido por todo mapeamento, $\sigma$ corresponde a função de ativação, W representa o vetor de pesos da camada $k$, e o símbolo ``$*$'' representa a operação de convolução.
É utilizado somente um viés por representação para que cada filtro se especialize na amostra inteira, uma vez que a adição de um viés por pixel resultaria em uma grande quantidade de termos a serem calculados por camada, dificultando a convergência da rede.
A reconstrução é obtida usando a equação a seguir
\begin{equation}
    g(h) = \sigma\bigl(\sum_{k\epsilon H}{h^k * \bar{W}^k + c}\bigr)
    \label{eq:conv_reconst}
\end{equation}
onde $H$ representa o grupo de características codificadas, $\bar{W}$ corresponde a operação de transposição das dimensões e do vetor de pesos da camada.
De modo análogo há somente um viés $c$ por representação.

\begin{figure}[H]
    \centering
    \includegraphics[width=.9\textwidth, height=45mm]{Figuras/convae.png}
    \caption{Representação de um Convolutional Autoencoder.}
    \label{convolutional_repr:1}
\end{figure}

A estrutura de um Convolutional Autoencoder pode possuir camadas de Pooling, onde ocorre a aproximação de dados de regiões de tamanho determinado, assim ocorre a redução da dimensionalidade dos dados, servindo o modo de funcionamento de um autoencoder.
Essa redução também pode ser realizada com um parâmetro chamado stride.
Esse parâmetro controla quais dados serão usados na operação de convolução com o filtro.
Quando o valor é diferente de um, o filtro ignora determinados pixels do mapa de características, reduzindo a sua dimensionalidade.

A abordagem apresentada em~\cite{Guo2017} propõe a redução significativa da dimensão da camada codificada para um
tamanho $n$, sendo $n$ o número de grupos nos quais se deseja classificar a rede, essa proposta foi denominada Deep Clustering.
O objetivo é forçar a rede neural a reduzir a dimensão da codificação o máximo possível, agrupando os dados em $n$ grupos.
Tal abordagem, ilustrada na Figura~\ref{convolutional_repr:2}, se mostrou eficiente para desafios de clusterização (agrupamento).
Ela possui a mesma estrutura de um Convolutional Autoencoder, com exceção das camadas que precedem e sucedem a camada codificada.
A camada que precede corresponde a uma camada de achatamento, com o objetivo de tornar os dados unidimensionais.
A camada que sucede tem o corresponde a uma camada totalmente conectada, que tem como objetivo retornar os dados a sua dimensão anterior ao achatamento.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.85\textwidth, height=50mm]{Figuras/deep_ae_clust.png}
    \caption{Exemplo da estrutura proposta por~\cite{Guo2017}.
        O termo $h$ refere-se à camada codificada.
        O termo $Flatten$ refere-se a camada de achatamento.
        O termo $FC$ se refere a camada totalmente conectada.
        As camadas do decodificador são chamadas de camadas convolucionais transpostas ou de camadas deconvolucionais. }
    \label{convolutional_repr:2}
\end{figure}

O ponto chave desta proposta é a limitação agressiva da dimensão da camada de codificação.
Aprender uma representação incompleta força o autoencoder a captura as características mais relevantes dos dados.

\section{\glsentryfull{mp}}\label{sec:motion-planing-network}

A \gls{mp} é definida por~\cite{Qureshi2019} como um algoritmo de planejamento iterativo bi-direcional baseado em uma rede neural profunda.
É composta por dois módulos: módulo de codificação e o módulo de planejamento.

O \gls{enet} é responsável por transformar as informações de obstáculos $X_{obs}$ presentes no ambiente $X$ em uma representação codificada $Z$.
É composto por um~\gls{ae} que codifica os dados provindos dos sensores, efetivamente reduzindo sua dimensionalidade.
Existem diversas implementações de~\gls{ae}s, como Contractive Autoencoders~\cite{Rifai2011}, Variational
Autoencoders~\cite{Kingma2013} e Convolutional Autoencoders~\cite{Masci2011}.
A implementação usada na~\gls{mp} foi o Contractive Autoencoder, por ser capaz de aprender campos de características robustos, o que é desejado em problemas de planejamento e controle, e por possuir uma melhor performance do que outras implementações.

O~\gls{pnet} é uma rede neural profunda estocástica feed-forward com parâmetros $\theta^{p}$.
Os dados da trajetórias são uma lista de pontos $\sigma = \{c_0, c_1, ... , c_T\}$, que conectam o ponto de início ao ponto objetivo da trajetória de modo que não existam obstáculos em nenhum ponto da lista.
Para treinar a~\gls{pnet} utiliza-se a estratégia de prever o próximo passo.
Desta maneira, a~\gls{mp} utiliza a nuvem de pontos dos obstáculos codificada $Z$, o estado atual do agente $c_t$ e o estado objetivo $c_T$ como entrada da rede, e retorna como saída o próximo estado $\hat{c}_{t+1}$ em direção ao estado objetivo.
Utiliza-se a função de perda do erro quadrático médio~\gls{mse} entre o ponto previsto $\hat{c}_{t+1}$ e o ponto referência $c_{t+1}$.
A equação~\ref{eq:loss_pnet} descreve essa função de perda.
\begin{equation}
    l_{Pnet}(\theta) = \frac{1}{N_p}\sum_{j}^{\hat{N}}\sum_{i}^{T-1}\norm{\hat{c}_{j, i+1} - {c_{j, i+1}}}^2,
    \label{eq:loss_pnet}
\end{equation}
onde $T$ é o comprimento da trajetória, $\hat{N}~\epsilon~\mathbb{N}$ é o número total de trajetórias, e $N_p = \hat{N} \times T$.

A \gls{mp} é capaz de gerar tanto trajetórias livres de colisão quanto amostras informadas para planejadores baseados em amostras.

Sua estratégia de planejamento de trajetórias tem as seguintes características:

\begin{itemize}
    \item \textbf{Planejamento Bidirecional}: o modelo pode tanto planejar a frente, indo do ponto inicial ao objetivo, quanto o contrário, indo do objetivo ao ponto inicial, até que ambos os caminhos planejados se encontrem.
    \item \textbf{Planejamento Recursivo de Dividir e Conquistar}: o planejador é recursivo e soluciona dado problema de planejamento de trajetória através de uma estratégia de dividir e conquistar.
          Começando com o planejador global que resulta em estados críticos, sem colisões que são vitais para gerar uma trajetória factível.
          Se algum dos vértices críticos consecutivos no caminho global não for conectável, a~\gls{mp} os considera como ponto de início e objetivo e planeja recursivamente um caminho entre eles.
          Desta maneira, a~\gls{mp} decompõe o problema em problemas menores e se executa recursivamente para eventualmente encontrar uma trajetória.
\end{itemize}

Além dessa flexibilidade, a~\gls{mp} apresenta como vantagens o fato de planejar caminhos com complexidade constante independente da geometria dos obstáculos, e apresenta um tempo médio de computação de menos de um segundo~\cite{Qureshi2019}, também é capaz de generalizar ambientes similares aos dados de treinamento, mesmo que esses ambientes não estivessem presentes nos dados em que o modelo foi treinado.

Enquanto outros planejadores tenham que expandir seus espaços de planejamento para fazer uma busca exaustiva antes de encontrar um caminho ótimo, a~\gls{mp} não precisa realizar essa busca através do ambiente para encontrar um caminho similar, como é ilustrado na Figura~\ref{planners_comparison:1}.

\subsection{Aprendizado em lotes offline}\label{subsec:offline-batch}

Este método requer que todos os dados de treinamento estejam disponíveis antes do treinamento da~\gls{mp}.
Ou seja, são necessárias representações do ambiente, estados de início e de objetivo em cada ambiente, além de uma solução de um algoritmo especialista para cada trajetória, possiblitando que seja feita a propagação de erros.

Nesta abordagem, os módulos podem ser treinados separadamente ou do início ao fim utilizando a função de perda da rede de planejamento.
Para realizar o treinamento do~\gls{pnet} e do~\gls{enet} juntos, o gradiente da função de perda da~\gls{pnet} é propagado através de ambos os módulos.
Para que os módulos sejam treinados separadamente é necessário que haja uma ampla quantidade de dados de nuvem de pontos do ambiente disponível.

\subsection{Aprendizado contínuo com memória episódica}\label{sub:mp:cont_learn_mem}

Este método de treinamento assume que todos os dados chegam em fluxos, assim a distribuição de dados global é desconhecida.
É necessário que os módulos~\gls{pnet} e~\gls{enet} sejam treinados juntos, já que ambos devem ser adaptar às mudanças contínuas no ambiente.
Problemas com esse tipo de aquisição de dados são comuns quando se trata do mundo real, um exemplo disso seriam os dados de um \gls{lidar} em um carro autônomo.
Este treinamento requer que ambos os módulos sejam treinados juntos do início ao fim, já que precisam se adaptar ao fluxo de dados.
Como este problema é considerado como um problema de aprendizado supervisionado, os dados de entrada chegam com as referências, por exemplo,
\begin{equation}
    (s_1, y_1, ... , s_i, y_i, ... , s_N, y_N)
\end{equation}
onde $s = (c_t, c_T, x_{obs})$ se refere aos dados de entrada do~\gls{mp} sendo compostos pelo estado atual do robô $c_t$, o estado objetivo $c_T$, e a informação referente aos obstáculos $x_{obs}$.
O objetivo $y$ é o próximo estado $c_{t+1}$ em um trajetória fornecida por um planejador especialista.

Um problema comum em redes neurais que utilizam de aprendizado contínuo é um esquecimento catastrófico.
Este problema é descrito em termos da transferência de conhecimento na direção reversa.
Considerando a taxa média de sucesso do modelo em um conjunto de dados $M$ após aprender um exemplo especialista no período $t - 1$ e $t$ como $A_{M, t-1}$ e $A_{M, t}$.
Uma métrica descrita como transferência reversa pode ser escrita como $B = A_{M, t} - A_{M, t-1}$.
Uma transferência reversa $B >= 0$ indica que após aprender de uma nova experiência, ocorreu uma melhora na performance do planejador em experiências anteriores representadas como $M$.
Já uma transferência reversa $B < 0$ indica que ao aprender a partir de uma nova experiência, a performance do planejador em experiências anteriores deteriorou.

Para evitar que a transferência reversa negativa leve a um esquecimento catastrófico, é empregado o gradiente de memória episódica.
Este método usa a memória episódica $M$ que tem um conjunto finito de dados contínuos vistos anteriormente para garantir que a atualização do modelo não leve a uma transferência reversa negativa, permitindo somente que a transferência reversa seja positiva.

Além da memória episódica $M$, também é utilizada um buffer $B^*$ de replay.
Esse buffer de replay permite que a~\gls{mp} revise amostras passadas e aprenda novamente a partir delas.
Essa revisão ajuda a mitigar o problema do esquecimento catastrófico e leva a uma melhor performance~\cite{Qureshi2019}.

\subsection{Aprendizado contínuo ativo}

Este método de treinamento, proposto por~\cite{Qureshi2019}, também assume que todos os dados chegam em fluxos, porém a referência de um planejador especialista é requisitada quando necessário.
É construído em cima do método de aprendizado contínuo com memória episódica.

Primeiramente, adquire-se os dados de treinamento ao ativamente requisitar demonstrações para problemas que a~\gls{mp} falhou em encontrar um caminho.
Depois, é empregada uma estratégia de seleção de amostras para suprimir as demonstrações do especialista de modo que preencham a memória episódica, assim aproximando-as da distribuição global do fluxo de dados.
Essa seleção de dados em dois níveis aumenta a eficiência das amostras de treinamento enquanto aprende a generalizar modelos neurais para a~\gls{mp}.

A cada passo de tempo $t$, o ambiente gera um problema de planejamento $(c_0, c_T, X_{obs})$ composto pelo estado inicial do agente $c_0$, pelo estado objetivo $c_T$ e pelo espaço de estados dos obstáculos $X_{obs}$.
Inicialmente, utilizam-se demonstrações de um especialista como pré treinamento por um valor $N_c\;\epsilon\;N_{\ge0}$ de iterações.
Se a~\gls{mp} falhou em determinar uma solução para a trajetória, um planejador especialista é executado para determinar a solução para o problema.
A trajetória especialista $\sigma$ é armazenada no buffer de replay $B^*$ e na memória episódica $M$ baseada nas suas estratégias de seleção de amostras.
Assim como no método de aprendizado contínuo com memória episódica, são realizadas revisões de amostras anteriores.