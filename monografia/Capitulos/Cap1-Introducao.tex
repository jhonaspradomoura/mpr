\chapter{Introdução} \label{ch:introducao}

Com o crescente desenvolvimento da robótica, produtos como veículos autônomos e robôs industriais têm se tornando cada vez mais o foco de pesquisas.
Uma das áreas que se destacam na operação destes produtos é a da navegação.
Ela é composta por algoritmos responsáveis pela interpretação e mapeamento do ambiente, pela localização do agente e de pontos de interesse e pelo planejamento de trajetórias.
Neste trabalho, o planejamento de trajetórias será tratado como foco.

O planejador de trajetórias é o algoritmo responsável por definir a trajetória a ser percorrida pelo agente para chegar a um objetivo, evitando possíveis obstáculos.
Este objetivo pode variar com o tipo de agente a executá-lo.
Um veículo autônomo teria como objetivo uma coordenada geográfica.
Já no caso de um robô industrial, o objetivo seria alcançar uma determinada pose a partir da rotação e translação de seus atuadores.

Um planejador de trajetórias pode ser categorizado como global ou local.
Como definido por~\citeauthoronline{Marin2018planners}, um planejador global usa as informações adquiridas até o momento, por exemplo um mapa do ambiente, para buscar uma trajetória a ser percorrida~\cite{Marin2018planners}.
Um planejador local é definido como o planejador responsável por recalcular a trajetória definida pelo planejador global somente em um raio ao redor do agente, buscando desviar de obstáculos previamente desconhecidos.

~\citeauthoronline{Qureshi2019} define o planejador de trajetórias ideal como aquele que segue os seguintes requisitos~\cite{Qureshi2019}:
\begin{enumerate}
    \item garantia de que uma solução globalmente ótima será encontrada caso exista alguma;
    \item eficiência computacional em termos de tempo de processamento e gerenciamento de memória;
    \item insensibilidade à complexidade do ambiente, assim sendo eficiente e efetivo ao encontrar soluções independente das características do ambiente;
\end{enumerate}

No estado da arte destacam-se algoritmos de busca heurística, como $A^*$~\cite{Kurzer2016} e Dijkstra~\cite{Sniedovich2006}, algoritmos baseados em amostragem, como \gls{rrt}~\cite{lavalle_2006} e \gls{rrts}~\cite{karaman2011}, e os recentes algoritmos de~\gls{am}, como~\gls{mp}~\cite{Qureshi2019} e algoritmos que usam ~\gls{ar}~\cite{reinforcementPath}~\cite{Yu2018}~\cite{wulfmeier2017}.
Tanto algoritmos baseados em heurística quanto baseados em amostragem possuem como fragilidade o aumento do custo computacional proporcionalmente à dimensionalidade do ambiente.
Além disso, algoritmos iterativos como~\gls{rrts} só garantem que a trajetória seja ótima quando o número de amostra tende ao infinito.
A fim de evitar tais desvantagens, soluções baseadas em~\gls{am} têm sido objeto de pesquisa.

\section{Objetivos}

Este trabalho implementa a versão com aprendizado supervisionado do algoritmo~\gls{mp} e avalia sua eficácia em gerar uma trajetória em que não ocorra conflito com obstáculos e que seja eficiente computacionalmente, de modo a contribuir na redução do custo computacional desta tarefa em agentes autônomos.
Estes objetivos contrastam com as desvantagens dos outros algoritmos que compõem o estado da arte.

Este trabalho também avalia possíveis aprimorações no processo de treinamento das redes neurais supervisionadas utilizando recursos recém introduzidos no estado da arte de~\gls{am}, como a função de ativação SELU e a redução dinâmica da taxa de aprendizagem durante o treinamento da rede neural.

Os experimentos realizados neste trabalho avaliam a eficácia do uso de uma rede neural supervisionada para o planejamento de trajetórias em ambientes com obstáculos fixos.
Deste modo, esses resultados refletem o uso deste algoritmo como um planejador global.
Adicionalmente, é possível perceber que o uso das contribuições sugeridas pelo presente trabalho auxilia beneficamente no treinamento das redes neurais.

\section{Organização do Trabalho}

Esta monografia é composta por cinco capítulos além da introdução. Suas finalidades são descritas a seguir:

\begin{description}
    \item[Trabalhos Relacionados -] realiza uma breve descrição de alguns métodos utilizados pelo estado da arte baseados em amostragem e em~\gls{am};
    \item[Fundamentação Teórica -] descreve os fundamentos teóricos necessários para entender e implementar este trabalho;
    \item[Desenvolvimento dos Experimentos -] explica o procedimento utilizado para a aquisição de dados, treinamento do modelo e implementação do algoritmo;
    \item[Resultados Obtidos -] analisa os resultados obtidos comparando a performance da estrutura sugerida em~\cite{Qureshi2019} com a estrutura implementada neste trabalho. Além disso, também é comparado o tempo de execução do modelo implementado com algoritmos do estado da arte;
    \item[Considerações Finais -] conclui o projeto analisando o valor dos resultados obtidos e apresentando propostas para serem estudadas futuramente;
\end{description}

% Soluções que utilizam algoritmos de busca heurística em agentes autônomos, como $A^*$~\cite{Kurzer2016} e Dijkstra~\cite{Sniedovich2006}, possuem uma baixa eficiência computacional, portanto seu uso é problemático em ambientes que exigem uma resposta ágil do agente ou quando a dimensionalidade do ambiente é grande, o que aumenta consideravelmente a complexidade e portanto diminui a velocidade de processamento do algoritmo.

% Os algoritmos baseados em amostragem, como o \gls{rrt}~\cite{lavalle_2006} e o \gls{rrts}~\cite{karaman2011}, exploram iterativamente o ambiente por soluções, o que os torna ineficazes quando uma resposta rápida é exigida do agente ou quando a dimensionalidade do ambiente é grande.
% Além disso, apesar de haver garantia de que uma solução seja encontrada, caso uma exista, o número de iterações tende ao infinito proporcionalmente a complexidade do ambiente e não há garantia de que a solução encontrada seja ótima.

% Uma das soluções comumente propostas para o planejamento de trajetórias em agentes autônomos são os algoritmos de busca por heurística, como A*~\cite{Kurzer2016} e Dijkstra~\cite{Sniedovich2006}.
% Esses algoritmos realizam o cálculo do custo de cada ação vizinha iterativamente, auxiliados por uma função heurística para cada nó, até alcançar seu objetivo.
% Desta maneira, o caminho com menor custo representa o caminho mais curto, denominada trajetória ótima.
% Porém, esses algoritmos exigem um alto custo computacional quando lidam com um espaço de busca elevado, isto é, com uma quantidade de estados possíveis muito grande.

% Dentre os trabalhos englobados no contexto da solução de problemas de planejamento de trajetórias, destacam-se os algoritmos \gls{rrt} descritos em ~\cite{lavalle_2006} e sua variação otimizada~\gls{rrts}~\cite{karaman2011}, além dos métodos de buscas guiadas por heurísticas como~\cite{Gammell2015}, e aquele baseados em busca preguiçosa como~\cite{haghtalab2017provable}.
% Porém, por mais que esses algoritmos sejam eficientes em termos de processamento e precisão, ainda há espaço para aprimoramentos.


% Uma nova estratégia muito explorada recentemente combina aspectos da teoria de planejamento de trajetórias e elementos do \gls{am}.
% Na robótica, essa união com~\gls{am} já é bem estabelecida no campo de controle, sendo uma área de pesquisa bastante explorada por trabalhos relevantes na literatura como~\cite{Jiang2014} e~\cite{Yu2018}.
% Tomando como referência os três requisitos definidos por~\cite{Qureshi2019} podemos verificar o potencial dessa união.
% \begin{enumerate}
%     \item Algoritmos de~\gls{am} são capazes de processar uma grande quantidadede dados, e são capazes de extrair informações relevantes de tais dados para auxiliar na tomada de decisão de agentes autônomos com um baixo custo computacional.
%           Ao analisar esses dados, os algoritmos são modelados de forma a seguir uma referência ao processar os dados de entrada, sendo uma rede neural supervisionada ou não, assim ao alimentar a rede neural com dados que refletem o problema é possível encontrar um solução ótima.
%     \item Em uma rede neural, a maior parte do processamento é realizado na fase de treinamento, isso permite que ocorra a otimização do modelo gerado ao ser executado.
%           A rede pode não ser capaz de analisar um grande volume de dados em tempo real, o que incapacita essa solução para o requisitem, porém problemas, como um planejador global, que admitem uma certa latência são muito beneficiados com a capacidade de análise de dados que provê de uma rede neural.
%     \item Algoritmos de~\gls{am} são capazes de generalizar informações a partir de um conjunto de dados, o que os possibilita adaptar-se à complexidade do ambiente.
% \end{enumerate}
% Desta forma o potencial em empregar técnicas de \gls{am} em uma planejador de trajetórias é demonstrado pela sua capacidade de satisfazer os três requisitos definidos por~\cite{Qureshi2019}.

% \section{Justificativa}

% Justificativa para a realização do trabalho, situando sua relevância no contexto da sua área de formação e sua
%importância para o avanço do conhecimento. A justifica também precisa destacar a motivação pessoal para a escolha do
%tema estudado.


% Descrever o objetivo principal e os objetivos específicos da pesquisa. Os objetivos constituem a finalidade de um
%trabalho científico. O objetivo principal é mais amplo e deve descrever de forma clara e sucinta qual a meta que se
%deseja atingir (uma proposta que solucione um problema; uma proposta de melhoria; uma análise de uma situação, entre
%outros exemplos).

% Para cumprir o objetivo principal muitas vezes é necessário delimitá-lo em partes menores, ou seja, em objetivos
%específicos. Os objetivos específicos devem ser partes do trabalho, que após cumpridos pelo pesquisador, atinge-se  o
%objetivo principal do trabalho. Devem vir listados, apresentados por tópicos e separados por ponto-e-vírgula.


% \section{Organização do Trabalho}

% Um parágrafo fazendo uma descrição dos capítulos restantes do documento. 

% \subsection{Estrutura da Monografia}

% Segue uma \textbf{sugestão} para a estrutura da monografia: 

% \begin{description}
%   \item[Capítulo 1:] Introdução.
%   \item[Capítulo \ref{RevisaoBibliografica}:] Revisão Bibliográfica/ Embasamento Teórico (com o referencial teórico
%e trabalhos relacionados).
%   \item[Capítulo \ref{desenvolvimento}:] Metodologia ou Desenvolvimento (material e métodos).
%   \item[Capítulo \ref{resultado}:] Resultados e Discussões.
%   \item[Capítulo \ref{conclusao}:] Conclusão (e trabalhos futuros).
% \end{description}












