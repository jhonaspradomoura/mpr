\chapter{Trabalhos Relacionados}\label{chap:trabalhos-relacionados}

Nesta seção são tratados os trabalhos de destaque no tema de planejamento de trajetórias.
Para isso, são explicadas as técnicas que surgiram na busca pela solução deste problema e evoluções que estas causaram.

Inicialmente, algoritmos completos foram desenvolvidos para solucionar o problema de planejamento de trajetórias, porém estes sofriam com processamento ineficiente~\cite{lavalle_2006}.
Isso levou ao desenvolvimento de métodos mais eficientes com completude de resolução e completude probabilística.
Algoritmos completos~\cite{Kurzer2016}~\cite{Sniedovich2006} garantem que uma trajetória seja encontrada, caso uma exista, em um tempo finito, porém são impráticos em muitas aplicações no mundo real por precisarem de informações sobre todo o ambiente, o que não é simples de obter.
Algoritmos com completude de resolução~\cite{real-time-obstacle-avoidance-1985} são iguais aos de completude total, porém precisam que parâmetros sejam configurados para se adequar a cada problema de planejamento.
Algoritmos com completude probabilística surgiram com o objetivo de superar as limitações de algoritmos completos e com completude de resolução, estes também são chamados de~\gls{pba}~\cite{lavalle_2006}.
Esses métodos utilizam de técnicas de amostragem para gerar árvores de exploração rápida pelo espaço de estados livre de obstáculos.
A trajetória possível é determinada analisando o conjunto de trajetórias encontradas e selecionando a mais curta dentre elas.
Esses algoritmos tem completude probabilística pois a probabilidade de achar uma solução, se uma existir, se aproxima de um quando o número de amostras se aproximar do infinito.

Dentre os algoritmos utilizados em~\gls{pba}s estão os de consulta única, como o~\gls{rrt}~\cite{Lavalle98rapidly-exploringrandom}, e os de consulta múltipla, como o ~\gls{prm}~\cite{latombe1998probabilistic}.
Métodos baseados no~\gls{prm} precisam que seja realizado um pré-processamento do ambiente, o que tem alto custo de performance.
Além disso, métodos de consulta múltipla podem ser substituídos por várias requisições a métodos de consulta única.
Deste modo, métodos como o~\gls{rrt} e suas variantes se tornaram ferramentas promissoras para o planejamento de trajetórias.
Apesar de sempre achar uma solução de maneira eficiente, esses métodos não garantem a trajetória encontrada seja ótima.
Para solucionar isso, foi desenvolvido o~\gls{rrts}~\cite{lavalle_2006} que garante assintoticamente que a menor trajetória seja encontrada, se existente.
Porém, o~\gls{rrts} se torna mais ineficiente computacionalmente a medida que a dimensionalidade do problema aumenta, o que faz com que sua performance seja próxima com a de algoritmos como $A^*$ em ambientes de alta dimensionalidade.
Vários métodos foram desenvolvidos buscando mitigar esse fator, como a amostragem enviesada~\cite{Gammell2015}~\cite{GammellInformed}, a avaliação preguiçosa de arestas~\cite{haghtalab2017provable} e a geração bidirecional de árvores~\cite{QURESHI20151}.

\gls{pba}s com amostragem enviesada realizam a amostragem adaptativamente do espaço de estados para superar limitações causadas pela exploração aleatória dos métodos anteriores.
O Informed-RRT$^*$~\cite{GammellInformed}, por exemplo, define uma região elipsoidal utilizando a trajetória inicial encontrada pelo~\gls{rrts} de modo que limite a região de amostragem, reduzindo o espaço de estados.
A área da elipse é reduzida até que seja encontrada a trajetória ótima.
No entanto, devido a dependência do~\gls{rrts} para encontrar a trajetória inicial, o Informed-RRT$^*$ sofre em situações em que encontrá-la seja desafiador.
Outra alternativa é o~\gls{bit}~\cite{Gammell2015}, um algoritmo de busca incremental de grafos que usa uma região elipsoidal que muda dinamicamente para gerar lotes de amostragens e assim computar as possíveis trajetórias.
Apesar de haverem melhorias na velocidade de processamento, esses métodos continuam ineficientes em ambientes de alta dimensionalidade.

Algoritmos que fazem avaliação preguiçosa de arestas demonstram melhorias significantes na velocidade de processamento ao avaliar arestas somente ao longo de soluções em potencial.
Entretanto, esses métodos são dependentes do algoritmo seletor de arestas e exibem uma performance limitada em alguns ambientes~\cite{houdeep}.
Métodos de geração bidirecional de árvores apresentam melhorias quando o ambiente possui passagens apertadas porém mantém as limitações dos~\gls{pba}s~\cite{QURESHI20151}.

Soluções que usam aprendizado por reforço profundo também têm obtido resultados promissores~\cite{Yu2018}~\cite{wulfmeier2017}.
No~\gls{ar}, o agente adquire conhecimento através da exploração do ambiente e aprende por tentativa e erro.
Por ser um método de aprendizado não supervisionado, a quantidade de informações previamente adquiridas necessárias é consideravelmente menor do que em outros métodos de~\gls{am}, devido ao fato do algoritmo coletar amostras durante o processo de treinamento~\cite{wulfmeier2017}.
Em~\citeauthoronline{lei2018}, a habilidade do agente de dinamicamente desviar de obstáculos e realizar o planejamento local no ambiente é aprimorada com a adição de um algoritmo de Q-Learning ao algoritmo de~\gls{ar}.
~\citeauthoronline{wen2020}~\cite{wen2020} usa redes residuais totalmente convolucionais no reconhecimento de obstáculos, um algoritmo de Dueling Deep Q-Network para realizar o planejamento do desvio de obstáculos, e simultaneamente cria o mapa 2D do ambiente com um algoritmo de FastSLAM, para enfim obter um algoritmo capaz de desviar de obstáculos fixos e móveis em ambientes desconhecidos.
~\citeauthoronline{Yu2018}~\cite{Yu2018} propõe a junção de redes neurais e de aprendizado por reforço hierárquico, assim obtendo melhoria na performance do planejador e na sutileza da trajetória quando comparado com outras soluções que usam Q-Learning.

Outras soluções usam redes neurais para aprimorar~\gls{pba}s ou como novos métodos de planejamento.
Conditional Variational Autoencoders foram propostos por~\citeauthoronline{ichter2017}~\cite{ichter2017} a fim de contextualizar as informações do ambiente, assim gerando amostras codificadas para~\gls{pba}s.
~\citeauthoronline{Bhardwaj2017}~\cite{Bhardwaj2017} propõe um método que aprende um política determinística para guiar a expansão do grafo do planejador para regiões que potencialmente contém a solução.
~\citeauthoronline{tamar2017value}~\cite{tamar2017value} apresenta as~\gls{vin} uma abordagem que considera que o algoritmo de planejamento value-iteration~\cite{bertsekas2005dynamic} pode ser representado por uma rede neural convolucional.
Desse modo, anexando um módulo de planejamento em uma rede neural feedforward de classificação, obtem-se um modelo capaz de aprender os parâmetros de planejamento e de entregar previsões válidas.
Entretanto, as~\gls{vin} só são aplicáveis em tarefas de planejamento discreto.
~\citeauthoronline{ichter2018robot}~\cite{ichter2018robot} apresenta o algoritmo~\gls{l2rrt}, que explora globalmente o espaço de estados mantendo uma representação codificada do ambiente formada através da amostragem de estados codificados e propagando dinamicamente trajetórias próximas na árvore, sendo constantemente supervisionado por uma rede de checagem de colisões.
Entretanto, não é claro se a existência de uma trajetória no espaço de estados sempre implica na existência de uma trajetória no espaço codificado aprendido e vice-versa.


% Nesta seção são tratados os trabalhos relacionados ao tema de planejamento de trajetórias.
% A seguir estão dispostos métodos empregados na solução de problemas envolvendo este tema.
% São detalhados métodos baseados em amostragem, e um método que propõe a utilização de~\gls{am} na sua solução.
% 
% \section{Métodos de planejamento baseados em amostragem}\label{subsec:sampling}
% 
% ~\gls{smp} são métodos que evitam a construção de um espaço de estados de obstáculos $C_{obs}$ pertencente a um espaço de estados $C$, preferindo realizar uma busca que explora o espaço de estados $C$ por meio de técnicas de amostragem, como descrito em~\cite{lavalle_2006}.
% 
% Os algoritmos que utilizam deste tipo de método a serem tratados neste trabalho são:~\gls{rrt},  \gls{rrts}, \gls{bit} e Informed RRT.
% 
% \subsection{\glsentryfull{rrt}}\label{subsec:rrt}
% 
% O~\gls{rrt} é um método introduzido por~\cite{Lavalle98rapidly-exploringrandom} como uma estrutura de dados randômica projetada para uma gama de problemas de planejamento de trajetórias.
% 
% Para explorar o espaço de estados $C$, o~\gls{rrt} precisa expandir seus vértices.
% Para realizar essa expansão, primeiramente um estado $Z_{rand}$ é gerado aleatoriamente.
% Feito isso, encontra-se o vértice existente $Z_{near}$ mais próximo dele.
% Por fim, é traçada uma linha entre $Z_{rand}$ e $Z_{near}$.
% Dentre todos os possíveis pontos dispostos por essa linha, aquele que estiver mais distante de $Z_{near}$ sem que haja um obstáculo entre eles será o novo vértice gerado $Z_{new}$.
% O algoritmo é finalizado quando uma solução é encontrada ao gerar um vértice dentro da região objetivo, ou ao alcançar o limite de iterações imposto.
% 
% O processo de expansão é exemplificado na Figura~\ref{fig:exemplo_rrt}, onde $\delta$ se refere a distância entre os vértices $Z_{near}$ e $Z_{new}$.
% Já $Z_{init}$ se refere ao estado inicial.
% 
% \begin{figure}[H]
%     \centering
%     \includegraphics[width=\textwidth, height=65mm]{Figuras/rrt_expansion.jpg}
%     \caption{Processo de expansão do algoritmo~\gls{rrt}.}\label{fig:exemplo_rrt}
% \end{figure}
% 
% O~\gls{rrt} apresenta algumas vantagens, como:
% \begin{itemize}
%     \item sua expansão é fortemente enviesada na direção de porções inexploradas do espaço de estados;
%     \item os vértices e arestas são mantidos independente da quantidade de iterações, portanto caso seja necessário que outra trajetória seja calculada no mesmo ambiente, todo o espaço de estados explorado será reaproveitado;
%     \item o algoritmo de planejamento inteiro pode ser construído sem que seja necessário realizar um mapeamento do ambiente;
% \end{itemize}
% 
% Pelo fato de se expandir aleatoriamente, o tempo computacional gasto pelo referido método aumenta de maneira proporcinal ao tamanho do espaço de estados do problema.
% Isto pode ser um gargalo em situações em que um sistema necessite de velocidade de processamento.
% Além disso, o~\gls{rrt} não atende as condições de otimalidade, isto é, não garante que a solução ótima seja encontrada.
% 
% \subsection{\glsentryfull{rrts}}\label{subsec:rrt*}
% 
% O~\gls{rrts}~\cite{karaman2011} é uma versão aprimorada do~\gls{rrt}, proposto para solucionar a não otimalidade do último.\nocite{NoreenRRTS}
% Para um vértice único $u\;\epsilon\;V$, seu predecessor é definido como a função que mapeia um vértice $v\;\epsilon\;V$ ao vértice $u$ de modo que $(u, v)\;\epsilon\;E$, onde $V$ e $E$ representam respectivamente o conjunto de vértices e o conjunto de arestas da árvore $G$.
% O custo de um vértice é definido como uma função que mapeia o vértice $v\;\epsilon\;V$ ao custo de um caminho único do vértice raiz da árvore $G$ até o vértice $v$.
% O custo assume a equação $Custo(v) = Custo(Predecessor(v)) + c(Linha(Predecessor(v), v))$, onde $Linha(x_1, x_2)$ representa uma linha reta entre dois pontos $x_1$ e $x_2$, e $c(L)$ representa uma função para avaliar o custo de uma linha $L$.
% Por convenção, considerando $v_0\;\epsilon\;V$ como o vértice raiz da árvore $G$, então $Custo(v_0) = 0$.
% 
% Assim como o~\gls{rrt}, o~\gls{rrts} expande seus nós gerando um estado $Z_{ rand }$ aleatoriamente, encontrando o vértice mais próximo ao estado gerado $Z_{near}$, e por fim criando o vértice $Z_{new}$.
% A partir daí começam as diferenças entre o~\gls{rrts} e o~\gls{rrt}.
% A Figura~\ref{fig:rrts_expansion} ilustra o processo de expansão após a geração do vértice $Z_{new}$.
% Primeiramente é definida uma circunferência de raio $r$ fixo ao redor de $Z_{new}$ de modo que englobe vértices vizinhos.
% Então é criada uma nova aresta entre $Z_{new}$ e o vértice de menor custo dentre os vizinhos.
% Feito isso, caso o vértice de menor custo não seja o mesmo que $Z_{near}$ a aresta entre $Z_{new}$ e $Z_{near}$ é removida.
% 
% \begin{figure}[H]
%     \centering
%     \includegraphics[width=0.9\textwidth, height=70mm]{Figuras/rrts_expansion.png}
%     \caption{Expansão da árvore do~\gls{rrts}.}\label{fig:rrts_expansion}
% \end{figure}
% 
% Após a expansão da árvore, é realizada a operação de revisão e reconexão dos vértices da árvore.
% Como é ilustrado pela Figura~\ref{fig:rrts_rewiring}, investiga-se se o custo de algum dos vizinhos de $Z_{new}$ pode ser reduzido caso seu caminho seja reconectado de modo que passe por $Z_{new}$.
% Caso isso ocorra, a aresta de maior custo é removida e uma nova aresta com $Z_{new}$ é criada, como acontece com o vértice 6 na Figura~\ref{fig:rrts_rewiring}.
% 
% \begin{figure}[H]
%     \centering
%     \includegraphics[width=0.90\textwidth, height=65mm]{Figuras/rrts_rewiring.png}
%     \caption{Operação de reconexão dos vértices no algoritmo RRT Otimizado.}\label{fig:rrts_rewiring}
% \end{figure}
% 
% A expansão é interrompida quando o limite de iterações é alcançado.
% Enquanto isso não acontece, o algoritmo é capaz de retornar a melhor solução encontrada até o momento, se requisitado.
% 
% O algoritmo cria caminhos objetivos como é possível observar na Figura~\ref{fig:rrt*_tree}, desviando de obstáculos e encontrando o caminho mais curto.
% Além disso, caso ocorresse uma mudança no objetivo, a árvore poderia ser reaproveitada já que representa uma parte dos caminhos mais curtos.
% 
% \begin{figure}[H]
%     \centering
%     \includegraphics[width=\textwidth, height=75mm]{Figuras/rrts_formato.png}
%     \caption{Representação da árvore gerada pelo algoritmo \gls{rrts}.
%         Os pontos amarelos são os vértices expandidos.
%         Os pontos verdes fazem do caminho mais curto encontrado.
%         O ponto vermelho é o ponto de origem e os quadrados representam os obstáculos.}\label{fig:rrt*_tree}
% \end{figure}
% \nocite{8255234}
% 
% Apesar do~\gls{rrts} ser capaz de encontrar o caminho ótimo, só existe a garantia de que o caminho encontrado seja o caminho ótimo quando o número de vértices tende ao infinito, como apontado por~\cite{Gammell2015}.
% Além disso, já que é necessário checar se os obstáculos estão sendo evitados ao posicionar um novo vértice, ao conectá-lo a um vértice vizinho e para cada vértice que for reconectado, seu custo computacional é maior quando comparado ao~\gls{rrt}.
% 
% \subsection{Informed RRT*}\label{subsec:informedrrt}
% 
% Proposto por~\cite{GammellInformed}, este algoritmo busca garantir que o caminho ótimo seja encontrado em um número finito de iterações, diferente do~\gls{rrts} e também procura reduzir seu custo computacional.
% 
% A expansão dos nós se assemelha ao processo do algoritmo~\gls{rrts}.
% Após encontrada uma solução define-se uma elipse ao redor do caminho encontrado com pontos focais na origem e no objetivo e área proporcional ao custo do caminho.
% Considera-se que a elipse engloba os caminhos capazes de obter uma solução com menor custo, e assim a geração de novos vértices é focada nesta região.
% Sempre que algum caminho de menor custo for encontrado a área da elipse se reduz.
% Isso se repete até que a elipse se torne uma linha, elegendo essa linha como o caminho ótimo para o problema.
% O procedimento é descrito na Figura~\ref{fig:informed_rrt}.
% 
% \begin{figure}[H]
%     \centering
%     \includegraphics[width=\textwidth, height=70mm]{Figuras/informed_rrt.png}
%     \caption{Procedimento de busca após ser encontrado um caminho inicial~\cite{GammellInformed}}\label{fig:informed_rrt}
% \end{figure}
% 
% Na Figura~\ref{fig:informed_rrt}, o melhor caminho encontrado até o número de iterações está em rosa.
% Na Figura~\ref{fig:informed_rrt}a são demonstradas as iterações inicias, quando o algoritmo já encontrou uma solução e delimitou a elipse.
% Na Figura~\ref{fig:informed_rrt}b é ilustrado o processo em um estágio intermediário, onde a área da elipse está diminuindo.
% Na Figura~\ref{fig:informed_rrt}c é encontrada a melhor solução para o problema, nota-se que a elipse virou uma linha e essa solução foi encontrada em pouco mais de 1000 iterações.
% 
% O Informed RRT$^\star$ consegue garantir um resultado ótimo em um número finito de iteração graças a crescente limitação no espaço de busca causada pela elipse.
% 
% No entanto,~\cite{GammellInformed} denota que essa abordagem ainda possui desvantagens.
% O fato do tamanho da elipse ser proporcional à distância entre os pontos inicial e objetivo faz com que haja um custo computacional maior para casos em que essa distância é muito grande.
% Outro problema é que não há limitação na área da elipse, portanto é possível que a mesma seja maior do que o espaço de estados, o que resultaria em uma piora do desempenho computacional.
% 
% \subsection{\glsentryfull{bit}}\label{subsec:bit}
% 
% O algoritmo~\gls{bit}, apresentado em~\cite{Gammell2015}, balanceia os benefícios de uma busca em grafos com técnicas baseadas em amostragem.
% Possui como característica a utilização do loteamento de amostras, que tem como propósito a realização de uma busca ordenada em um domínio contínuo de planejamento mantendo a representação crescente do domínio do problema, o que permite aumentar sua precisão a cada iteração.
% Deste modo, sua busca tende a uma solução minimizada proposta pela heurística.
% 
% Ao processar vários lotes de amostras, o algoritmo converge assintoticamente para o ótimo global.
% Isso é feito eficientemente utilizando técnicas de busca incremental que incorporam as novas amostras na busca existente.
% Os múltiplos lotes também permitem buscas subsequentes focando assim em uma subregião que possa conter uma solução melhor, analogamente ao Informed RRT$^\star$.
% 
% Um conjunto discreto de estados $X_{samples}$ pertencente ao espaço de estados $X$ pode ser visto como um grafo no qual as arestas são dadas algoritmicamente por uma função de transição, também chamado de grafo implícito.
% Quando esses estados são amostrados randomicamente, as propriedades do grafo podem ser descritas por um modelo probabilístico conhecido como~\gls{rgg}~\cite{penrose_random_2003}.
% Em um~\gls{rgg}, as conexões entre estados dependem de sua posição geométrica relativa.
% Comumente, as~\gls{rgg}s possuem arestas para um número específico de vizinhos próximos de cada estado (um grafo $k$-nearest~\cite{xue_number_2004}) ou para vizinhos em um distância específica (um grafo de disco-$r$~\cite{gilbert_random_1961}).
% 
% Deste modo, planejadores baseados em amostragem podem ser vistos como algoritmos capazes de construir um~\gls{rgg} implícito e uma árvore explícita no espaço livre de um problema de planejamento.
% Assim como técnicas de busca em grafos, a performance de tal algoritmo depende da qualidade da representação do~\gls{rgg} e da eficiência da busca.
% 
% \begin{figure}[H]
%     \centering
%     \includegraphics[width=1.\textwidth, height=60mm]{Figuras/bit.png}
%     \caption{Procedimento utilizado pelo~\gls{bit}.
%         Os estados da origem e do objetivo são respectivamente ilustrados em verde e vermelho.
%         A solução atual está ilustrada em rosa.
%         O progresso do lote atual é descrito pelo pontilhado cinza enquanto a subregião que pode conter melhores regiões  é descrita pelo pontilhado preto.}\label{fig:bit_s}
% \end{figure}
% 
% A implementação do~\gls{bit}, ilustrada na Figura~\ref{fig:bit_s}, começa com um~\gls{rgg} com arestas implícitas definido por amostras aleatórias uniformemente distribuídas pelo espaço livre.
% Uma árvore explícita é construída partindo da origem em direção ao objetivo através de uma busca heurística~(Figura~\ref{fig:bit_s}a).
% A árvore inclui somente arestas sem colisão e sua construção deve parar quando uma solução for encontrada ou quando não houver mais possibilidade de expansão~(Figura~\ref{fig:bit_s}b).
% Isso define um lote.
% 
% Começando um novo lote, uma~\gls{rgg} implícita mais densa é construída adicionando mais amostras e atualizando o parâmetro ($r$ ou $k$).
% Se uma solução tiver sido encontrada, as amostras são limitadas para a subregião que pode conter uma solução melhor.
% A árvore é atualizada com técnicas de busca incremental reutilizando as informações existentes~(Figura~\ref{fig:bit_s}c).
% A construção da árvore ocorre enquanto houver possibilidade de melhorar a solução ou enquanto houverem arestas sem colisão disponiveis~(Figura~\ref{fig:bit_s}d).
% O processo continua com novos lotes enquanto o limite de iterações ou de tempo de execução não for atingido.
% 
% O~\gls{bit} obtém uma performance melhor que outros planejadores ótimos baseados em amostragem, especialmente em situações com alta dimensionalidade.
% Embora ainda seja custoso computacionalmente, o~\gls{bit} tem uma chance maior de encontrar a solução ótima do que planejadores como~\gls{rrts} e a encontra com menos iterações.
% 
% \section{Métodos que utilizam conceitos de aprendizado de máquinas}\label{sec:rev:metodos_am}
% 
% A partir do crescimento das redes neurais na última década, iniciou-se a busca pela eficácia de tais métodos para resolver problemas clássicos, como planejamento de trajetórias.
% Esta seção trata de métodos que utilizam redes neurais para solucionar problemas de planejamento de trajetórias de forma semelhante a~\gls{mp}.
% 
% \subsection{Planejador de trajetórias latente baseado em amostragem}
% 
% Apresentado por~\cite{L-SBMP}, a metodologia denominada \gls{lsbmp} aprende uma representação latente planejável para computar planos de trajetórias para sistemas robóticos complexos.
% A metodologia é capaz de encontrar soluções utilizando um ambiente representado por pixels ou até mesmo problemas de planejamento para robôs humanóides.
% Uma representação latente se refere a representação de todos os dados em uma forma simplifica que facilita a análise.
% 
% Para construir a representação latente são utilizados três estruturas: um \gls{ae}, um modelo latente de dinâmica local, e um classificador de colisõesj.
% Essas redes podem ser treinadas somente por dados não processados dos estados e das ações do sistema unidos à supervisão de um checador de colisões.
% Um algoritmo baseado no~\gls{rrt} é usado para planejar trajetórias diretamente em um espaço latente.
% Este algoritmo explora o espaço latente globalmente e é capaz de generalizar para novos ambientes.
% 
% A arquitetura, denominada~\gls{lsbmp}, inicialmente utiliza um autoencoder para projetar um estado de alta dimensionalidade $x_t$ para um estado latente de baixa dimensionalidade $z_t$, e também permite que a decodificação de $z_t$ para formar entradas para um controlador subsequente.
% Posteriormente, o modelo dinâmico reforça as limitações dinâmicas locais no espaço latente codificado e as permite serem propagadas.
% Por fim, o classificador de colisões verifica se a conexão entre dois estados latentes próximos é possível analisando o ambiente.
% 
% O espaço latente é treinado através de uma série de trajetórias do sistema robótica em operação, por exemplo utilizando uma sequência de estados $x_t$ e ações $u_t$.
% O classificador de colisões é treinado a partir de um banco de dados referentes a estados próximos e que possuem uma classificação denotando se a trajetória possui uma colisão.