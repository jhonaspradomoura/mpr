import numpy as np
import os

project_path = f"{os.path.abspath(__file__).split('/unify_dataset')[0]}"


def unify(folder):
    envs = os.listdir(f"{project_path}/{folder}")
    envs = [env for env in envs if "env" in env]
    for env in envs:
        paths = os.listdir(f"{project_path}/{folder}/{env}")
        env_data = []
        for path in paths:
            env_data.append(np.load(f"{project_path}/{folder}/{env}/{path}").reshape((-1, 2)))
        np.save(f"{project_path}/{folder}/npy/{env}", env_data)


if __name__ == '__main__':
    unify("training")
    unify("validation")
